import {Component, OnInit} from '@angular/core';
import {ArticleService} from './news/article.service';
import {Article} from './news/article';
import {TickerMeldung} from './tickerMeldung';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  tickerMeldungen = new Array<TickerMeldung>();
  disableActionBar = false;

  constructor(private articleService: ArticleService,
              private cookieService: CookieService) {
    this.disableActionBar = this.cookieService.check('disableActionBar');
    this.articleService.getAllArticles()
      .subscribe(actions => {
        const articles = this.articleService.getArticlesFromActions(actions) as Article[];
        for (let i = 0; i < 5; i++) {
          this.tickerMeldungen.push(new TickerMeldung(articles[i].headline, '/news', articles[i].id));
        }
      });
  }

  ngOnInit() {  }

  getDuration(): string {
    let joinedMeldungen = '';
    this.tickerMeldungen.forEach(meldung => {
      joinedMeldungen = joinedMeldungen + ' ' +  meldung.content;
    });
    return joinedMeldungen.length / 6 + 's';
  }

}
