import { Component, OnInit } from '@angular/core';
import {NavigationService} from './navigation.service';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  navigationLinks: Array<string>;

  constructor(private navigationService: NavigationService,
              private loginService: LoginService) {
    this.navigationLinks = this.navigationService.getNaviLinks();
    this.loginService.loggedIn.subscribe(() => this.navigationLinks = this.navigationService.getNaviLinks());
  }

  ngOnInit() {
  }

}
