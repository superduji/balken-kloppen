import {Injectable} from '@angular/core';
import {Route, Router} from '@angular/router';
import {Link} from '../models/link.model';
import {LoginService} from "../login/login.service";

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private router: Router,
              private loginService: LoginService) {
  }

  getNaviLinks() {
    return this.setChildrenLinks(this.router.config);
  }

  private setChildrenLinks(configuration) {
    const links = [];
    for (const route of configuration) {
      if (this.showLinkInNavi(route)) {
        links.push(new Link(route.path, route.data['displayText'], true));
      }
    }
    return links;
  }

  private showLinkInNavi(route: Route): boolean {
    return route.data
      && route.data.displayText
      && !route.data.hide
      && !(route.data.loginRequired && !this.loginService.isUserLoggedIn());
  }

}
