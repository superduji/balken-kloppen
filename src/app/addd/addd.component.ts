import {Component, OnInit} from '@angular/core';
import {ColorHelper} from '../utils/colorHelper';
import {Addd} from './addd';

@Component({
  selector: 'app-addd',
  templateUrl: './addd.component.html',
  styleUrls: ['./addd.component.css']
})
export class AdddComponent implements OnInit {

  addds = new Array<Addd>();
  currentAdd: Addd;
  currentAddHash: string
  calculatedColors = new Map<string, string>();
  calculatedAnimationDelays = new Map<string, string>();


  constructor(private colorHelper: ColorHelper) {

    this.addds.push(new Addd('fuji_trikot_raw.png', 'https://www.owayo.com/3D/daten/2019/9/30/U5NE89XILP/index.html?o=19094105', true));
    this.addds.push(new Addd('fuji_trikot_lightblue.png', 'https://www.owayo.com/3D/daten/2019/9/30/U5NE89XILP/index.html?o=19094105', false));
    this.addds.push(new Addd('fuji_perlen_green.png', 'https://balken-kloppen.web.app/start', false));
    this.addds.push(new Addd('fuji_perlen_pink.png', 'https://balken-kloppen.web.app/start', false));
    this.addds.push(new Addd('fuji_perlen_raw.png', 'https://balken-kloppen.web.app/start', true));

    this.currentAdd = this.addds[Math.floor(Math.random() * this.addds.length)];
    this.currentAddHash = this.getHash();
  }

  getHash(): string {
    let result  = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = 8;
    for ( let i = 0; i < 8; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  ngOnInit() {
  }

  getColorFromImage(id: string): string {
    if (this.calculatedColors.get(id)) {
      return this.calculatedColors.get(id);
    }

    const img = document.getElementById(id) as HTMLImageElement;
    if (img && img.height > 0 && img.width > 0) {
      const canvas = document.createElement('canvas');
      canvas.width = img.width;
      canvas.height = img.height;
      canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
      const colors = canvas.getContext('2d').getImageData(0, img.height - 1, 1, 1).data;

      const hex = this.colorHelper.rgbToHex(colors[0], colors[1], colors[2]);
      if (hex === '000000') {
        return 'ffffff';
      } else {
        this.calculatedColors.set(id, hex);
        return hex;
      }
    }
    return 'ffffff';
  }

  getAnimationStart(id: string): string {
    if (this.calculatedAnimationDelays.get(id)) {
      return this.calculatedAnimationDelays.get(id);
    }
    const delay = '' + (Math.random() * -30) + 's';
    this.calculatedAnimationDelays.set(id, delay);
    return delay;
  }

}
