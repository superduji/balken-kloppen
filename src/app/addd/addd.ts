
export class Addd {

  imagePath: string;
  href: string;
  transparent: boolean;

  constructor(imagePath: string, href: string, transparent: boolean) {
    this.imagePath = imagePath;
    this.href = href;
    this.transparent = transparent;
  }

}
