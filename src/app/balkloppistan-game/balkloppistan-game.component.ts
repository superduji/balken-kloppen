import {Component, OnInit} from '@angular/core';
import {CreateBalkloppistanService} from '../create-balkloppistan/create-balkloppistan.service';
import {BalkloppistanService} from '../balkloppistan/balkloppistan.service';
import {Balkloppistan} from '../balkloppistan/balkloppistan';
import {BalkloppistanRound} from '../balkloppistan/balkloppistanRound';
import {Router} from '@angular/router';

@Component({
  selector: 'app-balkloppistan-game',
  templateUrl: './balkloppistan-game.component.html',
  styleUrls: ['./balkloppistan-game.component.css']
})
export class BalkloppistanGameComponent implements OnInit {

  localStorageKeyBalkloppistan = 'balkloppistan';
  balkloppistan: Balkloppistan;


  // temp. Hilfswerte
  currentPlayerName = '';
  showUnfinishedErrorText = false;
  failedPlayersNames = new Array<string>();
  passedPlayersNames = new Array<string>();

  // fuer Input-Button-Anordnung
  showInputButtons = false;
  numberOfItems = 17;
  radius = 200;
  offset = 4.7;

  // fuer SuddenDeath-Input-Felder
  showSuddenDeathFields = false;
  showSuddenDeathInputButtons = false;
  numberOfSuddenDeathItems = 7;
  suddenDeathRadius = 150;
  suddenDeathOffset = 2.914;
  currentSuddenDeathRoundIndex = 1;

  constructor(private createBalkloppistanService: CreateBalkloppistanService,
              private balkloppistanService: BalkloppistanService,
              private router: Router) {
  }

  ngOnInit() {
    this.getBalkloppistanFromDB();
  }

  getBalkloppistanFromDB() {
    const balkloppistanFromLocalStorage = JSON.parse(localStorage.getItem(this.localStorageKeyBalkloppistan)) as Balkloppistan;

    if (!balkloppistanFromLocalStorage) {
      const balkloppistanKey = this.createBalkloppistanService.getBalkloppistanKey();
      this.balkloppistanService.getBalkloppistanByKeyAsync(balkloppistanKey)
        .then(balkloppistanSnapshot => {
          this.balkloppistan = balkloppistanSnapshot.data() as Balkloppistan;
          localStorage.setItem(this.localStorageKeyBalkloppistan, JSON.stringify(this.balkloppistan));
        });
    } else {
      this.balkloppistan = balkloppistanFromLocalStorage;
    }
  }

  // for views
  getPlayers() {
    let returnValue = [];
    if (this.balkloppistan) {
      returnValue = this.balkloppistan.players;
    }
    return returnValue;
  }

  // ###################### normale Wuerfe ##############################

  // for views
  getOneItemForEachOfTheMaximumNumberOfPlayerRounds(): Array<number> {
    let max = 0;
    if (this.balkloppistan) {
      this.balkloppistan.players.forEach(player => {
        max = Math.max(max, player.rounds.length);
      });
    }
    return new Array<number>(max);
  }

  // for views
  activateInputButtons(playerName: string, clickRoundIndex: number): void {
    this.balkloppistan = Object.assign(new Balkloppistan(), this.balkloppistan); // shitty workaround
    if (!this.balkloppistan.getPlayerByName(playerName).rounds[clickRoundIndex].finished) {
      this.currentPlayerName = playerName;
      this.showInputButtons = true;
    }
  }

  // for views
  deactivateInputButtons(): void {
    this.currentPlayerName = '';
    this.showInputButtons = false;
  }

  // for views
  getLeftValue(n: number): string {
    const x = this.radius * Math.cos(n / this.numberOfItems * 2 * Math.PI + this.offset);
    return (x + window.innerWidth / 2 - 25) + 'px';
  }

  // for views
  getTopValue(n: number): string {
    const y = this.radius * Math.sin(n / this.numberOfItems * 2 * Math.PI + this.offset);
    return (y + 300) + 'px';
  }

  setThrowsValue(currentRoundIndex: number, currentPlayer: string, throwValue: number) {

    this.balkloppistan = Object.assign(new Balkloppistan(), this.balkloppistan); // shitty workaround

    const currentPlayerRound = this.balkloppistan.getPlayerByName(currentPlayer).rounds[currentRoundIndex - 1];
    if (currentPlayerRound) {
      currentPlayerRound.throwValue = throwValue;
      currentPlayerRound.handicapedValue = this.getHandicapedValueFrom(throwValue);

      localStorage.setItem(this.localStorageKeyBalkloppistan, JSON.stringify(this.balkloppistan));

      this.showUnfinishedErrorText = false;
    }
    this.deactivateInputButtons();
  }

  getHandicapedValueFrom(throwValue): number {
    if (throwValue === 0) {
      return 0;
    }
    if (throwValue === 15) {
      return 15;
    }
    return throwValue * 2 < 16 ? throwValue * 2 : 15; // TODO
  }

  finishRound() {
    this.failedPlayersNames = new Array<string>();
    this.passedPlayersNames = new Array<string>();
    let roundFinished = true;

    this.balkloppistan = Object.assign(new Balkloppistan(), this.balkloppistan); // shitty workaround
    const minRoundValue = this.balkloppistan.getMinHandicapedValueOfRound(this.balkloppistan.currentRoundIndex);

    // filter passed and failed players
    this.balkloppistan.players.forEach(player => {
      const currentPlayerRound = player.rounds[this.balkloppistan.currentRoundIndex - 1];

      if (currentPlayerRound) {
        const handicapedPlayerValueForRound = currentPlayerRound.handicapedValue;
        if (handicapedPlayerValueForRound > minRoundValue) {
          this.passedPlayersNames.push(player.name);
        } else if (handicapedPlayerValueForRound === minRoundValue) {
          this.failedPlayersNames.push(player.name);
        } else if (handicapedPlayerValueForRound === undefined) {
          roundFinished = false;
        }
      }
    });

    // console.log('activateNextRound, finished and passed', roundFinished, this.passedPlayersNames,
    //   this.failedPlayersNames, minRoundValue, this.balkloppistan.currentRoundIndex);
    this.activateNextStep(roundFinished);
  }

  // ###################### Sudden-Death-Wuerfe ##############################

  // for views
  showSuddenDeathFieldsForPlayer(playerName: string): boolean {
    let found = false;
    this.failedPlayersNames.forEach(failedPlayersName => {
      if (failedPlayersName === playerName) {
        found = true;
      }
    });
    return found;
  }

  // for views
  activateSuddenDeathInputButtons(playerName: string) {
    this.currentPlayerName = playerName;
    this.showSuddenDeathInputButtons = true;
  }

  // for views
  deactivateSuddenDeathInputButtons() {
    this.currentPlayerName = '';
    this.showSuddenDeathInputButtons = false;
  }

  // for views
  getCurrentSuddenDeathValueForPlayer(name: string): string {
    let returnValue = '';
    const playerRoundData = this.balkloppistan.getPlayerByName(name).rounds[this.balkloppistan.currentRoundIndex - 1];
    if (playerRoundData && playerRoundData.suddenDeathThrows[this.currentSuddenDeathRoundIndex - 1] !== undefined) {
      returnValue = '' + playerRoundData.suddenDeathThrows[this.currentSuddenDeathRoundIndex - 1];
    }
    return returnValue;
  }

  // for views
  getSuddenDeathLeftValue(n: number): string {
    const x = this.suddenDeathRadius * Math.cos((n * -1) / this.numberOfSuddenDeathItems * 2 * Math.PI + this.suddenDeathOffset);
    return (x + window.innerWidth / 2 - 25) + 'px';
  }

  // for views
  getSuddenDeathTopValue(n: number): string {
    const y = this.suddenDeathRadius * Math.sin((n * -1) / this.numberOfSuddenDeathItems * 2 * Math.PI + this.suddenDeathOffset);
    return (y + 300) + 'px';
  }

  setSuddenDeathThrowValue(currentPlayer: string, throwValue: number) {
    const player = this.balkloppistan.getPlayerByName(currentPlayer);
    if (player.rounds[this.balkloppistan.currentRoundIndex - 1].suddenDeathThrows.length === this.currentSuddenDeathRoundIndex) {
      player.rounds[this.balkloppistan.currentRoundIndex - 1].suddenDeathThrows[this.currentSuddenDeathRoundIndex - 1] = throwValue;
    } else {
      player.rounds[this.balkloppistan.currentRoundIndex - 1].suddenDeathThrows.push(throwValue);
    }
    localStorage.setItem(this.localStorageKeyBalkloppistan, JSON.stringify(this.balkloppistan));
    // managing
    this.deactivateSuddenDeathInputButtons();
  }

  finishSuddenDeathRound() {
    let suddenDeathRoundFinished = true;
    let minThrowsValueOfCurrentSuddenDeathRound = 5;

    this.failedPlayersNames.forEach(failedSuddenDeathPlayersName => {
      const player = this.balkloppistan.getPlayerByName(failedSuddenDeathPlayersName);
      minThrowsValueOfCurrentSuddenDeathRound = Math.min(minThrowsValueOfCurrentSuddenDeathRound,
        player.rounds[this.balkloppistan.currentRoundIndex - 1].suddenDeathThrows[this.currentSuddenDeathRoundIndex - 1]);
    });

    const passedSuddenDeathPlayersNames = new Array<string>();
    const failedSuddenDeathPlayersNames = new Array<string>();

    this.failedPlayersNames.forEach(failedPlayerName => {
      const player = this.balkloppistan.getPlayerByName(failedPlayerName);
      const suddenDeathThrows = player.rounds[this.balkloppistan.currentRoundIndex - 1].suddenDeathThrows;

      if (suddenDeathThrows && suddenDeathThrows.length > this.currentSuddenDeathRoundIndex - 1) {
        const value = suddenDeathThrows[this.currentSuddenDeathRoundIndex - 1];
        if (value === minThrowsValueOfCurrentSuddenDeathRound) {
          failedSuddenDeathPlayersNames.push(failedPlayerName);
        } else if (value > minThrowsValueOfCurrentSuddenDeathRound) {
          passedSuddenDeathPlayersNames.push(failedPlayerName);
        } else {
          suddenDeathRoundFinished = false;
        }
      }
    });

    // console.log(suddenDeathRoundFinished, passedSuddenDeathPlayersNames,
    //   failedSuddenDeathPlayersNames, minThrowsValueOfCurrentSuddenDeathRound);
    // console.log(this.passedPlayersNames, this.failedPlayersNames);
    if (suddenDeathRoundFinished) {
      this.showUnfinishedErrorText = false;
      passedSuddenDeathPlayersNames.forEach(passedSuddenDeathPlayersName => {
        this.passedPlayersNames.push(passedSuddenDeathPlayersName);
        this.failedPlayersNames = this.failedPlayersNames.filter(name => name !== passedSuddenDeathPlayersName);
      });

      // console.log(this.passedPlayersNames, this.failedPlayersNames);
      if (failedSuddenDeathPlayersNames.length !== 1) {
        this.currentSuddenDeathRoundIndex++;
      } else {
        this.activateNextStep(suddenDeathRoundFinished);
      }
    } else {
      this.showUnfinishedErrorText = true;
    }
  }

  // ############################ Rundenauswertung #############################

  activateNextStep(roundFinished: boolean) {
    if (roundFinished) {
      this.showUnfinishedErrorText = false;

      if (this.failedPlayersNames.length !== 1) {
        this.prepareSuddenDeath();
      } else {
        this.setPositionForFailedPlayer(this.failedPlayersNames[0]);
        this.failedPlayersNames = new Array<string>();

        this.showSuddenDeathFields = false;
        document.querySelector('.next-round .button').classList.add('hidden');

        this.setRoundFinished();

        if (this.passedPlayersNames.length !== 1) {
          this.setNextRounds(this.passedPlayersNames);
        } else {
          this.finishGame(this.passedPlayersNames[0]);
          setTimeout(() => {
            this.router.navigate(['/balkloppistan']);
          }, 500);
        }
        this.lockCurrentRound();
        this.updateTempVariables();
        localStorage.setItem(this.localStorageKeyBalkloppistan, JSON.stringify(this.balkloppistan));
      }
    } else {
      this.showUnfinishedErrorText = true;
    }
  }

  private prepareSuddenDeath() {
    this.showSuddenDeathFields = true;
  }

  private setPositionForFailedPlayer(failedPlayersName: string) {
    this.balkloppistan = Object.assign(new Balkloppistan(), this.balkloppistan); // shitty workaround
    this.balkloppistan.getPlayerByName(failedPlayersName).position
      = this.balkloppistan.players.length + 1 - this.balkloppistan.currentRoundIndex;
    this.balkloppistan.getPlayerByName(failedPlayersName).lastRound
      = this.balkloppistan.currentRoundIndex;
    // TODO Positionierung wenn mehr als 1 Spiel in der Runde rausfliegt
  }

  setRoundFinished() {
    this.balkloppistan.players.forEach(player => {
      const currentPlayerRound = player.rounds[this.balkloppistan.currentRoundIndex];
      if (currentPlayerRound !== undefined) {
        currentPlayerRound.finished = true;
      }
    });
  }

  private setNextRounds(passedPlayerNames) {
    passedPlayerNames.forEach(passedPlayerName => {
      this.balkloppistan
        .getPlayerByName(passedPlayerName)
        .rounds
        .push(new BalkloppistanRound(this.balkloppistan.currentRoundIndex));
    });
  }

  private finishGame(winnerName: string) {
    this.balkloppistan = Object.assign(new Balkloppistan(), this.balkloppistan); // shitty workaround
    this.balkloppistan.getPlayerByName(winnerName).position = 1;
    this.balkloppistan.getPlayerByName(winnerName).lastRound = this.balkloppistan.currentRoundIndex;
    this.balkloppistan.finished = true;
    this.balkloppistanService.updateBalkloppistan(this.balkloppistan);
    localStorage.removeItem(this.localStorageKeyBalkloppistan);
  }

  private updateTempVariables() {
    this.balkloppistan.incrementRoundIndex();
  }

  private lockCurrentRound() {
    this.balkloppistan.players.forEach(player => {
      if (player.rounds[this.balkloppistan.currentRoundIndex - 1]) {
        player.rounds[this.balkloppistan.currentRoundIndex - 1].finished = true;
      }
    });
  }

}
