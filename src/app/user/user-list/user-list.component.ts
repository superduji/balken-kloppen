import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];
  nameSortFactor;
  teamSortFactor;
  handSortFactor;
  nameSorted;
  teamSorted;
  handSorted;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getAllAscendingByName()
      .subscribe(actions => this.users = this.userService.getUsersFromActions(actions));

    this.nameSortFactor = 1;
    this.teamSortFactor = 1;
    this.handSortFactor = 1;
  }

  sort(type) {
    const that = this;

    if (type === 'name') {
      this.users = this.users.sort((a, b) => {
        if (a.name < b.name) {
          return -1 * that.nameSortFactor;
        }
        if (a.name > b.name) {
          return 1 * that.nameSortFactor;
        }
        return 0;
      });
      this.nameSortFactor = this.nameSortFactor * -1;
      this.teamSortFactor = 1;
      this.handSortFactor = 1;
      this.nameSorted = this.nameSortFactor;
      this.teamSorted = undefined;
      this.handSorted = undefined;
    }

    if (type === 'team') {
      this.users = this.users.sort((a, b) => {
        if (a.team < b.team) {
          return -1 * that.teamSortFactor;
        }
        if (a.team > b.team) {
          return 1 * that.teamSortFactor;
        }
        return 0;
      });
      this.nameSortFactor = 1;
      this.teamSortFactor = this.teamSortFactor * -1;
      this.handSortFactor = 1;
      this.nameSorted = undefined;
      this.teamSorted = this.teamSortFactor;
      this.handSorted = undefined;
    }

    if (type === 'hand') {
      this.users = this.users.sort((a, b) => {
        if (a.hand < b.hand) {
          return -1 * that.handSortFactor;
        }
        if (a.hand > b.hand) {
          return 1 * that.handSortFactor;
        }
        return 0;
      });
      this.nameSortFactor = 1;
      this.teamSortFactor = 1;
      this.handSortFactor = this.handSortFactor * -1;
      this.nameSorted = undefined;
      this.teamSorted = undefined;
      this.handSorted = this.handSortFactor;
    }
  }

  showLargeLogo(event, user) {
    const logoContainer = document.querySelector('.team-logo__container');
    const playSoundButton = document.querySelector('.team-logo__play-sound');
    const img = logoContainer.querySelector('.team-logo--large');
    const src = event.target.src;
    const navigation = document.querySelector('.navigation');
    const navigationMarginValue = window.getComputedStyle(navigation).getPropertyValue('margin-bottom');
    const navigationHeight = (navigation as HTMLElement).offsetHeight + parseInt(navigationMarginValue, 10);

    img.setAttribute('src', src.replace('_s.', '.'));
    (logoContainer as HTMLElement).style.top = `${window.pageYOffset - navigationHeight}px`;
    logoContainer.classList.remove('hidden');
    document.body.style.overflowY = 'hidden';
    (playSoundButton as HTMLElement).dataset.soundName = user.soundFinish;
  }
}
