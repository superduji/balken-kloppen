import {Component} from '@angular/core';

import {User} from '../user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent {

  user: User = new User();
  submitted = false;

  constructor(private userService: UserService) { }

  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  save() {
    this.userService.createUser(this.user);
    this.user = new User();
  }

}
