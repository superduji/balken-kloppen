import {Component, Input} from '@angular/core';
import {User} from './user';
import {GameSoundService} from '../games/game-sound.service';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  loginService: LoginService;

  @Input() user: User;

  constructor(private gameSoundService: GameSoundService, loginService: LoginService) {
    this.gameSoundService = gameSoundService;
    this.loginService = loginService;
  }

  closeTeamLogo() {
    const logoContainer = document.querySelector('.team-logo__container');
    logoContainer.classList.add('hidden');
    document.body.style.overflowY = 'visible';
  }

  playPlayerSound(event) {
    const button = event.target;
    const soundName = button.dataset.soundName;
    const logo = button.previousElementSibling;
    this.gameSoundService.playPlayerSound(logo, soundName);
  }
}
