import {Injectable} from '@angular/core';
import {Action, AngularFirestore, DocumentChangeAction, DocumentSnapshot} from '@angular/fire/firestore';
import {User} from './user';
import {Observable} from 'rxjs';
import {DbService} from '../utils/db.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private db: AngularFirestore;
  private dbPath = '/user';

  constructor(private angularFirestore: AngularFirestore,
              private dbService: DbService) {
    this.db = angularFirestore;
  }

  getAllAscendingByName(): Observable<DocumentChangeAction<User>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('name', 'asc'))
      .snapshotChanges() as Observable<DocumentChangeAction<User>[]>;
  }

  getUserByKey(key): Observable<Action<DocumentSnapshot<User>>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .snapshotChanges() as Observable<Action<DocumentSnapshot<User>>>;
  }

  async getUserByKeyAsync(key): Promise<DocumentSnapshot<User>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .ref.get() as Promise<DocumentSnapshot<User>>;
  }

  createUser(user: User): void {
    this.dbService.setDocumentWithCustomKey(this.db, this.dbPath, user.name, user);
  }

  updateUser(user: User): void {
    this.dbService.updateDocument(this.db, this.dbPath, user.name, user);
  }

  getUsersFromActions(actions: DocumentChangeAction<User>[]): User[] {
    return actions.map(this.getUserFromAction);
  }

  private getUserFromAction(action: DocumentChangeAction<User>): User {
    const key = action.payload.doc.id;
    const user = action.payload.doc.data() as User;
    user.key = key;
    return user;
  }
}
