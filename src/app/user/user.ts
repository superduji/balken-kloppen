export class User {
  key: string;
  name: string;
  team: string;
  teamShort: string;
  teamBlazonPath: string;
  // avatar: string;
  hand: string;
  ligaRefs: Array<string>;
  balkloppistanKeys: Array<string>;
  cupRefs: Array<string>;
  testGamesRefs: Array<string>;
  inactive: boolean;
  soundFinish: string;
}
