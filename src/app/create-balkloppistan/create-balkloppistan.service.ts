import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CreateBalkloppistanService {

  balklopistanKey: string;

  setBalkloppistsnKey(balklopistanKey: string): void {
    this.balklopistanKey = balklopistanKey;
  }

  getBalkloppistanKey(): string {
    return this.balklopistanKey;
  }

  clearAll() {
    this.balklopistanKey = undefined;
  }

}
