import {Component} from '@angular/core';

import {User} from '../user/user';
import {UserService} from '../user/user.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Balkloppistan} from '../balkloppistan/balkloppistan';
import {BalkloppistanService} from '../balkloppistan/balkloppistan.service';
import {LoginService} from '../login/login.service';
import {CreateBalkloppistanService} from './create-balkloppistan.service';

@Component({
  selector: 'app-create-balkloppistan',
  templateUrl: './create-balkloppistan.component.html',
  styleUrls: ['./create-balkloppistan.component.css']
})
export class CreateBalkloppistanComponent {

  loginService: LoginService;

  balkloppistan: Balkloppistan = new Balkloppistan();
  users: User[];

  form: FormGroup;
  name: FormControl;
  player: FormArray;

  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private balkloppistanService: BalkloppistanService,
              private createBalkloppistanService: CreateBalkloppistanService,
              private router: Router,
              loginService: LoginService) {
    this.loginService = loginService;

    // retrieve users from userservice
    this.userService.getAllAscendingByName()
      .subscribe(actions => {
        this.users = this.userService.getUsersFromActions(actions).filter(user => !user.inactive);
      });

    // build formControl for name-field
    this.name = new FormControl(this.balkloppistan.name, [Validators.required]);

    // build complete formGroup (temp. with empty players-array)
    this.form = this.formBuilder.group({
      name: this.name,
      players: new FormArray([])
    });

    // after waiting for async-answer from userService, fill players-formArray with checkboxes
    setTimeout(() => {
      this.addCheckboxes();
    }, 500);
  }


  private addCheckboxes() {
    this.users.forEach(() => {
      const control = new FormControl();
      (this.form.controls.players as FormArray).push(control);
    });
  }


  onSubmit() {
    this.submitted = true;
    const userKeys = this.form.value.players
      .map((v, i) => v ? this.users[i].key : null)
      .filter(v => v !== null);

    // save in DB
    const name = this.form.value.name;
    const key = name.replace('/', '-');
    this.save(name, key, this.shuffle(userKeys));

    // set for BalkloppistanGameComponent
    this.createBalkloppistanService.setBalkloppistsnKey(key);

    // "redirect" to BalkloppistanGameComponent
    setTimeout(() => {
      this.router.navigate(['/balkloppistan-game']);
    }, 500);
  }

  private save(name, key, userKeys) {
    this.balkloppistanService.createBalkloppistan(name, key, userKeys);
  }

  private shuffle(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

}
