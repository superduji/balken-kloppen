export class TickerMeldung {
  content: string;
  href: string;
  id: number;
  type: string;

  constructor(content: string, href: string, id: number) {
    this.content = content;
    this.href = href;
    this.id = id;
  }

}
