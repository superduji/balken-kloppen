import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ArticleService} from '../news/article.service';
import {Article} from '../news/article';
import {Comment} from './comment';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  id: number;
  articles: Article[];
  article: Article = new Article(
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    false,
    false,
    null,
    null,
    null,
    '',
    null,
    null
  );
  defaultImage = 'https://firebasestorage.googleapis.com/v0/b/balken-kloppen.appspot.com/o/'
    + 'placeholder-image.png?alt=media&token=b0d8ab73-9d39-4dcb-bd50-c7b468573afe';
  staticTexts = {
    photoCopyright: 'Foto ©',
    readMore: 'mehr Infos',
    plus: 'BK+',
    author: 'Author',
    deleteComment: 'Möchtest du den Kommentar wirklich rauskloppen?'
  };
  showAddComment = false;
  comment = new Comment('', '', '', new Date(), Date.now().toString(10), 0);
  averageRating = 0;
  roundedAverageRating = [];
  error = false;
  @Output() cookieChange = new EventEmitter();

  constructor(private articleService: ArticleService,
              private route: ActivatedRoute,
              private loginService: LoginService) {
    this.route.params.subscribe(param => {
      this.id = parseInt(param.id, 10);
      this.articleService.getAllArticles()
        .subscribe(actions => {
            this.articles = this.articleService.getArticlesFromActions(actions);
            this.article = this.articles.find(article => article.id === this.id);
            this.articleService.updateArticlesCookie(this.id);
            this.getAverageRating();
          }
        );
    });
  }

  ngOnInit() {
  }

  getParts(article: Article): string[] {
    let parts = new Array<string>();

    if (article.paragraph) {
      parts = article.paragraph.replace('<br><br>', '<br> <br>').split('<br>');
    }

    parts.forEach((part, index) => {
      if (part.indexOf('<in>') > -1) {
        parts[index] = '<b>' + part.replace('<in>', article.intervieweeInitials + ': </b>');
      } else {
        parts[index] = '<b>' + article.author.toUpperCase() + ': </b>' + part;
      }
    });
    return parts;
  }

  addComment(): void {
    if (this.comment.author) {
      if (!this.article.comments) {
        this.article.comments = new Array<Comment>();
      }
      this.comment.id = this.article.comments.length + 1;
      this.article.comments.unshift(this.comment);
      this.articleService.updateArticle(this.article);
      this.comment = new Comment('', '', '', new Date(), Date.now().toString(10), 0);
      this.showAddComment = false;
    } else {
      this.error = true;
    }

  }

  getCommentDate(time: string): string {
    const date = new Date(time);
    const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : `${date.getMinutes()}`;
    if (this.isToday(date)) {
      return `heute, ${date.getHours()}:${minutes} Uhr`;
    } else {
      return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}, ${date.getHours()}:${minutes} Uhr`;
    }
  }

  isToday(date: Date): boolean {
    const today = new Date();
    return date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear();
  }

  setRating(rating: number): void {
    this.comment.rating = rating;
  }

  getAverageRating(): void {
    let sum = 0;
    if (this.article.comments) {
      this.article.comments.forEach(comment => sum += comment.rating ? comment.rating : 0);
      this.averageRating = sum / this.article.comments.length;
    }
    this.getRoundedAverageRating();
  }

  getRoundedAverageRating(): void {
    this.roundedAverageRating = new Array<number>(Math.ceil(this.averageRating));
  }

  deleteComment(id: number): void {
    if (window.confirm(this.staticTexts.deleteComment)) {
      this.article.comments = this.article.comments.filter(comment => id !== comment.id);
      this.articleService.updateArticle(this.article);
    }
  }

}
