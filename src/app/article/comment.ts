export class Comment {
  id: number;
  author: string;
  headline: string;
  text: string;
  time: Date;
  createTimeStamp: string;
  rating: number;

  constructor(author: string, headline: string, text: string, time: Date, createTimeStamp: string, rating: number) {
    this.author = author;
    this.headline = headline;
    this.text = text;
    this.time = time;
    this.createTimeStamp = createTimeStamp;
    this.rating = rating;
  }
}
