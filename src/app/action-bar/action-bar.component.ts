import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../news/article.service';
import {Article} from '../news/article';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

  LAST_VIEWED_COOKIE_NAME = 'viewedArticles';
  texts: object = {
    CONTENT_HEADLINE: 'Zuletzt gesehene Artikel',
    HEADLINE: 'Zuletzt gesehen'
  };

  articles: Article[];
  cookieValues: number[];
  open = false;

  constructor(private articleService: ArticleService,
              private cookieService: CookieService) {
    this.cookieValues = this.getCookieValues();
    this.getArticlesFromCookieValues();

    this.articleService.cookieChanged$.subscribe(() => {
      this.cookieValues = this.getCookieValues();
      this.getArticlesFromCookieValues();
    });

  }

  ngOnInit() {
  }

  getCookieValues(): number[] {
    return this.cookieService
      .get(this.LAST_VIEWED_COOKIE_NAME)
      .split(',')
      .map(value => parseInt(value, 10));
  }

  getArticlesFromCookieValues(): void {
    this.articleService.getAllArticles()
      .subscribe(actions => {
          this.articles = this.articleService
            .getArticlesFromActions(actions)
            .filter(article => this.cookieValues.includes(article.id))
            .reverse();
        }
      );
  }

  toggle(): void {
    this.open = !this.open;
  }

}
