import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  file: File;

  constructor(private angularFireStorage: AngularFireStorage) {
  }

  upload(file: File) {
    const ref = this.angularFireStorage.ref(file.name);

    return ref.put(file);
  }
}
