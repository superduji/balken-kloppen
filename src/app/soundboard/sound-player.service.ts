import { Injectable } from '@angular/core';
import {Sound} from './sound';

@Injectable({
  providedIn: 'root'
})
export class SoundPlayerService {

  audio;
  themeSound;
  themeAudio;

  constructor() {
    this.initializeTheme();
  }

  playSound(sound: Sound): void {
    this.audio = new Audio();
    this.audio.src = sound.path;
    this.audio.load();
    this.audio.play();
  }

  stopSound(): void {
    if (this.audio) {
      this.audio.pause();
    }
  }

  playPlayerSound(sound: Sound, logo): void {
    logo.classList.toggle('invert');
    const audio = new Audio();
    audio.src = sound.path;
    audio.load();
    audio.play();
    audio.addEventListener('ended', () => {
      logo.classList.toggle('invert');
    });
  }

  playSounds(sounds: Sound[]): void {
    let i = 0;
    const audio = new Audio();
    this.playAudio(audio, sounds[i]);
    audio.addEventListener('ended', () => {
      i++;
      if (sounds[i]) {
        this.playAudio(audio, sounds[i]);
      }
    });
  }

  playAudio(audio, sound) {
    audio.src = sound.path;
    audio.load();
    audio.play();
  }

  initializeSounds() {
    const sounds = new Map();
    sounds.set('Applaus', new Sound('Applaus', 'applause.mp3', SoundCategory.SUCCESS));
    sounds.set('Applaus kurz', new Sound('Applaus kurz', 'applause-short.mp3', SoundCategory.SUCCESS));
    sounds.set('Ausgleich', new Sound('Ausgleich', 'ausgleich.mp3', SoundCategory.INFO));
    sounds.set('Ausgleich Joschi', new Sound('Ausgleich Joschi', 'ausgleichjoschi.mp3', SoundCategory.INFO));
    sounds.set('Boah', new Sound('Boah', 'boah.mp3', SoundCategory.SUCCESS));
    sounds.set('Bockstark', new Sound('Bockstark', 'bockstark.mp3', SoundCategory.SUCCESS));
    sounds.set('Bog', new Sound('Bog', 'bog.mp3', SoundCategory.PLAYER));
    sounds.set('Bogdanov', new Sound('Bogdanov', 'bogdanov.mp3', SoundCategory.SUCCESS));
    sounds.set('Bogstark', new Sound('Bogstark', 'bogstark.mp3', SoundCategory.SUCCESS));
    sounds.set('Bogstarknow', new Sound('Bogstarknow', 'bogstarknow.mp3', SoundCategory.SUCCESS));
    sounds.set('Buuh', new Sound('Boo', 'boo.mp3', SoundCategory.PLAYER));
    sounds.set('Buzzer', new Sound('Buzzer', 'buzzer.mp3', SoundCategory.FAILURE));
    sounds.set('Das ist der Ausgleich', new Sound('Das ist der Ausgleich', 'unddasistderausgleich.mp3', SoundCategory.INFO));
    sounds.set('Düji', new Sound('Düji', 'düji_superstar.mp3', SoundCategory.PLAYER));
    sounds.set('Enttäuschung', new Sound('Enttäuschung', 'aaww.mp3', SoundCategory.FAILURE));
    sounds.set('Fertig werden', new Sound('Fertig werden', 'fertigwerden.mp3', SoundCategory.INFO));
    sounds.set('Freigabe', new Sound('Freigabe', 'freigabe.mp3', SoundCategory.INFO));
    sounds.set('Frohes Schaffen', new Sound('Frohes Schaffen', 'frohesschaffen.mp3', SoundCategory.INFO));
    sounds.set('Hahahaha', new Sound('Hahahaha', 'hahahaha.mp3', SoundCategory.FAILURE));
    sounds.set('Hände reiben', new Sound('Hände reiben', 'händereiben.mp3', SoundCategory.PLAYER));
    sounds.set('Hanke', new Sound('Hanke', 'hanke.mp3', SoundCategory.PLAYER));
    sounds.set('Mosi: Bäääh', new Sound('Mosi: Bäääh', 'mosi-bäääh.mp3', SoundCategory.MOSI));
    sounds.set('Mosi: Bööö', new Sound('Mosi: Bööö', 'mosi-bööö.mp3', SoundCategory.MOSI));
    sounds.set('Mosi: Hallo Leute', new Sound('Mosi: Hallo Leute', 'mosi-halloleute.mp3', SoundCategory.MOSI));
    sounds.set('Mosi: Husten', new Sound('Mosi: Husten', 'mosi-husten.mp3', SoundCategory.MOSI));
    sounds.set('Mosi: Wieder da', new Sound('Mosi: Wieder da', 'mosi-ichbinwiederda.mp3', SoundCategory.MOSI));
    sounds.set('Mosi: Mir gehtsch gut', new Sound('Mosi: Mir gehtsch gut', 'mosi-mirgehtschgut.mp3', SoundCategory.MOSI));
    sounds.set('Nächste Runde', new Sound('Nächste Runde', 'naechsterunde.mp3', SoundCategory.INFO));
    sounds.set('Nice', new Sound('Nice', 'nice.mp3', SoundCategory.SUCCESS));
    sounds.set('Ja', new Sound('Ja', 'ja.mp3', SoundCategory.SUCCESS));
    sounds.set('Jubel', new Sound('Jubel', 'jubel.mp3', SoundCategory.SUCCESS));
    sounds.set('Oh nein', new Sound('Oh nein', 'ohnein.mp3', SoundCategory.FAILURE));
    sounds.set('Oh no', new Sound('Oh no', 'ohno.mp3', SoundCategory.FAILURE));
    sounds.set('Reen', new Sound('Reen', 'reen.mp3', SoundCategory.PLAYER));
    sounds.set('Reen on fire', new Sound('Reen on fire', 'reenonfire.mp3', SoundCategory.PLAYER));
    sounds.set('Richtig gutes Zeug', new Sound('Richtig gutes Zeug', 'richtigguteszeug.mp3', SoundCategory.SUCCESS));
    sounds.set('Scheiße', new Sound('Scheiße', 'scheiße.mp3', SoundCategory.FAILURE));
    sounds.set('So close!', new Sound('So close!', 'soclose.mp3', SoundCategory.SUCCESS));
    sounds.set('Stark', new Sound('Stark', 'stark.mp3', SoundCategory.SUCCESS));
    sounds.set('Überwurf er', new Sound('Überwurf er', 'ueberwurf-er.mp3', SoundCategory.FAILURE));
    sounds.set('Überwurf sie', new Sound('Überwurf sie', 'ueberwurf-sie.mp3', SoundCategory.FAILURE));
    sounds.set('Wie macht er den denn', new Sound('Wie macht er den denn', 'wiemachterdendenn.mp3', SoundCategory.SUCCESS));
    sounds.set('Wie macht sie den denn', new Sound('Wie macht sie den denn', 'wiemachtsiedendenn.mp3', SoundCategory.SUCCESS));
    sounds.set('Yeah', new Sound('Yeah', 'yeah.mp3', SoundCategory.SUCCESS));
    sounds.set('Yoshi', new Sound('Yoshi', 'yoshi.mp3', SoundCategory.PLAYER));
    return sounds;
  }

  initializeTheme() {
    this.themeSound = new Sound('Theme', 'theme.mp3', SoundCategory.THEMES);
    this.themeAudio = new Audio();
    this.themeAudio.src = this.themeSound.path;
    this.themeAudio.volume = 0.1;
    this.themeAudio.load();
    this.themeAudio.addEventListener('ended', () => {
      this.themeAudio.load();
      this.themeAudio.play();
    });
  }

  toggleTheme(play) {
    if (play) {
      this.themeAudio.play();
    } else {
      this.themeAudio.pause();
    }
  }

  setThemeVolume(volume) {
    this.themeAudio.volume = volume;
  }

}
