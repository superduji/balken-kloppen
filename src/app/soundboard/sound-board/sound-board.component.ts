import { Component, OnInit } from '@angular/core';
import { SoundPlayerService} from '../sound-player.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-sound-board',
  templateUrl: './sound-board.component.html',
  styleUrls: ['./sound-board.component.scss']
})
export class SoundBoardComponent implements OnInit {

  sounds = new Map();
  soundBoardCategories = [
    SoundCategory.SUCCESS,
    SoundCategory.FAILURE,
    SoundCategory.INFO,
    SoundCategory.PLAYER,
    SoundCategory.MOSI
  ];
  open = false;

  constructor(private soundPlayerService: SoundPlayerService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.queryParams.subscribe((params: Params) => {
      this.open = params.soundboard === 'open';
      if (!this.open) {
        document.body.classList.remove('soundboard-open');
      } else {
        document.body.classList.add('soundboard-open');
      }
    });
    this.sounds = this.soundPlayerService.initializeSounds();
  }

  ngOnInit() {
  }

  playSound($event, name: string) {
    this.soundPlayerService.stopSound();
    const button = $event.target;
    button.classList.add('active');
    window.setTimeout(() => {
      button.classList.remove('active');
    }, 100);
    this.soundPlayerService.playSound(this.sounds.get(name));
  }

  toggleUrlQueryParam() {
    const paramValue = this.open ? 'open' : null;
    this.router.navigate([], {
      queryParams: {
        soundboard: paramValue
      },
      queryParamsHandling: 'merge',
    });
  }

  toggleSoundBoard() {
    this.open = !this.open;
    this.toggleUrlQueryParam();
    document.body.classList.toggle('soundboard-open');
  }

  getFilteredSounds(category) {
    const successSounds = new Map();

    this.sounds.forEach((sound, key) => {
      if (sound.category === category) {
        successSounds.set(key, sound);
      }
    });

    return successSounds;
  }
}
