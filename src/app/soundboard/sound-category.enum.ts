const enum SoundCategory {
  SUCCESS = 'success',
  FAILURE = 'failure',
  PLAYER = 'player',
  THEMES = 'themes',
  INFO = 'info',
  MOSI = 'mosi',
}
