export class Sound {

  name: string;
  base: string;
  path: string;
  category: string;

  constructor(name: string, path: string, category: string) {
    this.name = name;
    this.base = 'assets/sounds/';
    this.path = this.base + path;
    this.category = category;
  }
}
