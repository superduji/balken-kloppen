import {Component, OnInit} from '@angular/core';
import {KloppenService} from './kloppen.service';
import {UserService} from '../user/user.service';
import {User} from '../user/user';
import {LoginService} from '../login/login.service';
import {log} from 'util';

@Component({
  selector: 'app-kloppen',
  styleUrls: ['./kloppen.component.scss'],
  templateUrl: './kloppen.component.html'
})
export class KloppenComponent implements OnInit {

  loginService;

  private disciplines = [Discipline.FIFTEEN, Discipline.TEAM];
  private players: User[];
  private selectedDate;
  private selectedDiscipline;
  private selectedType: string;
  private selectedPlayers: Array<User>;
  private isLegacy;
  private event;


  constructor(private kloppenService: KloppenService,
              private userService: UserService,
              loginService: LoginService) {
    this.loginService = loginService;
  }


  ngOnInit() {
    this.userService.getAllAscendingByName()
      .subscribe(actions => {
        this.players = this.userService.getUsersFromActions(actions).filter(user => !user.inactive);
      });
    this.selectedPlayers = new Array<User>();
    this.selectedDate = new Date().toISOString().substr(0, 10);
  }

  getDiscipline(): string {
    return this.selectedDiscipline;
  }

  onDiscipline(discipline: string): void {
    this.selectedDiscipline = discipline;
    if (discipline === Discipline.FIFTEEN) {
      this.selectedType = Type.TEST;
    }
  }

  hasDiscipline(): boolean {
    if (this.selectedDiscipline) {
      return true;
    } else {
      return false;
    }
  }

  getType(): string {
    return this.selectedType;
  }

  hasType(): boolean {
    if (this.selectedType) {
      return true;
    } else {
      return false;
    }
  }

  onSelectedUser(player: User) {
    if (this.selectedPlayers.includes(player)) {
      this.selectedPlayers = this.selectedPlayers.filter(p => p !== player);
    } else {
      this.selectedPlayers.push(player);
    }
  }

  hasDate(): boolean {
    if (this.selectedDate) {
      return true;
    } else {
      return false;
    }
  }

  isSelected(user: User): User {
    if (this.selectedPlayers.includes(user)) {
      return user;
    }
  }

  hasEnoughPlayers(): boolean {
    return this.selectedPlayers.length === 2;
  }

  showLegacy(): boolean {
    return this.showTypes();
  }

  showTypes(): boolean {
    return this.hasDiscipline();
  }

  showPlayers(): boolean {
    return this.hasType();
  }

  hasEventIfNecessary(): boolean {
    return (this.getType() === Type.LEAGUE && this.event !== undefined && this.event.trim() !== '')
      || (this.getType() === Type.CUP && this.event !== undefined && this.event.trim() !== '')
      || (this.getType() === Type.TEST);
  }

  createEventName(event: any): string {
    if (this.getType() === Type.TEST) {
      return this.selectedType;
    }
    return this.selectedType + ' ' + event;
  }

  showKloppenButton(): boolean {
    this.kloppenService.setDate(this.selectedDate);
    this.kloppenService.setDiscipline(this.selectedDiscipline);
    this.kloppenService.setType(this.selectedType);
    this.kloppenService.setPlayers(this.selectedPlayers);
    this.kloppenService.setLegacy(this.isLegacy);
    this.kloppenService.setEvent(this.createEventName(this.event));

    return this.hasEnoughPlayers() && this.hasDate() && this.hasEventIfNecessary();
  }
}
