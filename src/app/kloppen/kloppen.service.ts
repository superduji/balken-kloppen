import {Injectable} from '@angular/core';
import {User} from '../user/user';

@Injectable({
  providedIn: 'root',
})
export class KloppenService {

  private date: string;
  private selectedDiscipline: string;
  private selectedType: string;
  private selectedPlayers: Array<User> = new Array<User>();
  private legacy: boolean;
  private event: string;
  private pokalInfos: object;

  getDate(): string {
    return this.date;
  }

  setDate(date: string): void {
    this.date = date;
  }

  getDiscipline(): string {
    return this.selectedDiscipline;
  }

  setDiscipline(value: string): void {
    this.selectedDiscipline = value;
  }

  getType(): string {
    return this.selectedType;
  }

  setType(type: string): void {
    this.selectedType = type;
  }

  getPlayers(): Array<User> {
    return this.selectedPlayers;
  }

  setPlayers(players: Array<User>): void {
    this.selectedPlayers = players;
  }

  isLegacy(): boolean {
    return this.legacy || false;
  }

  setLegacy(value: boolean) {
    this.legacy = value;
  }

  setEvent(event: string) {
    this.event = event;
  }

  getEvent(): string {
    return this.event;
  }

  setPokalInfos(pokalInfos: object) {
    this.pokalInfos = pokalInfos;
  }

  getPokalInfos(): object {
    return this.pokalInfos;
  }
}
