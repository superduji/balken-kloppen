import { Component, OnInit } from '@angular/core';
import {News} from "./news";

@Component({
  selector: 'app-news-ticker',
  templateUrl: './news-ticker.component.html',
  styleUrls: ['./news-ticker.component.scss']
})
export class NewsTickerComponent implements OnInit {

  texts;
  news = [];
  headline;

  constructor() {
    this.texts = {
      title: 'NEWS:',
      linkText: 'Zum Artikel'
    };
    this.news.push(
      new News(
        'Die IBKA®-Balken-Kloppen-Liga hat mit Thyssen Krupp Magnettechnik einen neuen Hauptsponsor gewonnen.',
        'http://www.thyssenkrupp-magnettechnik.com/index.php/hartferritmagnete.html',
        true
      ),
      new News(
        'Joschi gewinnt für den BuMS Hohentor Neustadt zum zweiten Mal den Karwenzel-Cup.',
        '/statistiken/spiele',
          false
      ),
      new News(
        'Azubi BC erhält für Carsten den Zuschlag für den letzten Liga-Startplatz für Saison 7.',
        '/liga',
        false
      )
    );
    this.headline = this.getRandomNews();
  }

  ngOnInit() {
  }

  private getRandomNews() {
    const random = Math.floor(Math.random() * Math.floor(this.news.length));
    return this.news[random];
  }

}
