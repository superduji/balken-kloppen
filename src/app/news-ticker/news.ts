export class News {
  text: string;
  href: string;
  extern: boolean;

  constructor(text: string, href: string, extern: boolean) {
    this.text = text;
    this.href = href;
    this.extern = extern;
  }
}
