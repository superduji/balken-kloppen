import {Injectable} from '@angular/core';
import {Player} from '../models/boundary/player.model';

@Injectable({
  providedIn: 'root'
})

export class GameFilteringService {

  isAHitOfFiltering(gameDataWrapper, filterKey: string, value: string): boolean {
    console.log('filtering... ' + filterKey + ': ' + value);
    const gameData = gameDataWrapper.payload.doc.data();

    switch (filterKey) {
      case 'key': {
        return gameDataWrapper.payload.doc.id === value;
      }
      case 'id': {
        return gameData.id === parseInt(value, 10);
      }
      case 'type': {
        return gameData.type === value;
      }
      case 'date': {
        return gameData.date === value;
      }
      case 'playerName': {
        const playerData1 = (gameData.players[0] as Player);
        const playerData2 = (gameData.players[1] as Player);
        return playerData1.name === value || playerData2.name === value;
      }
      default: {
        return true;
      }
    }
  }

}
