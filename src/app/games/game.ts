import {Player} from '../models/boundary/player.model';

export class Game {
  key: string;
  id: number;
  type: string;
  event: string;
  date: string;
  players: Array<Player>;
  winner: string;
  time: string;
  draw: boolean;
  legacy: boolean;
  suddenDeath: boolean;

  constructor(key: string,
              id: number,
              type: string,
              event: string,
              date: string,
              players: Array<Player>,
              winner: string,
              time: string,
              draw: boolean,
              legacy: boolean,
              suddenDeath: boolean) {
    this.key = key;
    this.id = id;
    this.type = type;
    this.event = event;
    this.date = date;
    this.players = players;
    this.winner = winner;
    this.time = time;
    this.draw = draw;
    this.legacy = legacy;
    this.suddenDeath = suddenDeath;
  }

}
