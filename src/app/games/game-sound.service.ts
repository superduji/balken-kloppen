import {Injectable} from '@angular/core';
import {SoundPlayerService} from '../soundboard/sound-player.service';
import {Sound} from '../soundboard/sound';
import {Player} from '../models/boundary/player.model';

@Injectable({
  providedIn: 'root'
})
export class GameSoundService {
  sounds;
  settings = {
    sounds: true
  };
  generalThrowSounds = {
    zero: ['Enttäuschung', 'Buzzer', 'Oh no'],
    one: ['Applaus kurz', 'Ja', 'Nice'],
    two: ['Stark', 'So close!', 'Richtig gutes Zeug'],
    three: ['Wie macht er den denn', 'Boah', 'Yeah'],
    threeFem: 'Wie macht sie den denn',
    exceed: ['Überwurf er', 'Überwurf sie'],
    equal: ['Ausgleich', 'Ausgleich Joschi', 'Das ist der Ausgleich']
  };
  playerSpecificThrowSounds = {
    dueji: 'Scheiße',
    jhv: 'Hahahaha',
    boggi: ['Bogstark', 'Bogstarknow']
  };

  constructor(private soundPlayerService: SoundPlayerService) {
    this.sounds = this.soundPlayerService.initializeSounds();
    this.addRestSounds();
  }

  setSoundSettings(input) {
    this.settings.sounds = input.checked;
  }

  playThrowSounds(throwData) {
    if (this.settings.sounds) {
      const maximalNochZuErreichendePunkte = (5 - throwData.activeRound.throwCount) * 3;
      const kannDieseRundeNichtMehrFertigWerden = throwData.restCount > maximalNochZuErreichendePunkte;
      const sounds = [];
      let random = Math.floor(Math.random() * Math.floor(3));

      // count specific sounds
      if (throwData.count === 0) {
        if (this.shouldPlayPlayerSpecificThrowSound(throwData)) {
          sounds.push(this.playPlayerSpecificThrowSound(throwData));
        } else {
          sounds.push(this.sounds.get(this.generalThrowSounds.zero[random]));
        }
      } else if (throwData.count === 1) {
        sounds.push(this.sounds.get(this.generalThrowSounds.one[random]));
      } else if (throwData.count === 2) {
        sounds.push(this.sounds.get(this.generalThrowSounds.two[random]));
      } else {
        const laeuftBeiIhrIhm = throwData.previousThrowCount === 3 && throwData.activeRound.throwCount !== 1;
        if (laeuftBeiIhrIhm) {
          if (throwData.activePlayer.name === 'Boggi') {
            sounds.push(this.sounds.get(this.playerSpecificThrowSounds.boggi[1]));
          } else {
            sounds.push(this.sounds.get('Bockstark'));
          }
        } else {
          if (this.isFemalePlayer(throwData.activePlayer)) {
            sounds.push(this.sounds.get(this.generalThrowSounds.threeFem));
          } else if (throwData.activePlayer.name === 'Boggi') {
            sounds.push(this.sounds.get(this.playerSpecificThrowSounds.boggi[0]));
          } else {
            random = Math.floor(Math.random() * Math.floor(this.generalThrowSounds.three.length));
            sounds.push(this.sounds.get(this.generalThrowSounds.three[random]));
          }
        }
      }

      if (this.shouldPlayAusgleichSound(throwData)) {
        if (throwData.restCount > 5) {
          sounds.push(this.sounds.get(this.generalThrowSounds.equal[random]));
        } else {
          sounds.push(this.sounds.get(this.generalThrowSounds.equal[2]));
        }
      }

      if (kannDieseRundeNichtMehrFertigWerden && !throwData.activeRound.reminderNextRound) {
        throwData.activeRound.reminderNextRound = true;
        sounds.push(this.sounds.get('Nächste Runde'));
      }

      if ((throwData.restCount <= 5 || throwData.activeRound.throwCount === 5) && throwData.restCount > 0) {
        if (throwData.restCount <= 3 && throwData.activeRound.throwCount !== 5 && random === 2) {
          sounds.push(this.sounds.get('Hände reiben'));
        }
        sounds.push(this.sounds.get(`Noch ${throwData.restCount}`));
      } else if (throwData.restCount === 0) {
        sounds.push(this.sounds.get(throwData.activePlayer.soundFinish));
      }
      this.soundPlayerService.playSounds(sounds);
    }
  }

  shouldPlayAusgleichSound(throwData) {
    return throwData.restCount !== 15 && throwData.count !== 0 && throwData.equal;
  }

  playExceedSounds(startRest: number, activePlayer: Player) {
    if (this.settings.sounds) {
      const sounds = [];

      if (this.isFemalePlayer(activePlayer)) {
        sounds.push(this.sounds.get(this.generalThrowSounds.exceed[1]));
      } else {
        sounds.push(this.sounds.get(this.generalThrowSounds.exceed[0]));
      }
      sounds.push(this.sounds.get(`Noch ${startRest}`));
      this.soundPlayerService.playSounds(sounds);
      }
  }

  playPlayerSound(logo, soundName) {
    if (this.settings.sounds) {
      if (soundName) {
        this.soundPlayerService.playPlayerSound(this.sounds.get(soundName), logo);
      } else {
        if (logo) {
          logo.classList.toggle('invert');
        }
      }
    }
  }

  playRoundSound(activeRest) {
    if (this.settings.sounds) {
      this.soundPlayerService.playSound(this.sounds.get(`Noch ${activeRest}`));
    }
  }

  private playPlayerSpecificThrowSound(throwData) {
    if (throwData.activePlayer.key === 'JHV') {
      return this.sounds.get(this.playerSpecificThrowSounds.jhv);
    } else if (throwData.activePlayer.key === 'Düji') {
      return this.sounds.get(this.playerSpecificThrowSounds.dueji);
    }
  }

  private addRestSounds() {
    for (let i = 0; i <= 15; i++) {
      this.sounds.set(`Noch ${i}`, new Sound(`Noch ${i}`, `rest/Noch${i}.mp3`, SoundCategory.INFO));
    }
  }

  private shouldPlayPlayerSpecificThrowSound(throwData) {
    return throwData.activePlayer.key === 'JHV' || throwData.activePlayer.key === 'Düji';
  }

  private isFemalePlayer(activePlayer) {
    return activePlayer.key === 'Anne'
        || activePlayer.key === 'Inga'
        || activePlayer.key === 'Scsskia'
        || activePlayer.key === 'Steffi';
  }
}
