import { TestBed } from '@angular/core/testing';

import { GameSoundService } from './game-sound.service';

describe('GameSoundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameSoundService = TestBed.get(GameSoundService);
    expect(service).toBeTruthy();
  });
});
