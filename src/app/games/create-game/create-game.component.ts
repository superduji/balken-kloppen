import {AfterViewInit, Component, HostListener, OnInit} from '@angular/core';
import {GameService} from '../game.service';
import {Game} from '../game';
import {Game as GameModel} from '../../models/boundary/game.model';
import {KloppenService} from '../../kloppen/kloppen.service';
import {SoundPlayerService} from '../../soundboard/sound-player.service';
import {UserService} from '../../user/user.service';
import {User} from '../../user/user';
import {Player} from '../../models/boundary/player.model';
import {Liga} from '../../create-liga/liga';
import {GameSoundService} from '../game-sound.service';
import {Router} from '@angular/router';
import {CreateGameUpdateLigaService} from './create-game-update-liga.service';
import {LoginService} from '../../login/login.service';
import {CupService} from '../../create-pokal/cup.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

export enum KEY_CODE {
  NUMPAD_0 = 'Numpad0',
  NUMPAD_1 = 'Numpad1',
  NUMPAD_2 = 'Numpad2',
  NUMPAD_3 = 'Numpad3'
}

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.scss'],
  animations: [
    trigger('fadeIn', [
      state('void', style({
        opacity: 0
      })),
      transition('void => *', animate(300)),
    ]),
  ]
})
export class CreateGameComponent implements OnInit, AfterViewInit {

  loginService: LoginService;

  ACTIVE_CLASS = 'active';
  WIN_CLASS = 'win';
  DATABASE_GAME_HREF = 'https://console.firebase.google.com/project/balken-kloppen/database/firestore/data~2Fgame~2F';

  activeArea: Element;
  activeAreaId: number;
  activeRound;
  activeRoundSumCount: number;
  buttons = Array(4);
  game: Game;
  finishedPlayers: Player[] = [];
  games: GameModel[];
  nonActiveArea: Element;
  playerAreas: NodeListOf<Element>;
  previousThrowCount: number;
  sounds = new Map();
  startPoints = 15;
  throws = Array(5);
  users: User[];

  confirmation = false;
  confirmationId: string;
  editRounds = false;
  enableKloppen = false;
  showLigaButton = false;
  submitted = false;
  suddenDeath = false;

  constructor(private gameService: GameService,
              private kloppenService: KloppenService,
              private soundPlayerService: SoundPlayerService,
              private createGameUpdateLigaService: CreateGameUpdateLigaService,
              private userService: UserService,
              private gameSoundService: GameSoundService,
              private router: Router,
              loginService: LoginService,
              private cupService: CupService) {
    this.loginService = loginService;
    this.getGames();
  }

  ngOnInit(): void {
    this.users = this.getUsers();
    this.game = this.getNewGame();
    this.saveDataForPageReload();
  }

  ngAfterViewInit() {
    this.playerAreas = document.querySelectorAll('.game__player-area');
  }

  getGames(): void {
    // if (localStorage.getItem('games')) {
    //   this.games = JSON.parse(localStorage.getItem('games'));
    // } else {
      this.gameService.getAllDescendingById()
        .subscribe(actions => {
          this.games = this.gameService.getGamesFromActions(actions);
          // localStorage.setItem('games', JSON.stringify(this.games));
        });
    // }
  }

  getNewGame(): Game {
    return new Game(
      null,
      null,
      this.getType(),
      this.getEvent(),
      this.getDate(),
      this.getPlayersFromUsers(),
      null,
      null,
      false,
      this.isLegacy(),
      false
    );
  }

  getPlayersFromUsers(): Player[] {
    const players = new Array<Player>();
    this.users.forEach(user => players.push(
      new Player(
        user.name,
        user.team,
        this.startPoints,
        0,
        0,
        new Array<string>(),
        new Array<string>(),
        false
      )
    ));
    console.log(players);
    return players;
  }

  /*   ¯\_(ツ)_/¯   */
  getRest(player: Player): number {
    let rest = 15;

    for (let i = 0; i < player.throws.length; i = i + 5) {
      let validRound = true;
      let roundSum = 0;
      const start = i;
      for (let j = 0; j < 5; j++) {
        if (player.throws[start + j]) {
          if (player.throws[start + j].indexOf('Ü') > -1 || player.throws[start + j] === 'X') {
            validRound = false;
          } else {
            roundSum += parseInt(player.throws[start + j], 10);
          }
        }
      }
      if (validRound) {
        rest -= roundSum;
      }
    }

    player.rest = rest;
    return rest;
  }

  save(): void {
    this.game.id = this.games[0].id + 1;
    this.gameService.createGame(this.game, this.showConfirmation.bind(this));

    if (this.game.type === Type.LEAGUE) {
      const ligaKey = this.game.event.replace('Liga ', '').replace('/', '-');
      this.createGameUpdateLigaService.calculateLigaTableAndSetGameKeyToLigaGameTableGame(this.game, ligaKey);
    } else if (this.game.type === Type.CUP) {
      this.cupService.addGameToCup(this.game, this.getPokalInfos());
      return;
    }

    this.showLigaButton = true;
    this.game = this.getNewGame();
  }

  showConfirmation(id: string): void {
    this.confirmation = true;
    this.confirmationId = id;
  }

  onSubmit(): void {
    this.submitted = true;
    this.soundPlayerService.toggleTheme(false);
    this.save();
  }

  isTestMatch(): boolean {
    return Type.TEST === this.game.type;
  }

  // Todo: REFACTOR
  playReturnMatch(): void {
    this.kloppenService.setPlayers(this.users.reverse());
    this.router.navigate(['/addGame']);
  }

  animateButton(button: HTMLElement) {
    if (button) {
      button.classList.add(this.ACTIVE_CLASS);
      setTimeout(() => {
        button.classList.remove(this.ACTIVE_CLASS);
      }, 200);
    }
  }

  playPlayerSound(eventTarget: HTMLElement, gamePlayer: Player) {
    const player = this.users.find(user => user.name === gamePlayer.name);
    this.gameSoundService.playPlayerSound(eventTarget, player.soundFinish);
  }

  // Wenn ich diese enterThrow function sehe, schießen mir Tränen der Freude in die Augen!
  enterThrow($event: Event, count: number): void {
    if ($event) {
      const button = $event.target as HTMLElement;
      this.animateButton(button);
    }

    const validThrow = this.activeRound.throwCount < 5 && this.game.players[this.activeAreaId].throws.length < 30;

    if (validThrow) {
      this.activeArea = document.querySelectorAll('.game__player-area')[this.activeAreaId];
      this.saveRoundStartRest(this.activeAreaId);

      const activePlayer = this.game.players[this.activeAreaId];

      if (this.suddenDeath) {
        this.handleSuddenDeathThrow(activePlayer, count);
      } else {
        this.handleRegularThrow(activePlayer, count);
        this.evaluateResult();

        if (this.activeRound.throwCount === 5) {
          this.activeArea.classList.remove(this.ACTIVE_CLASS);
        }
        this.previousThrowCount = count;

        if (this.game.players[0].throws.length === 30 && this.game.players[1].throws.length === 30) {
          if (this.game.type === Type.CUP || this.game.type === Type.TEST) {
            this.suddenDeath = true;
          } else {
            this.game.draw = true;
          }
        }
      }
    }
  }

  handleRegularThrow(activePlayer: Player, count: number): void {
    activePlayer.rest -= count;
    const restCount = activePlayer.rest;

    if (activePlayer.rest < 0) {
      this.handleExceed(count);
    } else {
      activePlayer.throws.push(count.toString());
      this.activeRound.throwCount++;
      activePlayer.numberOfThrows = activePlayer.throws.length;

      const throwData = {
        count,
        restCount,
        activePlayer: this.users.find(user => user.key === activePlayer.name),
        activeRound: this.activeRound,
        previousThrowCount: this.previousThrowCount,
        equal: this.game.players[0].rest === this.game.players[1].rest
      };
      this.gameSoundService.playThrowSounds(throwData);
    }
  }

  handleSuddenDeathThrow(activePlayer: Player, count: number): void {
    this.game.suddenDeath = true;
    activePlayer.suddenDeathThrows.push(count.toString());
    if (this.activeAreaId === 1) {
      const gewonnen = count > parseInt(
        this.game.players[0].suddenDeathThrows[this.game.players[0].suddenDeathThrows.length - 1],
        10
      );
      const verloren = count < parseInt(
        this.game.players[0].suddenDeathThrows[this.game.players[0].suddenDeathThrows.length - 1],
        10
      );
      this.activeArea.classList.remove(this.ACTIVE_CLASS);

      if (gewonnen) {
        this.game.winner = this.game.players[1].name;
        this.activeArea.classList.add(this.WIN_CLASS);
      } else if (verloren) {
        this.game.winner = this.game.players[0].name;
        this.nonActiveArea = document.querySelectorAll('.game__player-area')[0];
        this.nonActiveArea.classList.add(this.WIN_CLASS);
      }
    }
  }

  handleExceed(count: number): void {
    const activePlayer = this.game.players[this.activeAreaId];
    const activePlayerThrows = activePlayer.throws;
    activePlayerThrows.push(`Ü${count}`);
    this.activeRound.throwCount = 5;
    const throwsLength = activePlayerThrows.length;
    const throwsToFill = throwsLength % 5 > 0 ? 5 - (throwsLength % 5) : throwsLength % 5;
    activePlayer.numberOfThrows = activePlayerThrows.length + throwsToFill;

    // Fill round with X and reset rest
    for (let i = 0; i < throwsToFill; i++) {
      activePlayerThrows.push('X');
    }
    activePlayer.rest = this.activeRound.startRest;

    this.gameSoundService.playExceedSounds(this.activeRound.startRest, activePlayer);
  }

  evaluateResult(): void {
    const activePlayer = this.game.players[this.activeAreaId];
    const newRestCount = activePlayer.rest;
    if (newRestCount === 0) {
      this.activeArea.classList.remove(this.ACTIVE_CLASS);
      this.finishedPlayers.push(activePlayer);
      this.activeRound.throwCount = 5;

      if (this.activeAreaId === 0) {
        // IF SPIELER 0
        const maximalNochZuSpielendeWuerfe = this.game.players[0].throws.length - this.game.players[1].throws.length;
        const maximalNochZuErreichendePunkte = maximalNochZuSpielendeWuerfe * 3;
        const kannGegnerNichtMehrFeddichWerden = maximalNochZuErreichendePunkte < this.game.players[1].rest;
        if (kannGegnerNichtMehrFeddichWerden) {
          this.game.winner = activePlayer.name;
          this.activeArea.classList.remove(this.ACTIVE_CLASS);
          this.activeArea.classList.add(this.WIN_CLASS);
        }
      } else {
        // IF SPIELER 1
        const istGegnerFeddich = this.game.players[0].rest === 0;
        if (!istGegnerFeddich) {
          this.game.winner = activePlayer.name;
          this.activeArea.classList.remove(this.ACTIVE_CLASS);
          this.activeArea.classList.add(this.WIN_CLASS);
        } else {
          if (activePlayer.throws.length < this.game.players[0].throws.length) {
            this.game.winner = activePlayer.name;
            this.activeArea.classList.remove(this.ACTIVE_CLASS);
            this.activeArea.classList.add(this.WIN_CLASS);
          } else {
            if (this.game.type === Type.CUP || this.game.type === Type.TEST) {
              this.suddenDeath = true;
            } else {
              this.game.draw = true;
            }
          }
        }
      }
    } else if (newRestCount > 0) {
      if (this.activeAreaId === 1) {
        const istGegnerSchonFeddich = this.game.players[0].rest === 0;
        if (istGegnerSchonFeddich) {
          const maximalNochZuSpielendeWuerfe = this.game.players[0].throws.length - this.game.players[1].throws.length;
          const maximalNochZuErreichendePunkte = maximalNochZuSpielendeWuerfe * 3;
          const kannNichtMehrFeddichWerden = maximalNochZuErreichendePunkte < this.game.players[1].rest;
          if (kannNichtMehrFeddichWerden) {
            this.game.winner = this.game.players[0].name;
            this.activeArea.classList.remove(this.ACTIVE_CLASS);
            this.nonActiveArea = document.querySelectorAll('.game__player-area')[0];
            this.nonActiveArea.classList.add(this.WIN_CLASS);
          }
        }
      }
    }
  }

  getRoundSum(throws: string[], start: number): number {
    let sum = 0;
    for (let i = start; i < start + 5; i++) {
      if (throws[i]) {
        if (throws[i].indexOf('Ü') > -1) {
          return 0;
        } else {
          sum += parseInt(throws[i], 10);
        }
      }
    }
    return sum;
  }

  saveRoundStartRest(activeAreaId: number): void {
    if (this.activeRound.throwCount === 0) {
      this.activeRound.startRest = this.game.players[activeAreaId].rest;
    }
  }

  toggleKloppen(event: Event, playerId: number): void {
    const activeArea = (event.target as Element).closest('.game__player-area');
    this.activeAreaId = playerId;
    const activeRest = this.game.players[playerId].rest;
    if (activeArea.classList.contains(this.ACTIVE_CLASS)) {
      this.disableKloppen();
    } else {
      this.activateKloppen(activeArea, activeRest);
    }
  }

  activateKloppen(activeArea: Element, activeRest: number): void {
    this.disableAllPlayerAreas();
    activeArea.classList.add(this.ACTIVE_CLASS);
    this.enableKloppen = true;
    if (!this.activeRound || this.activeRound.throwCount === 5) {
      this.activeRound = {};
      this.activeRound.throwCount = 0;
    }
    this.activeRoundSumCount = 0;
    this.gameSoundService.playRoundSound(activeRest);
  }

  disableKloppen(): void {
    this.disableAllPlayerAreas();
    this.enableKloppen = false;
  }

  disableAllPlayerAreas(): void {
    this.playerAreas.forEach((area) => {
      area.classList.remove(this.ACTIVE_CLASS);
    });
  }

  getUsers(): User[] {
    if (this.kloppenService.getPlayers().length > 0) {
      return this.kloppenService.getPlayers();
    }
    return JSON.parse(localStorage.getItem('players')) as User[];
  }

  getDate(): string {
    if (this.kloppenService.getDate()) {
      return this.kloppenService.getDate();
    }
    return JSON.parse(localStorage.getItem('date')) as string;
  }

  getType(): string {
    if (this.kloppenService.getType()) {
      return this.kloppenService.getType();
    }
    return JSON.parse(localStorage.getItem('type')) as string;
  }

  isLegacy(): boolean {
    if (this.kloppenService.isLegacy() !== undefined) {
      return this.kloppenService.isLegacy();
    }
    return JSON.parse(localStorage.getItem('legacy')) as boolean;
  }

  getEvent(): string {
    if (this.kloppenService.getEvent()) {
      return this.kloppenService.getEvent();
    }
    return JSON.parse(localStorage.getItem('event')) as string;
  }

  getPokalInfos(): object {
    if (this.kloppenService.getPokalInfos()) {
      return this.kloppenService.getPokalInfos();
    }
    return JSON.parse(localStorage.getItem('pokalInfos')) as object;
  }

  saveDataForPageReload(): void {
    if (this.users.length > 0) {
      localStorage.setItem('players', JSON.stringify(this.users));
    }
    localStorage.setItem('date', JSON.stringify(this.game.date));
    localStorage.setItem('type', JSON.stringify(this.game.type));
    localStorage.setItem('event', JSON.stringify(this.game.event));
    localStorage.setItem('legacy', JSON.stringify(this.game.legacy));
    if (this.kloppenService.getPokalInfos() !== undefined) {
      localStorage.setItem('pokalInfos', JSON.stringify(this.kloppenService.getPokalInfos()));
    }
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  @HostListener('window:keyup', ['$event'])
  enterThrowNyKeyboardInput(event: KeyboardEvent) {
    const eventCode = event.code;
    console.log(event.key, eventCode);

    if ((KEY_CODE.NUMPAD_0 === eventCode
          || KEY_CODE.NUMPAD_1 === eventCode
          || KEY_CODE.NUMPAD_2 === eventCode
          || KEY_CODE.NUMPAD_3 === eventCode)
        && this.activeRound) {
      this.enterThrow(null, parseInt(event.key, 10));
    }
  }
}
