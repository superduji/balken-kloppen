import {Injectable} from '@angular/core';
import {Game} from '../game';
import {LigaService} from '../../create-liga/liga.service';
import {Liga} from '../../create-liga/liga';
import {Player} from '../../models/boundary/player.model';
import {LigaTablePlayer} from '../../create-liga/ligaTablePlayer';
import {LigaGameTableGame} from '../../create-liga/ligaGameTableGame';

@Injectable({
  providedIn: 'root'
})
export class CreateGameUpdateLigaService {


  constructor(private ligaService: LigaService) {
  }


  calculateLigaTableAndSetGameKeyToLigaGameTableGame(game: Game, ligaKey: string): void {

    const playerDataFromGameA = game.players[0] as Player;
    const playerDataFromGameB = game.players[1] as Player;

    this.ligaService.getLigaSnapshotByKeyAsync(ligaKey)
      .then(ligaSnapshot => ligaSnapshot.data() as Liga)
      .then(liga => {

        let ligaTablePlayerA: LigaTablePlayer;
        let ligaTablePlayerB: LigaTablePlayer;
        let ligaGameTableGames: LigaGameTableGame[];
        liga.ligaPlayers.forEach(ligaPlayer => {
          if (ligaPlayer.name === playerDataFromGameA.name) {
            ligaTablePlayerA = ligaPlayer.ligaTablePlayer;
            ligaGameTableGames = ligaPlayer.ligaGameTableRow.ligaGameTableGames;
          } else if (ligaPlayer.name === playerDataFromGameB.name) {
            ligaTablePlayerB = ligaPlayer.ligaTablePlayer;
          }
        });

        if (ligaTablePlayerA && ligaTablePlayerB && ligaGameTableGames) {
          ligaGameTableGames.forEach(ligaGameTableGame => {
            if (ligaGameTableGame.columnPlayerName === playerDataFromGameB.name) {
              ligaGameTableGame.gameRef = game.key;

              this.calculatePlayersData(ligaTablePlayerA,
                ligaTablePlayerB,
                playerDataFromGameA,
                playerDataFromGameB,
                game,
                ligaGameTableGame.round);

              this.updateLigaTablePositions(liga, ligaGameTableGame.round);
              this.ligaService.updateLiga(liga);
            }
          });
        }
      });
  }

  private calculatePlayersData(ligaTablePlayerA: LigaTablePlayer,
                               ligaTablePlayerB: LigaTablePlayer,
                               playerDataFromGameA: Player,
                               playerDataFromGameB: Player,
                               game: Game,
                               round: string): void {

    this.calculatePlayerData(ligaTablePlayerA, playerDataFromGameA, game, round);
    this.calculatePlayerData(ligaTablePlayerB, playerDataFromGameB, game, round);
  }

  private calculatePlayerData(ligaTablePlayer: LigaTablePlayer, playerDataFromGame: Player, game: Game, round: string): void {

    if (round && round === 'first') {
      ligaTablePlayer.playedGamesFirstRound++;
      ligaTablePlayer.numberOfThrowsFirstRound += playerDataFromGame.throws.length;

      if (playerDataFromGame.name === game.winner) {
        ligaTablePlayer.winsFirstRound++;
        ligaTablePlayer.pointsFirstRound += 2;
      } else if (game.draw) {
        ligaTablePlayer.pointsFirstRound += 1;
        ligaTablePlayer.drawsFirstRound++;
      } else {
        ligaTablePlayer.defeatsFirstRound++;
      }

    } else if (round && round === 'second') {
      ligaTablePlayer.playedGamesSecondRound++;
      ligaTablePlayer.numberOfThrowsSecondRound += playerDataFromGame.throws.length;

      if (playerDataFromGame.name === game.winner) {
        ligaTablePlayer.winsSecondRound++;
        ligaTablePlayer.pointsSecondRound += 2;
      } else if (game.draw) {
        ligaTablePlayer.pointsSecondRound += 1;
        ligaTablePlayer.drawsSecondRound++;
      } else {
        ligaTablePlayer.defeatsSecondRound++;
      }
    }

    ligaTablePlayer.playedGames++;
    ligaTablePlayer.numberOfThrows += playerDataFromGame.throws.length;

    if (playerDataFromGame.name === game.winner) {
      ligaTablePlayer.wins++;
      ligaTablePlayer.points += 2;
    } else if (game.draw) {
      ligaTablePlayer.points += 1;
      ligaTablePlayer.draws++;
    } else {
      ligaTablePlayer.defeats++;
    }

    this.calculateAndSetQuote(ligaTablePlayer, playerDataFromGame, round);

    ligaTablePlayer.gameRefs.push(game.key);
  }

  private calculateAndSetQuote(ligaTablePlayer: LigaTablePlayer, playerDataForGame: Player, round: string): void {
    let sumA = 0;

    const newThrowsArray = this.replaceUeberwuerfeByZeros(playerDataForGame.throws.map(String));
    console.log('newThrows: ', newThrowsArray);

    newThrowsArray.forEach(t => {
      sumA += +t;
    });

    if (round && round === 'first') {
      ligaTablePlayer.throwsSumFirstRound += sumA;
      ligaTablePlayer.quoteFirstRound = parseFloat((ligaTablePlayer.throwsSumFirstRound / ligaTablePlayer.numberOfThrowsFirstRound)
        .toFixed(3));
    } else if (round && round === 'second') {
      ligaTablePlayer.throwsSumSecondRound += sumA;
      ligaTablePlayer.quoteSecondRound = parseFloat((ligaTablePlayer.throwsSumSecondRound / ligaTablePlayer.numberOfThrowsSecondRound)
        .toFixed(3));
    }

    ligaTablePlayer.throwsSum += sumA;
    ligaTablePlayer.quote = parseFloat((ligaTablePlayer.throwsSum / ligaTablePlayer.numberOfThrows).toFixed(3));
  }

  private replaceUeberwuerfeByZeros(originalThrowsArray: Array<string>): Array<string> {
    const newThrowsArray = originalThrowsArray;

    for (let i = 0; i < Math.ceil(originalThrowsArray.length / 5); i++) {
      for (let j = 0; j < 5; j++) {
        if (originalThrowsArray[i * 5 + j]) {
          if (originalThrowsArray[i * 5 + j].indexOf('Ü') > -1) {
            for (let k = 0; k < 5; k++) {
              newThrowsArray[i * 5 + k] = '0';
            }
            j = 4;
          }
        }
      }
    }

    return newThrowsArray;
  }

  private updateLigaTablePositions(liga: Liga, round: string): void {

    if (round && round === 'first') {
      this.sortLigaTablePlayerPositionsFirstRound(liga);
    } else if (round && round === 'second') {
      this.sortLigaTablePlayerPositionsSecondRound(liga);
    }
    this.sortLigaTablePlayerPositions(liga);

  }

  private sortLigaTablePlayerPositionsFirstRound(liga: Liga): void {

    const ligaTablePlayers: LigaTablePlayer[] = new Array<LigaTablePlayer>();

    liga.ligaPlayers.forEach(ligaPlayer => {
      ligaTablePlayers.push(ligaPlayer.ligaTablePlayer);
    });

    ligaTablePlayers.sort((a, b) => {
      if (a.pointsFirstRound > b.pointsFirstRound) {
        return -1;
      }
      if (a.pointsFirstRound < b.pointsFirstRound) {
        return 1;
      }

      if (a.quoteFirstRound > b.quoteFirstRound) {
        return -1;
      }
      if (a.quoteFirstRound < b.quoteFirstRound) {
        return 1;
      }

      if (a.playedGamesFirstRound < b.playedGamesFirstRound) {
        return -1;
      }
      if (a.playedGamesFirstRound > b.playedGamesFirstRound) {
        return 1;
      }

      return 0;
    });

    ligaTablePlayers.forEach((ligaTablePlayer, index) => {
      ligaTablePlayer.positionFirstRound = index + 1;
    });
  }

  private sortLigaTablePlayerPositionsSecondRound(liga: Liga): void {

    const ligaTablePlayers: LigaTablePlayer[] = new Array<LigaTablePlayer>();

    liga.ligaPlayers.forEach(ligaPlayer => {
      ligaTablePlayers.push(ligaPlayer.ligaTablePlayer);
    });

    ligaTablePlayers.sort((a, b) => {
      if (a.pointsSecondRound > b.pointsSecondRound) {
        return -1;
      }
      if (a.pointsSecondRound < b.pointsSecondRound) {
        return 1;
      }

      if (a.quoteSecondRound > b.quoteSecondRound) {
        return -1;
      }
      if (a.quoteSecondRound < b.quoteSecondRound) {
        return 1;
      }

      if (a.playedGamesSecondRound < b.playedGamesSecondRound) {
        return -1;
      }
      if (a.playedGamesSecondRound > b.playedGamesSecondRound) {
        return 1;
      }

      return 0;
    });

    ligaTablePlayers.forEach((ligaTablePlayer, index) => {
      ligaTablePlayer.positionSecondRound = index + 1;
    });
  }

  private sortLigaTablePlayerPositions(liga: Liga): void {

    let ligaTablePlayers: LigaTablePlayer[] = new Array<LigaTablePlayer>();
    const unsortedLigaTablePlayersArray = ligaTablePlayers;

    liga.ligaPlayers.forEach(ligaPlayer => {
      ligaTablePlayers.push(ligaPlayer.ligaTablePlayer);
    });

    ligaTablePlayers.sort((a, b) => {
      if (a.points > b.points) {
        return -1;
      }
      if (a.points < b.points) {
        return 1;
      }

      if (a.quote > b.quote) {
        return -1;
      }
      if (a.quote < b.quote) {
        return 1;
      }

      if (a.playedGames < b.playedGames) {
        return -1;
      }
      if (a.playedGames > b.playedGames) {
        return 1;
      }

      return 0;
    });

    ligaTablePlayers.forEach((ligaTablePlayer, index) => {
      ligaTablePlayer.position = index + 1;
    });

    ligaTablePlayers = unsortedLigaTablePlayersArray;
  }
}
