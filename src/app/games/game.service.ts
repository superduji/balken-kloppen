import {Injectable} from '@angular/core';
import {Action, AngularFirestore, DocumentChangeAction, DocumentSnapshot} from '@angular/fire/firestore';
import {Game} from './game';
import {Game as GameModel} from '../models/boundary/game.model';
import {Player} from '../models/boundary/player.model';
import {GameFilteringService} from './game-filtering.service';
import {Observable} from 'rxjs';
import {DbService} from '../utils/db.service';
import {User} from '../user/user';

@Injectable({
  providedIn: 'root'
})

export class GameService {

  private db: AngularFirestore;
  private dbPath = '/game';

  filterKey: string;
  filterValue: string;

  constructor(private angularFirestore: AngularFirestore,
              private gameFilteringService: GameFilteringService,
              private dbService: DbService) {
    this.db = angularFirestore;
  }

  // ######################### SET ##################################

  createGame(game: Game, cb): string {
    console.log('create game: ', game);
    const documentKey = this.dbService.setDocumentAndReturnCustomizedKey(this.db, this.dbPath, game.id.toString(), game);
    console.log('Document written with ID: ', documentKey);
    cb(documentKey);
    game.key = documentKey;
    return documentKey;
  }
  // START: Bitte drin lassen: neue Funktionen
  setFilterKey(filterKey): void {
    this.filterKey = filterKey;
  }

  setFilterValue(filterValue): void {
    this.filterValue = filterValue;
  }

  // ######################### GET ##################################

  getAllDescendingById(): Observable<DocumentChangeAction<GameModel>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('id', 'desc'))
      .snapshotChanges() as Observable<DocumentChangeAction<GameModel>[]>;
  }

  getGamesFromActions(actions: DocumentChangeAction<GameModel>[]): GameModel[] {
    return actions
      .filter(
        action => !this.attributesAreReadyForFiltering()
          || this.gameFilteringService.isAHitOfFiltering(action, this.filterKey, this.filterValue)
      ).map(action => {
        return this.getGameFromAction(action);
      });
  }

  private getGameFromAction(action: DocumentChangeAction<GameModel>): GameModel {
    const game = action.payload.doc.data() as GameModel;
    game.key = action.payload.doc.id;
    game.players.forEach(player => {
      this.calculateFurtherPlayerData(player, game.winner);
    });
    return game;
  }

  calculateFurtherPlayerData(player: Player, winner: string): void {
    player.isWinner = winner === player.name;
    player.numberOfThrows = player.throws ? player.throws.length : 0;
    player.average = parseFloat(((15 - player.rest) / player.numberOfThrows).toFixed(3));
  }

  private attributesAreReadyForFiltering() {
    return this.filterKey !== undefined && this.filterKey.trim() !== ''
      && this.filterValue !== undefined && this.filterValue.trim() !== '';
  }

  getGameObservableByKey(key): Observable<Action<DocumentSnapshot<GameModel>>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .snapshotChanges() as Observable<Action<DocumentSnapshot<GameModel>>>;
  }

  async getGameByKeyAsync(key): Promise<DocumentSnapshot<GameModel>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .ref.get() as Promise<DocumentSnapshot<GameModel>>;
  }

  getGameModelFromGameObservable(observable): GameModel {
    const gameModel = observable.payload.data() as GameModel;
    gameModel.players.forEach(player => {
      this.calculateFurtherPlayerData(player, gameModel.winner);
    });
    return gameModel;
  }

  // ENDE: Bitte drin lassen: neue Funktionen

}
