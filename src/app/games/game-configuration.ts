export class GameConfiguration {
  playerCount: number;
  throwsPerRound: number;
  maxRounds: number;

  constructor() {}
}
