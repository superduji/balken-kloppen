import {StatisticsTable} from './statisticsTable';

export class StatisticsResult {

  resultArray: Array<string>;
  percent: number;
  extremValue: number
  resultTable: StatisticsTable;

  valuePxWidthForHtml = 120;

  constructor(resultArray: Array<string>, percent: number, extremValue: number, resultTable: StatisticsTable, valuePxWidthForHtml: number) {
    if (resultArray) {
      this.resultArray = resultArray;
    }

    if (percent) {
      this.percent = percent;
    }

    if (extremValue) {
      this.extremValue = extremValue;
    }

    if (resultTable) {
      this.resultTable = resultTable;
    }

    if (valuePxWidthForHtml) {
      this.valuePxWidthForHtml = valuePxWidthForHtml;
    }
  }
}
