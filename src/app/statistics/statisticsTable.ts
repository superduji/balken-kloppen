import {StatisticsTableRow} from './statisticsTableIRow';

export class StatisticsTable {

  tableHead: StatisticsTableRow;
  tableData: Array<StatisticsTableRow>;

  constructor(tableHead: StatisticsTableRow, tableData: Array<StatisticsTableRow>) {
    this.tableHead = tableHead;
    this.tableData = tableData;
  }
}
