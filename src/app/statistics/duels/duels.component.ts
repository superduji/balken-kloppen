import { Component } from '@angular/core';
import {GameService} from '../../games/game.service';
import {Game} from '../../models/boundary/game.model';
import {UserService} from '../../user/user.service';
import {User} from '../../user/user';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-duels',
  templateUrl: './duels.component.html',
  styleUrls: ['./duels.component.scss']
})
export class DuelsStatisticsComponent {

  games: Game[];
  users: User[];
  comparables = new Array(2);
  types = ['All', 'Test', 'Liga', 'Pokal'];
  players = [];
  logos = [];
  type;
  duelGames: Game[] = [];
  wins = [0, 0];
  defeats = [0, 0];
  draws = 0;
  points = [0, 0];
  throws = [0, 0];
  showKloppers = {
    0: false,
    1: false
  };
  urlParamNames = [];
  duelUsers = [];

  constructor(private gameService: GameService,
              private userService: UserService,
              private route: ActivatedRoute) {
    this.getGameList();
    this.getUsersList();
  }

  kloppOutDuels(type: string) {
    this.type = type;
    console.log(this.players, this.type);
    this.resetValues();
    this.duelGames = this.games.filter(
      game => {
        if (this.type && this.type !== 'All') {
          return this.players.includes(game.players[0].name) && this.players.includes(game.players[1].name) && this.type === game.type;
        }
        return this.players.includes(game.players[0].name) && this.players.includes(game.players[1].name);
      }
    );
    console.log(this.duelGames);
    this.evaluateGames();
  }

  chooseKlopper(i: number, user: User) {
    this.players[i] = user.name;
    this.logos[i] = this.getUserTeamLogo(user, 'm');
    console.log(this.players, name);
    if (this.players[0] && this.players[1]) {
      this.kloppOutDuels(this.type);
    }
  }

  toggleOptions(i) {
    this.showKloppers[i] = !this.showKloppers[i];
  }

  showOptions(i) {
    this.showKloppers[i] = true;
  }

  hideOptions(i) {
    setTimeout(() => {
      this.showKloppers[i] = false;
    }, 150);
  }

  getUserTeamLogo(user: User, size: string) {
    return `assets/images/user/verein/logo_${user.team.toLowerCase().replace(/ /g, '_')}_${size}.png`;
  }

  getWurfquote(points, throws) {
    if (throws !== 0) {
      return (points / throws).toFixed(3);
    }
    return 0;
  }

  getHighlighting(index: number, data) {
    if (index === 0) {
      return data[index] > data[index + 1];
    }
    return data[index] > data[index - 1];
  }

  private resetValues() {
    this.wins = [0, 0];
    this.defeats = [0, 0];
    this.draws = 0;
    this.points = [0, 0];
    this.throws = [0, 0];
  }

  private evaluateGames(): void {
    this.duelGames.forEach((game, index) => {
      console.log(game);

      // evaluiere Siege, Niederlagen, Unentschieden
      if (game.winner === this.players[0]) {
        this.wins[0]++;
        this.defeats[1]++;
      } else if (game.winner === this.players[1]) {
        this.wins[1]++;
        this.defeats[0]++;
      } else if (game.draw) {
        this.draws++;
      }

      // evaluiere Punkte und Würfe
      if (game.players[0].name === this.players[0]) {
        this.points[0] += 15 - game.players[0].rest;
        this.throws[0] += game.players[0].numberOfThrows;
        this.points[1] += 15 - game.players[1].rest;
        this.throws[1] += game.players[1].numberOfThrows;
      } else if (game.players[1].name === this.players[0]) {
        this.points[0] += 15 - game.players[1].rest;
        this.throws[0] += game.players[1].numberOfThrows;
        this.points[1] += 15 - game.players[0].rest;
        this.throws[1] += game.players[0].numberOfThrows;
      }
    });
  }

  private getGameList() {
    // für lokale Entwicklung
    // const gamesLocal = JSON.parse(localStorage.getItem('games'));
    // if (gamesLocal) {
    //   this.games = gamesLocal;
    // } else {
      this.gameService.getAllDescendingById()
        .subscribe(actions => {
          this.games = this.gameService.getGamesFromActions(actions);
          // return localStorage.setItem('games', JSON.stringify(this.games));
        });
    // }
  }

  private getUsersList() {
    // für lokale Entwicklung
    // const usersLocal = JSON.parse(localStorage.getItem('users'));
    // if (usersLocal) {
    //   this.users = usersLocal;
    // } else {
      this.userService.getAllAscendingByName()
        .subscribe(actions => {
          this.users = this.userService.getUsersFromActions(actions);

          this.route.queryParams.subscribe((params: Params) => {
            this.urlParamNames = params.users.split(',');
            // const validUsersFromUrlParam = this.duelUsers.length === 2;
            if (this.urlParamNames.length === 2) {
              this.urlParamNames.forEach((name, i) => {
                const player = this.users.find(user => name === user.name);
                if (player) {
                  this.duelUsers.push(this.users.find(user => name === user.name));
                }
              });
              if (this.duelUsers.length === 2) {
                this.duelUsers.forEach((player, i) => this.chooseKlopper(i, player));
              }
            }
          });
          // return localStorage.setItem('users', JSON.stringify(this.users));
        });
    // }
  }
}
