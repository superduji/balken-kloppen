import {StatisticsStyling} from './statisticsStyling';

export class StatisticsTableRow {

  name: string;
  rowValues: Array<number>;
  additionalData: Map<string, string[]>;
  styling: StatisticsStyling;

  constructor(name: string, rowValues: Array<number>, additionalData: Map<string, string[]>) {
    this.name = name;
    this.rowValues = rowValues;
    this.additionalData = additionalData;
  }
}
