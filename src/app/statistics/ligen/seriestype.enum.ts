const enum SeriesType {
  EMPTY = 'empty',
  QUOTE = 'quote',
  POINTS = 'points',
  POSITIONS = 'position',
  WINS_DRAWS_DEFEATS = 'winsDrawsDefeats'
}
