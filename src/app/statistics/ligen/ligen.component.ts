import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighchartsMore from 'highcharts/highcharts-more';
import * as HighchartsExporting from 'highcharts/modules/exporting';
import * as HighchartsExportData from 'highcharts/modules/export-data';
import {LigaService} from '../../create-liga/liga.service';
import {LigaTablePlayer} from '../../create-liga/ligaTablePlayer';
import {OptionsHelper} from './optionsHelper';
import {UserService} from '../../user/user.service';
import {ColorHelper} from '../../utils/colorHelper';
import {Liga} from '../../create-liga/liga';
import {StatisticsData} from '../statisticsData';
import {StatisticsResult} from '../statisticsResult';
import {StatisticsTable} from '../statisticsTable';
import {StatisticsTableRow} from '../statisticsTableIRow';
import {StatisticsStyling} from '../statisticsStyling';

HighchartsMore(Highcharts);
HighchartsExporting(Highcharts);
HighchartsExportData(Highcharts);

@Component({
  selector: 'app-ligen-statistics',
  templateUrl: './ligen.component.html',
  styleUrls: ['./ligen.component.css']
})
export class LigenStatisticsComponent implements OnInit {

  // data for chart
  highcharts = Highcharts;
  updateFlag = false;
  options = Highcharts.options;

  seriesMapHalf = new Map<string, Array<object>>();
  seriesMapFull = new Map<string, Array<object>>();
  xAxisHalf = [];
  xAxisFull = [];
  colorsMap = new Map<string, string>();
  logosMap = new Map<string, string>();

  seriesType = '';
  optionsType = 'full';

  // Hilfsmaps
  mappHalf = new Map<string, Map<string, object>>();
  mappFull = new Map<string, Map<string, object>>();
  teamMap = new Map<string, string>();

  // Hilfsbooleans
  officialLigenFinished = false;
  seriesDataCalculated = false;
  seriesTypeFilterWasChosen = false;

  // Statistiken
  officialLigen = new Array<Liga>();
  statisticsData = new Array<StatisticsData>();

  OFFICIAL_LEAGUES_FOR_STATISTICS = 'official-leagues-for-statistics';
  OFFICIAL_LEAGUES_FOR_STATISTICS_CREATION_DATE = 'official-leagues-for-statistics-creation-date';

  constructor(private ligaService: LigaService,
              private optionsHelper: OptionsHelper,
              private userService: UserService,
              private colorHelper: ColorHelper) {

    const officialLeaguesFromLocalStorage = localStorage.getItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS);
    const officialLeaguesCreationDateFromLocalStorage = localStorage.getItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS_CREATION_DATE);

    if (!officialLeaguesFromLocalStorage
      || (officialLeaguesCreationDateFromLocalStorage && this.dataAreTooOld(officialLeaguesCreationDateFromLocalStorage))) {

      this.ligaService.getAllAscendingById().subscribe(
        ligenActions => {
          this.ligaService.getLigenFromActions(ligenActions).forEach((liga, ligaIndex) => {
            console.log(ligaIndex, ligenActions.length - 1);
            if (liga.official) {
              this.officialLigen.push(liga);
            }
            if (ligaIndex === ligenActions.length - 1) {
              localStorage.removeItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS);
              localStorage.setItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS, JSON.stringify(this.officialLigen));
              localStorage.removeItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS_CREATION_DATE);
              localStorage.setItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS_CREATION_DATE, Date.now().toString(10));
              this.evaluateStatistics();
              this.officialLigenFinished = true;
            }
          });
        }
      );
    } else {
      this.officialLigen = JSON.parse(localStorage.getItem(this.OFFICIAL_LEAGUES_FOR_STATISTICS)) as Array<Liga>;
      this.evaluateStatistics();
      this.officialLigenFinished = true;
    }
  }

  ngOnInit(): void {
    this.setDefaultChartOptions();
  }

  dataAreTooOld(oldDate: string): boolean {
    if (Date.now() - (JSON.parse(oldDate) as number) > 86400000) {
      return true;
    }
    return false;
  }

  calculateSeriesData(): boolean {
    if (!this.seriesDataCalculated) {
      this.officialLigen.forEach((liga, ligaIndex) => {
        this.xAxisHalf.push(liga.name + ' (H)');
        this.xAxisHalf.push(liga.name + ' (R)');
        this.xAxisFull.push(liga.name);

        liga.ligaPlayers.forEach((ligaPlayer, ligaPlayerIndex) => {
          const ligaTablePlayer = ligaPlayer.ligaTablePlayer;
          const name = ligaPlayer.name;
          this.teamMap.set(name, ligaPlayer.team);

          this.collectSeriesData(SeriesType.QUOTE, name, ligaIndex, ligaTablePlayer);
          this.collectSeriesData(SeriesType.POINTS, name, ligaIndex, ligaTablePlayer);
          this.collectSeriesData(SeriesType.POSITIONS, name, ligaIndex, ligaTablePlayer);

          if (ligaIndex === this.officialLigen.length - 1 && ligaPlayerIndex === liga.ligaPlayers.length - 1) {
            this.setCollectedDataToSeriesMap(SeriesType.QUOTE);
            this.setCollectedDataToSeriesMap(SeriesType.POINTS);
            this.setCollectedDataToSeriesMap(SeriesType.POSITIONS);

            this.setDefaultChartOptions();

            this.updateFlag = true;
            this.seriesDataCalculated = true;
          }
        });
      });
    }
    return true;
  }

  private collectSeriesData(seriesTypeEnum: SeriesType,
                            name: string,
                            ligaIndex: number,
                            ligaTablePlayer: LigaTablePlayer) {

    const seriesType = seriesTypeEnum.valueOf();
    this.collectNormalSeriesData(seriesType, name, ligaIndex, ligaTablePlayer);
  }


  private collectNormalSeriesData(seriesType: string,
                                  name: string,
                                  ligaIndex: number,
                                  ligaTablePlayer: LigaTablePlayer) {
    // get Map for current seriesType
    let mepHalf;
    if (!this.mappHalf.has(seriesType)) {
      mepHalf = new Map<string, number[]>();
    } else {
      mepHalf = this.mappHalf.get(seriesType);
    }

    // map must have an entry for this player
    if (!mepHalf.has(name)) {
      mepHalf.set(name, new Array<number>());
    }

    // set new values-Array to map
    const valuesArrayHalf = mepHalf.get(name);
    if (ligaTablePlayer.playedGamesFirstRound > 0) {
      valuesArrayHalf[ligaIndex * 2] = ligaTablePlayer[seriesType + 'FirstRound'];
    }
    if (ligaTablePlayer.playedGamesSecondRound > 0) {
      valuesArrayHalf[(ligaIndex * 2) + 1] = ligaTablePlayer[seriesType + 'SecondRound'];
    }
    mepHalf.set(name, valuesArrayHalf);     // overwrite players-map
    this.mappHalf.set(seriesType, mepHalf); // overwrite seriesTypes-map

    // ########################

    // get Map for current seriesType
    let mepFull;
    if (!this.mappFull.has(seriesType)) {
      mepFull = new Map<string, number[]>();
    } else {
      mepFull = this.mappFull.get(seriesType);
    }

    // map must have an entry for this player
    if (!mepFull.has(name)) {
      mepFull.set(name, new Array<number>());
    }

    // set new values-Array to map
    const valuesArrayFull = mepFull.get(name);
    if (ligaTablePlayer.playedGamesFirstRound > 0) {
      valuesArrayFull[ligaIndex] = ligaTablePlayer[seriesType];
    }
    mepFull.set(name, valuesArrayFull);     // overwrite players-map
    this.mappFull.set(seriesType, mepFull); // overwrite seriesTypes-map
  }

  private setCollectedDataToSeriesMap(seriesTypeEnum: SeriesType) {
    const seriesType = seriesTypeEnum.valueOf();

    // push every user-data to options.series
    const seriesHalf = [];
    this.mappHalf.get(seriesType).forEach((value, key) => {
      const options = this.optionsHelper.getOptionsMap().get(seriesType);
      this.colorHelper.getDominantColorFromTeamLogo(key, this.userService, this.colorsMap, this.logosMap)
        .then(() => {
          options['color'] = this.colorsMap.get(key);
          options['marker'] = {symbol: 'url(' + this.logosMap.get(key) + ')'};
          options['name'] = key;
          options['data'] = this.fillEmptyArrayItems(value as Array<number>, null);
          if (seriesType === SeriesType.POSITIONS.valueOf()) {
            options['step'] = 'center';
          }

          seriesHalf.push(options);
        });
      this.seriesMapHalf.set(seriesType, seriesHalf);
    });

    const seriesFull = [];
    this.mappFull.get(seriesType).forEach((value, key) => {
      const options = this.optionsHelper.getOptionsMap().get(seriesType);
      this.colorHelper.getDominantColorFromTeamLogo(key, this.userService, this.colorsMap, this.logosMap)
        .then(() => {
          options['color'] = this.colorsMap.get(key);
          options['marker'] = {symbol: 'url(' + this.logosMap.get(key) + ')'};
          options['name'] = key;
          options['data'] = this.fillEmptyArrayItems(value as Array<number>, null);
          if (seriesType === SeriesType.POSITIONS.valueOf()) {
            options['step'] = 'center';
          }

          seriesFull.push(options);
        });
      this.seriesMapFull.set(seriesType, seriesFull);
    });
  }


  private fillEmptyArrayItems(values: number[], fillValue: number): Array<number> {
    // fill unset array-items with 'null'
    for (let i = 0; i < values.length; i++) {
      if (!values[i] && values[i] !== 0) {
        values[i] = fillValue;
      }
    }
    return values;
  }

  seriesTypeFilterChanged(seriesType: string) {
    if (seriesType) {
      this.seriesType = seriesType;
      this.setChartOptions(seriesType);
      this.seriesTypeFilterWasChosen = true;
    }
  }

  private setDefaultChartOptions() {
    const seriesType = SeriesType.EMPTY.valueOf();
    this.options = this.optionsHelper.getOptions(seriesType, {}, {}, this.getExporting(), 'Saisonhälften');
  }

  private setChartOptions(seriesType: string) {

    if (this.optionsType === 'half') {
      this.options = this.optionsHelper.getOptions(seriesType,
        this.xAxisHalf,
        this.seriesMapHalf.get(seriesType),
        this.getExporting(),
        'Saisonhälften'
      );
    } else if (this.optionsType === 'full') {
      this.options = this.optionsHelper.getOptions(seriesType,
        this.xAxisFull,
        this.seriesMapFull.get(seriesType),
        this.getExporting(),
        'Saisons'
      );
    }
    this.setEventsForSeriesses();
  }

  switchHighCharts() {
    // store all activated seriesses
    let activeSeriesses = [];
    if (this.options.series && this.options.series.length > 0) {
      activeSeriesses = this.options.series.filter(series => {
        return series.visible;
      });
    }

    // show series for season-halfs:
    if (this.optionsType === 'full') {
      this.options = this.optionsHelper.getOptions(
        this.seriesType,
        this.xAxisHalf,
        this.seriesMapHalf.get(this.seriesType),
        this.getExporting(),
        'Saisonhälften'
      );
      this.optionsType = 'half';

      // show series for full seasons:
    } else if (this.optionsType === 'half') {
      this.options = this.optionsHelper.getOptions(this.seriesType,
        this.xAxisFull,
        this.seriesMapFull.get(this.seriesType),
        this.getExporting(),
        'Saisons'
      );
      this.optionsType = 'full';
    }
    this.setEventsForSeriesses();

    // activate former active seriesses again
    this.options.series.forEach(series => {
      series.visible = false;
    });
    activeSeriesses.forEach(formerActiveSeries => {
      this.options.series.forEach(series => {
        if (series.name === formerActiveSeries.name) {
          series.visible = true;
        }
      });
    });
  }

  setEventsForSeriesses() {
    this.options.series.forEach(series => {
      series.events = {
        show: () => {
          series.visible = true;
          console.log(series);
        },
        hide: () => {
          // this.activeSeriessesNames = this.activeSeriessesNames.filter((item) => {
          //   return item === series.name;
          // });
          series.visible = false;
          console.log(series);
        }
      };
    });
  }

  getExporting() {
    return {
      menuItemDefinitions: {

        showHideAll: {
          onclick: () => {
            if (this.options.series) {

              // analyze current visibility-quote
              let numberOfVisible = 0;
              let numberOfInvisible = 0;
              this.options.series.forEach(series => {
                if (series.visible) {
                  numberOfVisible++;
                  // series.visible = true;
                } else {
                  numberOfInvisible++;
                }
              });

              // define future visibility-value for all series
              let visibilityValueToSet = true;
              if (numberOfInvisible === 0) {
                visibilityValueToSet = false;
              }

              // set new visibilty-value to all series
              this.options.series.forEach(series => {
                series.visible = visibilityValueToSet;
              });

              this.updateFlag = true;
            }
          },
          text: 'show / hide all'
        }
      },
      buttons: {
        contextButton: {
          menuItems: [
            'showHideAll',
            'separator',
            'downloadPNG',
            'downloadSVG',
            'downloadJPEG',
            'downloadPDF',
            'printChart'
          ]
        }
      }
    };
  }


  // ########################################## Statistiken ###############################################
  evaluateStatistics() {
    this.statisticsData.push(new StatisticsData('Meistertitel', this.getPositions(1, 'league-trophy_xs.png')));
    this.statisticsData.push(new StatisticsData('Positionen', this.getPositions(null, null)));
    this.statisticsData.push(new StatisticsData('Meister, die Herbst- und Frühlingsmeister waren', this.getAllMeisterMeister()));
    this.statisticsData.push(new StatisticsData('Herbstmeister, die Meister geworden sind', this.getHerbstMeisterMeister()));
    this.statisticsData.push(new StatisticsData('Herbstmeister, die nicht Meister geworden sind', this.getHerbstMeisterNotMeister()));
    this.statisticsData.push(new StatisticsData('Frühlingsmeister, die Meister geworden sind', this.getFrühlingsMeisterMeister()));
    this.statisticsData.push(new StatisticsData('Frühlingsmeister, die nicht Meister geworden sind', this.getFrühlingsMeisterNotMeister()));
    this.statisticsData.push(new StatisticsData('Meister, die weder Herbst- noch Frühlingsmeister waren', this.getNoMeisterMeister()));
    this.statisticsData.push(new StatisticsData('die höchste (relative) Punktzahl zum Meistertitel', this.getHighestPointsForWinner()));
    this.statisticsData.push(new StatisticsData('die geringste (relative) Punktzahl zum Meistertitel', this.getLeastPointsForWinner()));
    this.statisticsData.push(new StatisticsData('die höchste (relative) Punktzahl des Letzten', this.getHighestPointsForLast()));
    this.statisticsData.push(new StatisticsData('die geringste (relative) Punktzahl des Letzten', this.getLeastPointsForLast()));
    this.statisticsData.push(new StatisticsData('die beste Meister-Wurfquote', this.getBestQuoteForWinner()));
    this.statisticsData.push(new StatisticsData('die schlechteste Meister-Wurfquote', this.getLeastQuoteForWinner()));
    this.statisticsData.push(new StatisticsData('die beste Nicht-Meister-Wurfquote', this.getBestQuoteForNonWinner()));
    this.statisticsData.push(new StatisticsData('die beste Wurfquote des Letzten', this.getBestQuoteForLast()));
    this.statisticsData.push(new StatisticsData('die schlechteste Wurfquote des Letzten', this.getLeastQuoteForLast()));
  }

  private getAllMeisterMeister(): StatisticsResult {
    const herbstMeisterMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionFirstRound === 1
            && player.ligaTablePlayer.positionSecondRound === 1
            && player.ligaTablePlayer.position === 1) {
            herbstMeisterMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((herbstMeisterMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(herbstMeisterMeister, percent, null, null, null);
  }

  private getHerbstMeisterMeister(): StatisticsResult {
    const herbstMeisterMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionFirstRound === 1
            && player.ligaTablePlayer.position === 1) {
            herbstMeisterMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((herbstMeisterMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(herbstMeisterMeister, percent, null, null, null);
  }

  private getHerbstMeisterNotMeister(): StatisticsResult {
    const herbstMeisterNotMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionFirstRound === 1
            && player.ligaTablePlayer.position !== 1) {
            herbstMeisterNotMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((herbstMeisterNotMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(herbstMeisterNotMeister, percent, null, null, null);
  }

  private getFrühlingsMeisterMeister(): StatisticsResult {
    const fruehlingsMeisterMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionSecondRound === 1
            && player.ligaTablePlayer.position === 1) {
            fruehlingsMeisterMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((fruehlingsMeisterMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(fruehlingsMeisterMeister, percent, null, null, null);
  }

  private getFrühlingsMeisterNotMeister(): StatisticsResult {
    const fruehlingsMeisterNotMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionSecondRound === 1
            && player.ligaTablePlayer.position !== 1) {
            fruehlingsMeisterNotMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((fruehlingsMeisterNotMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(fruehlingsMeisterNotMeister, percent, null, null, null);
  }

  private getNoMeisterMeister(): StatisticsResult {
    const herbstMeisterMeister = new Array<string>();
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          if (player.ligaTablePlayer.positionFirstRound !== 1
            && player.ligaTablePlayer.positionSecondRound !== 1
            && player.ligaTablePlayer.position === 1) {
            herbstMeisterMeister.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });
    let percent = null;
    if (this.officialLigen && this.officialLigen.length > 0) {
      percent = parseFloat((herbstMeisterMeister.length / this.officialLigen.length * 100).toFixed(2));
    }

    return new StatisticsResult(herbstMeisterMeister, percent, null, null, null);
  }

  private getHighestPointsForWinner(): StatisticsResult {
    const highestPointsWinners = new Array<string>();
    let maxValue = 0;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1) {
            maxValue = Math.max(maxValue, ligaTablePlayer.points / ligaTablePlayer.playedGames);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1
            && maxValue === ligaTablePlayer.points / ligaTablePlayer.playedGames) {
            highestPointsWinners.push(
              liga.name
              + '  (' + player.name + ' ' + ligaTablePlayer.points + 'Pkt./' + ligaTablePlayer.playedGames + 'Sp.)');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      maxValue = parseFloat(maxValue.toFixed(3));
    }

    return new StatisticsResult(highestPointsWinners, null, maxValue, null, 190);
  }

  private getLeastPointsForWinner(): StatisticsResult {
    const leastPointsWinners = new Array<string>();
    let minValue = 1000;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1) {
            minValue = Math.min(minValue, ligaTablePlayer.points / ligaTablePlayer.playedGames);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1
            && minValue === ligaTablePlayer.points / ligaTablePlayer.playedGames) {
            leastPointsWinners.push(
              liga.name
              + '  (' + player.name + ' ' + ligaTablePlayer.points + 'Pkt./' + ligaTablePlayer.playedGames + 'Sp.)');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      minValue = parseFloat(minValue.toFixed(3));
    }

    return new StatisticsResult(leastPointsWinners, null, minValue, null, 190);
  }

  private getHighestPointsForLast(): StatisticsResult {
    const highestPointsLasts = new Array<string>();
    let maxValue = 0;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length) {
            maxValue = Math.max(maxValue, ligaTablePlayer.points / ligaTablePlayer.playedGames);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length
            && maxValue === ligaTablePlayer.points / ligaTablePlayer.playedGames) {
            highestPointsLasts.push(
              liga.name
              + '  (' + player.name + ' ' + ligaTablePlayer.points + 'Pkt./' + ligaTablePlayer.playedGames + 'Sp.)');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      maxValue = parseFloat(maxValue.toFixed(3));
    }

    return new StatisticsResult(highestPointsLasts, null, maxValue, null, 190);
  }

  private getLeastPointsForLast(): StatisticsResult {
    const leastPointsLasts = new Array<string>();
    let minValue = 1000;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length) {
            minValue = Math.min(minValue, ligaTablePlayer.points / ligaTablePlayer.playedGames);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length
            && minValue === ligaTablePlayer.points / ligaTablePlayer.playedGames) {
            leastPointsLasts.push(
              liga.name
              + '  (' + player.name + ' ' + ligaTablePlayer.points + 'Pkt./' + ligaTablePlayer.playedGames + 'Sp.)');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      minValue = parseFloat(minValue.toFixed(3));
    }

    return new StatisticsResult(leastPointsLasts, null, minValue, null, 190);
  }

  private getBestQuoteForWinner(): StatisticsResult {
    const bestQuoteWinners = new Array<string>();
    let bestQuote = 0;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1) {
            bestQuote = Math.max(bestQuote, ligaTablePlayer.quote);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1 && bestQuote === ligaTablePlayer.quote) {
            bestQuoteWinners.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      bestQuote = parseFloat(bestQuote.toFixed(3));
    }

    return new StatisticsResult(bestQuoteWinners, null, bestQuote, null, null);
  }

  private getLeastQuoteForWinner(): StatisticsResult {
    const leastQuoteWinners = new Array<string>();
    let leastQuote = 4;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1) {
            leastQuote = Math.min(leastQuote, ligaTablePlayer.quote);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === 1 && leastQuote === ligaTablePlayer.quote) {
            leastQuoteWinners.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      leastQuote = parseFloat(leastQuote.toFixed(3));
    }

    return new StatisticsResult(leastQuoteWinners, null, leastQuote, null, null);
  }

  private getBestQuoteForNonWinner(): StatisticsResult {
    const bestQuoteNonWinners = new Array<string>();
    let bestQuote = 0;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position !== 1) {
            bestQuote = Math.max(bestQuote, ligaTablePlayer.quote);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position !== 1 && bestQuote === ligaTablePlayer.quote) {
            bestQuoteNonWinners.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      bestQuote = parseFloat(bestQuote.toFixed(3));
    }

    return new StatisticsResult(bestQuoteNonWinners, null, bestQuote, null, null);
  }

  private getBestQuoteForLast(): StatisticsResult {
    const bestQuoteLasts = new Array<string>();
    let bestQuote = 0;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length) {
            bestQuote = Math.max(bestQuote, ligaTablePlayer.quote);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length && bestQuote === ligaTablePlayer.quote) {
            bestQuoteLasts.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      bestQuote = parseFloat(bestQuote.toFixed(3));
    }

    return new StatisticsResult(bestQuoteLasts, null, bestQuote, null, null);
  }

  private getLeastQuoteForLast(): StatisticsResult {
    const leastQuoteLasts = new Array<string>();
    let leastQuote = 4;

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length) {
            leastQuote = Math.min(leastQuote, ligaTablePlayer.quote);
          }
        });
      }
    });

    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        liga.ligaPlayers.forEach(player => {
          const ligaTablePlayer = player.ligaTablePlayer;
          if (ligaTablePlayer.position === liga.ligaPlayers.length && leastQuote === ligaTablePlayer.quote) {
            leastQuoteLasts.push(liga.name + '  (' + player.name + ')');
          }
        });
      }
    });

    if (this.officialLigen && this.officialLigen.length > 0) {
      leastQuote = parseFloat(leastQuote.toFixed(3));
    }

    return new StatisticsResult(leastQuoteLasts, null, leastQuote, null, null);
  }

  private getPositions(position: number, imagePath: string): StatisticsResult {

    // prepare maps
    const positionsMap = new Map<string, Array<number>>();
    const ligenForPositionsMap = new Map<string, Map<string, Array<string>>>();

    // get maxNumberOfPlayers of a league
    let maxNumberOfPlayers = 0;
    this.officialLigen.forEach(liga => {
      if (liga.finished) {
        maxNumberOfPlayers = Math.max(maxNumberOfPlayers, liga.ligaPlayers.length);
      }
    });

    // collect data
    if ((position && position <= maxNumberOfPlayers) || position === null) {
      this.officialLigen.forEach(liga => {
        if (liga.finished) {
          liga.ligaPlayers.forEach(player => {

            const playerName = player.name;
            const playerPosition = player.ligaTablePlayer.position;

            let ligenMap = ligenForPositionsMap.get(playerName);
            let ligenArray;
            if (ligenMap) {
              ligenArray = ligenMap.get('' + (playerPosition - 1));
              if (!ligenArray) {
                ligenArray = new Array<number>();
              }
            } else {
              ligenMap = new Map<string, string[]>();
              ligenArray = new Array<number>();
            }

            let positionCounts;
            if (position) {
              if (playerPosition === position) {
                positionCounts = positionsMap.get(playerName) || new Array(1).fill(0);

                ligenArray.push(liga.name);
                ligenMap.set('' + (playerPosition - 1), ligenArray);
                ligenForPositionsMap.set(playerName, ligenMap);
              }
            } else {
              positionCounts = positionsMap.get(playerName) || new Array(maxNumberOfPlayers).fill(0);

              ligenArray.push(liga.name);
              ligenMap.set('' + (playerPosition - 1), ligenArray);
              ligenForPositionsMap.set(playerName, ligenMap);
            }
            if (positionCounts) {
              positionCounts[playerPosition - 1] = positionCounts[playerPosition - 1] + 1;
              positionsMap.set(playerName, positionCounts);
            }
          });
        }
      });

      // transform from map to array of statisticsTableRow because of sortability
      const playerTableData = new Array<StatisticsTableRow>();
      Array.from(positionsMap.keys()).forEach(nameKey => {

        const statisticsTableRow = new StatisticsTableRow(
          nameKey,
          positionsMap.get(nameKey) as Array<number>,
          ligenForPositionsMap.get(nameKey)
        );

        if (imagePath) {
          statisticsTableRow.styling = new StatisticsStyling();
          statisticsTableRow.styling.imagePath = imagePath;
        }

        playerTableData.push(statisticsTableRow);
      });

      // sort absteigend
      playerTableData.sort((a: StatisticsTableRow, b: StatisticsTableRow) => {
        return this.sort(a, b, 0);
      });

      const positionsTable = new StatisticsTable(null, playerTableData);
      return new StatisticsResult(null, null, null, positionsTable, null);
    } else {
      return null;
    }
  }

  sort(a: StatisticsTableRow, b: StatisticsTableRow, index): number {
    if (index < a.rowValues.length && index < b.rowValues.length) {
      if (a.rowValues[index] > b.rowValues[index]) {
        return -1;
      } else if (a.rowValues[index] === b.rowValues[index]) {
        return this.sort(a, b, index + 1);
      } else {
        return 1;
      }
    } else {
      return 0;
    }
  }

  getArrayOfLength(length: number) {
    if (length) {
      return new Array(length);
    } else {
      return [];
    }
  }
}
