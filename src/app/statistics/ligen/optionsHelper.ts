import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsHelper {

  constructor() {}

  getOptions(seriesType: string, xAxisData, seriesData, exporting, xTitleText) {

    let options;

    switch (seriesType) {
      case SeriesType.QUOTE.valueOf(): {
        options = this.getQuoteOptions(xAxisData, seriesData, xTitleText);
        break;
      }
      case SeriesType.POINTS.valueOf(): {
        options = this.getPointsOptions(xAxisData, seriesData, xTitleText);
        break;
      }
      case SeriesType.POSITIONS.valueOf(): {
        options = this.getPositionOptions(xAxisData, seriesData, xTitleText);
        break;
      }
      default: {
        options = {
          title: {
            text: 'Entscheide dich! Was für Daten willst du sehen?'
          }
        };
      }
    }
    options.exporting = exporting;

    return options;
  }

  private getQuoteOptions(xAxisData, seriesData, xTitleText) {
    return {
      chart: {
        type: this.getChartTypeMap().get(SeriesType.QUOTE.valueOf())
      },
      title: {
        text: this.getTitleMap().get(SeriesType.QUOTE.valueOf())
      },
      credits: {
        enabled: false
      },
      series: seriesData,
      xAxis: {
        categories: xAxisData,
        title: {
          text: xTitleText
        }
      },
      yAxis: {
        title: {
          text: this.getYAxisTitleMap().get(SeriesType.QUOTE.valueOf())
        },
        reversed: false,
        alternateGridColor: '#fcfcfc',
        tickInterval: undefined
      }
    };
  }

  private getPointsOptions(xAxisData, seriesData, xTitleText) {
    return {
      chart: {
        type: this.getChartTypeMap().get(SeriesType.POINTS.valueOf())
      },
      title: {
        text: this.getTitleMap().get(SeriesType.POINTS.valueOf())
      },
      credits: {
        enabled: false
      },
      series: seriesData,
      xAxis: {
        categories: xAxisData,
        title: {
          text: xTitleText
        }
      },
      yAxis: {
        title: {
          text: this.getYAxisTitleMap().get(SeriesType.POINTS.valueOf())
        },
        reversed: false,
        alternateGridColor: '#fcfcfc',
        tickInterval: 1
      }
    };
  }

  private getPositionOptions(xAxisData, seriesData, xTitleText) {
    return {
      chart: {
        type: this.getChartTypeMap().get(SeriesType.POSITIONS.valueOf())
      },
      title: {
        text: this.getTitleMap().get(SeriesType.POSITIONS.valueOf())
      },
      credits: {
        enabled: false
      },
      series: seriesData,
      xAxis: {
        categories: xAxisData,
        title: {
          text: xTitleText
        }
      },
      yAxis: {
        title: {
          text: this.getYAxisTitleMap().get(SeriesType.POSITIONS.valueOf())
        },
        reversed: true,
        alternateGridColor: '#fcfcfc',
        tickInterval: 1
      }
    };
  }

  private getChartTypeMap() {
    const mep = new Map<string, string>();
    mep.set(SeriesType.QUOTE.valueOf(), 'spline');
    mep.set(SeriesType.POINTS.valueOf(), 'column');
    mep.set(SeriesType.POSITIONS.valueOf(), 'line');
    return mep;
  }

  private getTitleMap() {
    const mep = new Map<string, string>();
    mep.set(SeriesType.QUOTE.valueOf(), 'Wurfquoten');
    mep.set(SeriesType.POINTS.valueOf(), 'Punkte');
    mep.set(SeriesType.POSITIONS.valueOf(), 'Positionen');
    return mep;
  }

  private getYAxisTitleMap() {
    const mep = new Map<string, string>();
    mep.set(SeriesType.QUOTE.valueOf(), 'Ø');
    mep.set(SeriesType.POINTS.valueOf(), 'Punkte');
    mep.set(SeriesType.POSITIONS.valueOf(), 'Positionen');
    return mep;
  }

  getOptionsMap() {
    const mep = new Map<string, object>();
    mep.set(SeriesType.QUOTE.valueOf(), {
      visible: false
    });

    mep.set(SeriesType.POINTS.valueOf(), {
      visible: false
    });

    mep.set(SeriesType.POSITIONS.valueOf(), {
      visible: false,
    });

    return mep;
  }
}
