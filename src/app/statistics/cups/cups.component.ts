import {Component} from '@angular/core';
import {Game} from '../../models/boundary/game.model';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';
import {GameService} from '../../games/game.service';

@Component({
  selector: 'app-cups',
  templateUrl: './cups.component.html',
  styleUrls: ['./cups.component.css']
})
export class CupsStatisticsComponent {

  games: Game[];
  users: User[];
  cups: Map<string, Game[]> = new Map<string, Game[]>();

  constructor(private userService: UserService,
              private gameService: GameService) {
    this.userService.getAllAscendingByName()
      .subscribe(actions => this.users = this.userService.getUsersFromActions(actions));
    this.loadAllCupGames();
  }

  loadAllCupGames(): void {
    this.gameService.setFilterKey('type');
    this.gameService.setFilterValue(Type.CUP);
    this.gameService.getAllDescendingById()
      .subscribe(actions => {
        this.games = this.gameService.getGamesFromActions(actions);
        this.createCupsMap(this.games);
      }
      );
  }

  createCupsMap(games: Game[]) {
    for (const game of this.games) {
      const temp = this.cups.get(game.event);
      if (temp) {
        temp.push(game);
        this.cups.set(game.event, temp);
      }
      this.cups.set(game.event, [game]);
    }
  }
}
