import {Component, Input, OnInit} from '@angular/core';

import {GameService} from '../../games/game.service';
import {UserService} from '../../user/user.service';
import {Game} from '../../models/boundary/game.model';
import {User} from '../../user/user';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})

export class GamesStatisticsComponent implements OnInit {

  @Input() filtered: boolean;
  @Input() games: Game[];
  @Input() displayGames: Game[];
  @Input() showDateInTypeField: boolean;
  @Input() inputClasses: string;
  @Input() preview: boolean;
  @Input() limit: number;


  users: User[];
  types = ['Test', 'Liga', 'Pokal'];
  startCount = 15;
  lastShowedDate = '';


  constructor(private userService: UserService,
              private gameService: GameService) {
    this.userService.getAllAscendingByName()
      .subscribe(actions => this.users = this.userService.getUsersFromActions(actions));
  }

  ngOnInit(): void {
    this.updateGamesList();
  }

  getGames(): void {
    // if (localStorage.getItem('games')) {
    //   if (this.limit) {
    //     this.games = JSON.parse(localStorage.getItem('games')).slice(1, 10);
    //   } else {
    //     this.games = JSON.parse(localStorage.getItem('games'));
    //   }
    // } else {
    this.gameService.getAllDescendingById()
      .subscribe(actions => {
        if (this.limit) {
          this.games = this.gameService.getGamesFromActions(actions).slice(0, 10);
        } else {
          this.games = this.gameService.getGamesFromActions(actions);
          // localStorage.setItem('games', JSON.stringify(this.games));
        }
        this.displayGames = this.games;
      });
    // }
  }

  updateGamesListWithFilter(key: string, value: string): void {
    // this.gameService.setFilterKey(key);
    // this.gameService.setFilterValue(value);
    // this.updateGamesList();

    // filter local games instead of new cb call
    this.displayGames = this.games.filter(game => value === game.players[0].name || value === game.players[1].name);

    document.querySelectorAll('select.filter:not(#filter_' + key + ')').forEach(item => {
      const htmlSelectElement = item as HTMLSelectElement;
      htmlSelectElement.value = '';
    });
  }

  updateGamesList(): void {
    if (!this.filtered) {
      this.getGames();
    }
  }


  toggleAdditionalData(): void {
    if (!this.inputClasses || !this.inputClasses.includes('open')) {
      const element = event.currentTarget as HTMLDivElement;
      const openElements = document.querySelectorAll('.game.open');
      let wasToggledToCloseSelf;

      if (openElements) {
        [].forEach.call(openElements, openElement => {
          openElement.classList.remove('open');
          if (openElement === element) {
            wasToggledToCloseSelf = true;
          }
        });
      }
      if (!wasToggledToCloseSelf) {
        element.classList.add('open');
      }
    }
  }

  resetFilters() {
    this.updateGamesListWithFilter(undefined, undefined);
  }

  nameFilterChanged(value: string) {
    this.updateGamesListWithFilter('playerName', value);
  }

  typeFilterChanged(value: string) {
    this.updateGamesListWithFilter('type', value);
  }

  showDate(date: string): boolean {
    if (this.lastShowedDate !== date) {
      this.lastShowedDate = date;
      return true;
    }
    return false;
  }

}
