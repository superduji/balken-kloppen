import {Component, OnInit} from '@angular/core';
import {Route, Router} from '@angular/router';
import {Link} from '../models/link.model';
import {LigaModel} from '../liga/ligaModel';
import {LigaService} from '../create-liga/liga.service';
import {LigaPlayerService} from '../create-liga/ligaPlayer.service';
import {LigaTablePlayerService} from '../create-liga/ligaTablePlayer.service';
import {LigaGameTableRowService} from '../create-liga/ligaGameTableRow.service';
import {Liga} from '../create-liga/liga';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
})

export class StatisticsComponent implements OnInit {

  links;
  ligaModels: LigaModel[];

  constructor(private router: Router,
              private ligaService: LigaService,
              private ligaPlayerService: LigaPlayerService,
              private ligaTablePlayerService: LigaTablePlayerService,
              private ligaGameTableRowService: LigaGameTableRowService) {

  }

  // ########################## Sub-Navi bauen ######################################

  ngOnInit() {
    // this.ligaModels = this.createLigaModels();
    this.links = this.setChildrenLinks(this.router.config);
  }

  setChildrenLinks(configuration: Route[]) {
    const links = [];
    for (const route of configuration) {
      if (route.path === 'statistiken') {
        for (const childrenRoute of route.children) {
          links.push(new Link(childrenRoute.path, childrenRoute.data['displayText'], true));
        }
      }
    }
    return links;
  }

  // ######################### Ligen zusammen bauen ###################################

  // private createLigaModels(): LigaModel[] {
  //   const ligaModels = new Array<LigaModel>();
  //
  //   this.ligaService.getAllDescendingByName().subscribe(actions => {
  //     this.ligaService.getLigenFromActions(actions).forEach(liga => {
  //       ligaModels.push(this.createLigaModel(liga));
  //     });
  //   });
  //   return ligaModels;
  // }
  //
  // private createLigaModel(liga: Liga): LigaModel {
  //   const ligaModel = new LigaModel();
  //
  //   ligaModel.setKey(liga.key);
  //   ligaModel.setName(liga.name);
  //   ligaModel.setId(liga.id);
  //   ligaModel.setFinished(liga.finished);
  //   ligaModel.setLegacy(liga.legacy);
  //
  //   liga.ligaPlayerKeys.forEach(ligaPlayerKey => {
  //     this.ligaPlayerService.getLigaPlayerObservableByKey(ligaPlayerKey).subscribe(observable => {
  //       const ligaTablePlayerKey = this.ligaPlayerService.getLigaTablePlayerKeyFromLigaPlayerObservable(observable);
  //       const ligaGameTableRowKey = this.ligaPlayerService.getLigaGameTableRowKeyFromLigaPlayerObservable(observable);
  //
  //       this.ligaTablePlayerService.getLigaTablePlayerObservableByKey(ligaTablePlayerKey).subscribe(ligaTablePlayerObservable => {
  //         ligaModel.addLigaTablePlayer(
  //           this.ligaTablePlayerService.getLigaTablePlayerFromLigaTablePlayerObservable(ligaTablePlayerObservable)
  //         );
  //
  //         this.ligaGameTableRowService.getLigaGameTableRowObservableByKey(ligaGameTableRowKey).subscribe(ligaGameTableRowObservable => {
  //           ligaModel.addLigaGameTableRow(
  //             this.ligaGameTableRowService.getLigaGameTableRowModelFromLigaGameTableRowObservable(ligaGameTableRowObservable)
  //           );
  //         });
  //       });
  //
  //     });
  //   });
  //
  //   console.log('LigaModel:', ligaModel);
  //   return ligaModel;
  // }

  orderByLigaId(ligen) {
    if (ligen) {
      return ligen.sort((a, b) => a.id < b.id ? 1 : a.id === b.id ? 0 : -1);
    }
    return '';
  }


}




