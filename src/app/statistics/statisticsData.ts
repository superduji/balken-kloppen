import {StatisticsResult} from './statisticsResult';

export class StatisticsData {

  description: string;
  data: StatisticsResult;

  constructor(description: string, data: StatisticsResult) {
    this.description = description;
    this.data = data;
  }
}
