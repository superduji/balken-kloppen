import {Component} from '@angular/core';
import {LigaService} from '../../create-liga/liga.service';
import {Liga} from '../../create-liga/liga';
import {LigaTablePlayer} from '../../create-liga/ligaTablePlayer';
import {EwigeLigaTabelleEntry} from './ewigeLigaTabelleEntry';
import {EwigeLigaTabelle} from './ewigeLigaTabelle';
import {UserService} from '../../user/user.service';
import {User} from '../../user/user';


@Component({
  selector: 'app-ewigeligatabelle',
  templateUrl: './ewigeligatabelle.component.html',
  styleUrls: ['./ewigeligatabelle.component.css']
})
export class EwigeLigaTabelleComponent {

  ligen: Liga[];
  ewigeLigaTabelle = new EwigeLigaTabelle();
  ewigeLigaTabelleEntries = new Array<EwigeLigaTabelleEntry>();
  ewigeLigaTabelleRelativeEntries = new Array<EwigeLigaTabelleEntry>();

  constructor(private ligaService: LigaService,
              private userService: UserService) {

    // get all ligen
    ligaService.getAllFinished().subscribe(ligaObservable => {
      this.ligen = ligaService.getLigenFromActions(ligaObservable);
      console.log('ligen', this.ligen);

      this.ligen.forEach(liga => {

        if (liga.official === true) {
          liga.ligaPlayers.forEach(ligaPlayer => {
            const ligaTablePlayer = ligaPlayer.ligaTablePlayer;

            // calculate
            const playerName = ligaPlayer.name;
            if (!this.ewigeLigaTabelle.entries.has(playerName)) {
              this.setEwigeLigaTabelleEntry(this.ewigeLigaTabelle.entries, playerName, ligaTablePlayer);
              this.setEwigeLigaTabelleEntry(this.ewigeLigaTabelle.relativeEntries, playerName, ligaTablePlayer);
            } else {
              this.updateEwigeLigaTabelleEntry(this.ewigeLigaTabelle.entries, playerName, ligaTablePlayer);
              this.updateEwigeLigaTabelleEntry(this.ewigeLigaTabelle.relativeEntries, playerName, ligaTablePlayer);
            }

            // sort and recalculatePositions
            this.ewigeLigaTabelleEntries = this.calculatePositions(this.sortAndGetEwigeLigaTabelleEntries(this.ewigeLigaTabelle.entries));
            this.ewigeLigaTabelleRelativeEntries
              = this.calculatePositions(this.sortAndGetEwigeLigaTabelleRelativeEntries(this.ewigeLigaTabelle.relativeEntries));
          });
        }
      });
    });

  }

  private setEwigeLigaTabelleEntry(entries, playerName, ligaTablePlayer) {
    entries.set(playerName, this.createEwigeLigaTabelleEntry(ligaTablePlayer, playerName));
  }

  private updateEwigeLigaTabelleEntry(entries, playerName, ligaTablePlayer) {
    entries.set(playerName, this.addDataFromFurtherSeason(entries.get(playerName), ligaTablePlayer));
  }

  private createEwigeLigaTabelleEntry(ligaTablePlayer: LigaTablePlayer, playerName: string): EwigeLigaTabelleEntry {
    const ewigeLigaTabelleEntry = new EwigeLigaTabelleEntry();

    this.userService.getUserByKeyAsync(playerName)
      .then(userSnapshot => userSnapshot.data() as User)
      .then(user => {
        ewigeLigaTabelleEntry.setTeam(user.team);
      });

    ewigeLigaTabelleEntry.setName(playerName);
    ewigeLigaTabelleEntry.incrementSeasons();
    ewigeLigaTabelleEntry.addPlayedGames(ligaTablePlayer.playedGames);
    ewigeLigaTabelleEntry.addPoints(ligaTablePlayer.points);
    ewigeLigaTabelleEntry.addNumberOfThrows(ligaTablePlayer.numberOfThrows);
    ewigeLigaTabelleEntry.addThrowSum(ligaTablePlayer.throwsSum);
    ewigeLigaTabelleEntry.setQuote(+(ewigeLigaTabelleEntry.throwSum / ewigeLigaTabelleEntry.numberOfThrows).toFixed(3));
    ewigeLigaTabelleEntry.addWins(ligaTablePlayer.wins);
    ewigeLigaTabelleEntry.addDraws(ligaTablePlayer.draws);
    ewigeLigaTabelleEntry.addDefeats(ligaTablePlayer.defeats);

    return ewigeLigaTabelleEntry;
  }

  private addDataFromFurtherSeason(ewigeLigaTabelleEntry: EwigeLigaTabelleEntry, ligaTablePlayer: LigaTablePlayer): EwigeLigaTabelleEntry {
    ewigeLigaTabelleEntry.incrementSeasons();
    ewigeLigaTabelleEntry.addPlayedGames(ligaTablePlayer.playedGames);
    ewigeLigaTabelleEntry.addPoints(ligaTablePlayer.points);
    ewigeLigaTabelleEntry.addNumberOfThrows(ligaTablePlayer.numberOfThrows);
    ewigeLigaTabelleEntry.addThrowSum(ligaTablePlayer.throwsSum);
    ewigeLigaTabelleEntry.setQuote(+(ewigeLigaTabelleEntry.throwSum / ewigeLigaTabelleEntry.numberOfThrows).toFixed(3));
    ewigeLigaTabelleEntry.addWins(ligaTablePlayer.wins);
    ewigeLigaTabelleEntry.addDraws(ligaTablePlayer.draws);
    ewigeLigaTabelleEntry.addDefeats(ligaTablePlayer.defeats);

    return ewigeLigaTabelleEntry;
  }

  private sortAndGetEwigeLigaTabelleEntries(ewigeLigaTabelleEntries: Map<string, EwigeLigaTabelleEntry>): Array<EwigeLigaTabelleEntry> {

    const ewigeLigaTabelleEntryArray = Array.from(ewigeLigaTabelleEntries.values()).sort((a, b) => {
      if (a.points > b.points) {
        return -1;
      }
      if (a.points < b.points) {
        return 1;
      }

      if (a.quote > b.quote) {
        return -1;
      }
      if (a.quote < b.quote) {
        return 1;
      }

      if (a.playedGames < b.playedGames) {
        return -1;
      }
      if (a.playedGames > b.playedGames) {
        return 1;
      }

      return 0;
    });
    return ewigeLigaTabelleEntryArray;
  }

  private sortAndGetEwigeLigaTabelleRelativeEntries(ewigeLigaTabelleRelativeEntries: Map<string, EwigeLigaTabelleEntry>): Array<EwigeLigaTabelleEntry> {

    const ewigeLigaTabelleRelativeEntriesArray = Array.from(ewigeLigaTabelleRelativeEntries.values());

    ewigeLigaTabelleRelativeEntriesArray.forEach(entry => {
      entry.setPointsRelative(+(entry.points / entry.playedGames).toFixed(3));
      entry.setNumberOfThrowsRelative(+(entry.numberOfThrows / entry.playedGames).toFixed(3));
      entry.setThrowSumRelative(+(entry.throwSum / entry.playedGames).toFixed(3));
      entry.setWinsRelative(+(entry.wins / entry.playedGames).toFixed(3));
      entry.setDrawsRelative(+(entry.draws / entry.playedGames).toFixed(3));
      entry.setDefeatsRelative(+(entry.defeats / entry.playedGames).toFixed(3));
    });

    return ewigeLigaTabelleRelativeEntriesArray.sort((a, b) => {
      if (a.pointsRelative > b.pointsRelative) {
        return -1;
      }
      if (a.pointsRelative < b.pointsRelative) {
        return 1;
      }

      if (a.quote > b.quote) {
        return -1;
      }
      if (a.quote < b.quote) {
        return 1;
      }

      if (a.playedGames < b.playedGames) {
        return -1;
      }
      if (a.playedGames > b.playedGames) {
        return 1;
      }

      return 0;
    });
  }

  private calculatePositions(ewigeLigaTabelleEntries: Array<EwigeLigaTabelleEntry>): Array<EwigeLigaTabelleEntry> {

    ewigeLigaTabelleEntries.forEach((entry, index) => {
      entry.setPosition(index + 1);
    });

    return ewigeLigaTabelleEntries;
  }

  getTeam(player: EwigeLigaTabelleEntry): string {
    let returnValue = '';
    if (player && player.team) {
      returnValue = player.team;
    }
    return returnValue;
  }

}
