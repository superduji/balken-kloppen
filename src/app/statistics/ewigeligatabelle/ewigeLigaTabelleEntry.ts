export class EwigeLigaTabelleEntry {

  name: string;
  team: string;
  position = 0;

  seasons = 0;
  playedGames = 0;
  points = 0;
  pointsRelative = 0;

  numberOfThrows = 0;
  numberOfThrowsRelative = 0;
  throwSum = 0;
  throwSumRelative = 0;
  quote = 0;

  wins = 0;
  winsRelative = 0;
  draws = 0;
  drawsRelative = 0;
  defeats = 0;
  defeatsRelative = 0;

  setName(name: string) {
    this.name = name;
  }

  setTeam(team: string) {
    this.team = team;
  }

  setPosition(position: number) {
    this.position = position;
  }

  incrementSeasons() {
    this.seasons ++;
  }

  addPlayedGames(playedGames: number) {
    this.playedGames += playedGames;
  }

  addPoints(points: number) {
    this.points += points;
  }
  setPointsRelative(pointsRelative: number) {
    this.pointsRelative = pointsRelative;
  }

  addNumberOfThrows(numberOfThrows: number) {
    this.numberOfThrows += numberOfThrows;
  }
  setNumberOfThrowsRelative(numberOfThrowsRelative: number) {
    this.numberOfThrowsRelative = numberOfThrowsRelative;
  }

  addThrowSum(throwSum: number) {
    this.throwSum += throwSum;
  }
  setThrowSumRelative(throwSumRelative: number) {
    this.throwSumRelative = throwSumRelative;
  }

  setQuote(quote: number) {
    this.quote = quote;
  }

  addWins(wins: number) {
    this.wins += wins;
  }
  setWinsRelative(winsRelative: number) {
    this.winsRelative = winsRelative;
  }

  addDraws(draws: number) {
    this.draws += draws;
  }
  setDrawsRelative(drawsRelative: number) {
    this.drawsRelative = drawsRelative;
  }

  addDefeats(defeats: number) {
    this.defeats += defeats;
  }
  setDefeatsRelative(defeatsRelative: number) {
    this.defeatsRelative = defeatsRelative;
  }
}
