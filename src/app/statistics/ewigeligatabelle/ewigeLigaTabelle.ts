import {EwigeLigaTabelleEntry} from './ewigeLigaTabelleEntry';

export class EwigeLigaTabelle {

  entries = new Map<string, EwigeLigaTabelleEntry>();
  relativeEntries = new Map<string, EwigeLigaTabelleEntry>();

  setEntries(entries: Map<string, EwigeLigaTabelleEntry>) {
    this.entries = entries;
  }

  setRelativeEntriess(relativeEntries: Map<string, EwigeLigaTabelleEntry>) {
    this.relativeEntries = relativeEntries;
  }
}
