import {LigaGameTableGame} from '../create-liga/ligaGameTableGame';

export class LigaGameTableGameModel {

  key: string;
  ligaPlayerKey: string;
  round = '';
  rowPlayerName: string;
  rowPlayerKey: string;
  columnPlayerName: string;
  columnPlayerKey: string;
  gameRef = 'tbp.';
  playable = true;
  result: string;
  resultPointsPlayerA: string;
  resultPointsPlayerB: string;
  resultThrowsPlayerA: string;
  resultThrowsPlayerB: string;


  setKey(key: string) {
    this.key = key;
  }

  setLigaPlayerKey(ligaPlayerKey: string) {
    this.ligaPlayerKey = ligaPlayerKey;
  }

  setRound(round: string) {
    this.round = round;
  }

  setRowPlayerName(rowPlayerName: string) {
    this.rowPlayerName = rowPlayerName;
  }

  setRowPlayerKey(rowPlayerKey: string) {
    this.rowPlayerKey = rowPlayerKey;
  }

  setColumnPlayerName(columnPlayerName: string) {
    this.columnPlayerName = columnPlayerName;
  }

  setColumnPlayerKey(columnPlayerKey: string) {
    this.columnPlayerKey = columnPlayerKey;
  }

  setGameRef(gameRef: string) {
    this.gameRef = gameRef;
  }

  setPlayable(playable: boolean) {
    this.playable = playable;
  }

  constructor(ligaGameTableGame: LigaGameTableGame) {
    this.rowPlayerName = ligaGameTableGame.rowPlayerName;
    this.rowPlayerKey = ligaGameTableGame.rowPlayerRef;
    this.columnPlayerName = ligaGameTableGame.columnPlayerName;
    this.columnPlayerKey = ligaGameTableGame.columnPlayerRef;
    this.gameRef = ligaGameTableGame.gameRef;
    this.playable = ligaGameTableGame.playable;
  }

}
