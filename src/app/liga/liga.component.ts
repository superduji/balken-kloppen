import {AfterContentChecked, Component} from '@angular/core';
import {Liga} from '../create-liga/liga';
import {LigaService} from '../create-liga/liga.service';
import {Router} from '@angular/router';
import {KloppenService} from '../kloppen/kloppen.service';
import {UserService} from '../user/user.service';
import {LoginService} from '../login/login.service';
import {LigaPlayer} from '../create-liga/ligaPlayer';
import {LigaGameTableGame} from '../create-liga/ligaGameTableGame';
import {GameService} from '../games/game.service';
import {Game as GameModel, Game} from '../models/boundary/game.model';
import {LigaGameTableRow} from '../create-liga/ligaGameTableRow';
import {LigaTablePlayer} from '../create-liga/ligaTablePlayer';

@Component({
  selector: 'app-liga',
  templateUrl: './liga.component.html',
  styleUrls: ['./liga.component.css']
})
export class LigaComponent implements AfterContentChecked {

  loginService: LoginService;

  ligen = new Array<Liga>();
  liga = new Liga();
  initialLiga: Liga;

  tablePlayers = new Array<LigaTablePlayer>();
  tablePlayersFirstRound = new Array<LigaTablePlayer>();
  tablePlayersSecondRound = new Array<LigaTablePlayer>();
  rows = new Array<LigaGameTableRow>();

  gamesOfLiga = new Array<Game>();

  // game details
  showGameDetails = false;
  games = [];
  showDateInTypeField = true;
  refForOpenGameDetail = '';

  displayNamesForMultiWordPlayerNames = new Map<string, string>();

  users;

  constructor(private ligaService: LigaService,
              private kloppenService: KloppenService,
              private router: Router,
              private userService: UserService,
              private gameService: GameService,
              loginService: LoginService) {
    this.loginService = loginService;
    this.ligaService.getAllAscendingById().subscribe(actions => {
      this.ligaService.getLigenFromActions(actions).forEach((liga, index) => {
        this.ligen.push(liga);
        if (index === actions.length - 1) {
          this.liga = liga;
          this.updateModelData(liga);
        }
      });
    });

    this.userService.getAllAscendingByName()
      .subscribe(actions => this.users = this.userService.getUsersFromActions(actions));

  }

  private updateModelData(liga: Liga) {
    this.initialLiga = liga;
    this.rows = this.getRows(liga);
    this.tablePlayers = this.getTablePlayers(liga.ligaPlayers);
    this.tablePlayersFirstRound = this.getTablePlayers(liga.ligaPlayers);
    this.tablePlayersSecondRound = this.getTablePlayers(liga.ligaPlayers);

    liga.ligaPlayers.forEach(ligaPlayer => {
      ligaPlayer.ligaGameTableRow.ligaGameTableGames.forEach(ligaGameTableGame => {
        const ref = ligaGameTableGame.gameRef;
        if (ref !== 'tbp.') {
          this.gameService.getGameByKeyAsync(ref)
            .then(gameSnapshot => {
              const game = gameSnapshot.data() as Game;
              if (game) {
                game.key = gameSnapshot.id;
                this.gamesOfLiga.push(game);
              }
            });
        }
      });
    });
  }

  private getRows(liga: Liga): LigaGameTableRow[] {
    const rows = [];
    if (liga) {
      liga.ligaPlayers.forEach(ligaPlayer => {
        rows.push(ligaPlayer.ligaGameTableRow);
      });
    }
    return rows;
  }

  private getTablePlayers(ligaPlayers: LigaPlayer[]): LigaTablePlayer[] {
    const ligaTablePlayer = [];
    if (ligaPlayers) {
      ligaPlayers.forEach(ligaPlayer => {
        ligaTablePlayer.push(ligaPlayer.ligaTablePlayer);
      });
    }

    return ligaTablePlayer;
  }

  ngAfterContentChecked(): void {
    if (this.initialLiga) {
      if (this.initialLiga.official) {
        (document.querySelector('#filter_name_season') as HTMLSelectElement).value = this.initialLiga.name;
      } else {
        (document.querySelector('#filter_name_event') as HTMLSelectElement).value = this.initialLiga.name;
      }
    }
  }

  orderByLigaId(ligen: Liga[], official: boolean): Liga[] {
    if (ligen) {
      const allLigen = ligen.sort((a, b) => a.id < b.id ? 1 : a.id === b.id ? 0 : -1);
      return allLigen.filter(liga => liga.official === official);
    }
    return [];
  }

  orderByPosition(ligaTablePlayers: LigaTablePlayer[]): LigaTablePlayer[] {
    if (ligaTablePlayers) {
      ligaTablePlayers.sort((a, b) => a.position > b.position
        ? 1
        : a.position === b.position
          ? 0
          : -1);
    }
    return ligaTablePlayers;
  }

  getTeam(playerName: string): string {
    let returnValue = '';
    this.liga.ligaPlayers.forEach(ligaPlayer => {
      if (ligaPlayer.name === playerName) {
        returnValue = ligaPlayer.team;
      }
    });
    return returnValue;
  }

  orderByPositionFirstRound(ligaTablePlayers: LigaTablePlayer[]): LigaTablePlayer[] {
    if (ligaTablePlayers) {
      ligaTablePlayers.sort((a, b) => a.positionFirstRound > b.positionFirstRound
        ? 1
        : a.positionFirstRound === b.positionFirstRound
          ? 0
          : -1);
    }
    return ligaTablePlayers;
  }

  orderByPositionSecondRound(ligaTablePlayers: LigaTablePlayer[]): LigaTablePlayer[] {
    if (ligaTablePlayers) {
      ligaTablePlayers.sort((a, b) => a.positionSecondRound > b.positionSecondRound
        ? 1
        : a.positionSecondRound === b.positionSecondRound
          ? 0
          : -1);
    }
    return ligaTablePlayers;
  }

  getColumnPlayerNames(liga: Liga): string[] {
    const columnPlayerNames = new Array<string>();
    if (liga.ligaPlayers) {
      liga.ligaPlayers[0].ligaGameTableRow.ligaGameTableGames.forEach(ligaGameTableGame => {
        columnPlayerNames.push(ligaGameTableGame.columnPlayerName);
      });
    }
    return columnPlayerNames;
  }

  private onCellClick(game: LigaGameTableGame, ligaName: string) {
    if (this.loginService.isUserLoggedIn() && game.gameRef === 'tbp.') {
      const players = [];

      // Find users by name
      players.push(this.users.find(user => user.name === game.rowPlayerName));
      players.push(this.users.find(user => user.name === game.columnPlayerName));

      // Add relevant data to KloppenService
      this.kloppenService.setType(Type.LEAGUE);
      this.kloppenService.setEvent(`${Type.LEAGUE} ${ligaName}`);
      this.kloppenService.setDate(new Date().toISOString().substr(0, 10));
      this.kloppenService.setPlayers(players);

      this.router.navigate(['/addGame']);
    } else {
      if (this.refForOpenGameDetail !== game.gameRef) {
        this.showGameData(game.gameRef);
      } else {
        this.games = [];
        this.refForOpenGameDetail = '';
        this.showGameDetails = false;
      }
    }
  }

  showGameData(gameRef: string) {
    this.gameService.getGameByKeyAsync(gameRef)
      .then(gameSnapshot => {
        this.games = [];

        const game = gameSnapshot.data() as GameModel;
        game.key = gameRef;
        game.players.forEach(player => {
          player.isWinner = game.winner === player.name;
          player.numberOfThrows = player.throws ? player.throws.length : 0;
          player.average = parseFloat(((15 - player.rest) / player.numberOfThrows).toFixed(3));
        });

        this.refForOpenGameDetail = game.key;
        this.showGameDetails = true;
        this.games.push(game);
      });
  }

  getResult(ligaGameTableGame: LigaGameTableGame): string {
    let returnValue = '';

    this.gamesOfLiga.forEach(game => {
      if (game.key === ligaGameTableGame.gameRef) {
        if (game.draw) {
          returnValue = '=';
        } else if (game.players[0].name === game.winner) {
          returnValue = '&larr;';
        } else if (game.players[1].name === game.winner) {
          returnValue = '&uarr;';
        }
      }
    });
    return returnValue;
  }

  getResultPointsForPlayer(ligaGameTableGame: LigaGameTableGame, player: string): string {
    let returnValue = '';

    this.gamesOfLiga.forEach(game => {
      if (game.key === ligaGameTableGame.gameRef) {
        if (player === 'A') {
          returnValue = '' + (15 - game.players[0].rest);
        } else if (player === 'B') {
          returnValue = '' + (15 - game.players[1].rest);
        }
      }
    });
    return returnValue;
  }

  getResultThrowsForPlayer(ligaGameTableGame: LigaGameTableGame, player: string): string {
    let returnValue = '';

    this.gamesOfLiga.forEach(game => {
      if (game.key === ligaGameTableGame.gameRef) {
        if (player === 'A') {
          returnValue = '' + game.players[0].throws.length;
        } else if (player === 'B') {
          returnValue = '' + game.players[1].throws.length;
        }
      }
    });
    return returnValue;
  }

  // ################################### Views ###############################################
  toggleDetails() {
    const element = event.currentTarget as HTMLDivElement;
    const openLigaElements = document.querySelectorAll('.liga.open');
    let wasToggledToCloseSelf;

    if (openLigaElements) {
      [].forEach.call(openLigaElements, openLigaElement => {
        openLigaElement.classList.remove('open');
        openLigaElement.querySelector('.opener-icon').innerHTML = '&#8230;';
        if (openLigaElement === element.closest('.liga')) {
          wasToggledToCloseSelf = true;
        }
      });
    }
    if (!wasToggledToCloseSelf) {
      element.closest('.liga').classList.add('open');
      element.innerHTML = '&#8285;';
    }
  }

  nameFilterChanged(value: string): void {
    document.querySelectorAll('.liga.open').forEach(openLigaElement => {
      openLigaElement.classList.remove('open');
      openLigaElement.querySelector('.opener-icon').innerHTML = '&#8230;';
    });
    const BreakException = {};

    this.liga = this.ligen[0];

    try {
      this.ligen.forEach(liga => {
        if (value === liga.name) {
          this.liga = liga;
          this.updateModelData(liga);
          if (liga.official) {
            (document.querySelector('#filter_name_event') as HTMLSelectElement).value = 'default';
          } else {
            (document.querySelector('#filter_name_season') as HTMLSelectElement).value = 'default';
          }

          throw BreakException;
        }
      });
    } catch (e) {
      if (e !== BreakException) {
        // do nothing
      }
    }
  }

  getName(playerName: string): string {
    if (playerName && playerName.indexOf(' ') > -1) {
      if (this.displayNamesForMultiWordPlayerNames.get(playerName)) {
        return this.displayNamesForMultiWordPlayerNames.get(playerName);
      }

      const partsOfName = playerName.split(' ');
      const randomIndex = Math.floor(Math.random() * partsOfName.length);
      let pre = '';
      let post = '';
      if (randomIndex === 0) {
        post = ' ...';
      } else if (randomIndex === partsOfName.length - 1) {
        pre = '... ';
      } else {
        pre = '... ';
        post = ' ...';
      }

      const displayName = pre + partsOfName[randomIndex] + post;
      this.displayNamesForMultiWordPlayerNames.set(playerName, displayName);
      return displayName;
    } else {
      return playerName;
    }
  }

}
