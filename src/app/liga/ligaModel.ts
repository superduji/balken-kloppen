import {LigaTablePlayer} from '../create-liga/ligaTablePlayer';
import {LigaGameTableRowModel} from './ligaGameTableRowModel';

export class LigaModel {
  key: string;
  name: string;
  id: number;
  legacy: boolean;
  ligaTablePlayers = new Array<LigaTablePlayer>();
  ligaGameTableRows = new Array<LigaGameTableRowModel>();
  finished: boolean;

  setKey(key: string) {
    this.key = key;
  }

  setName(name: string) {
    this.name = name;
  }

  setId(id: number) {
    this.id = id;
  }

  setLegacy(legacy: boolean) {
    this.legacy = legacy;
  }

  addLigaTablePlayer(ligaTablePlayer: LigaTablePlayer) {
    this.ligaTablePlayers.push(ligaTablePlayer);
  }

  addLigaGameTableRow(ligaGameTableRow: LigaGameTableRowModel) {
    this.ligaGameTableRows.push(ligaGameTableRow);
  }

  setFinished(finished: boolean) {
    this.finished = finished;
  }
}
