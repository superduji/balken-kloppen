import {LigaGameTableGameModel} from './ligaGameTableGameModel';

export class LigaGameTableRowModel {

  key: string;
  ligaPlayerKey: string;
  ligaGameTableGames = new Array<LigaGameTableGameModel>();

  setKey(key: string) {
    this.key = key;
  }

  setLigaPlayerKey(ligaPlayerKey: string) {
    this.ligaPlayerKey = ligaPlayerKey;
  }

  addLigaGameTableGame(ligaGameTableGame: LigaGameTableGameModel) {
    this.ligaGameTableGames.push(ligaGameTableGame);
  }

}
