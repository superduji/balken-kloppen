import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Liga} from '../create-liga/liga';
import {LigaPlayerService} from '../create-liga/ligaPlayer.service';
import {LigaModel} from './ligaModel';
import {LigaService} from '../create-liga/liga.service';

@Injectable({
  providedIn: 'root'
})
export class LigenService {

  ligen: LigaModel[];

  constructor(private angularFirestore: AngularFirestore,
              private ligaPlayerService: LigaPlayerService,
              private ligaService: LigaService) {
  }


  // ######################### GET ##################################

  getAllLigenAscendingById(): LigaModel[] {
    this.createLigaModels();
    return this.ligen;
  }

  private createLigaModels() {
    const ligaModels = new Array<LigaModel>();

    // get Ligen
    this.ligaService.getAllAscendingById().subscribe(actions => {
      this.ligaService.getLigenFromActions(actions).forEach((liga, index) => {
        ligaModels.push(this.createLigaModel(liga));

        if (index === actions.length - 1) {
          this.ligen = this.orderByLigaId(ligaModels)[0];
        }
      });
    });
  }

  private createLigaModel(liga: Liga): LigaModel {
    const ligaModel = new LigaModel();

    ligaModel.setKey(liga.key);
    ligaModel.setName(liga.name);
    ligaModel.setId(liga.id);
    ligaModel.setFinished(liga.finished);
    ligaModel.setLegacy(liga.legacy);

    console.log('LigaModel:', ligaModel);
    return ligaModel;
  }

  private orderByLigaId(ligen) {
    if (ligen) {
      return ligen.sort((a, b) => a.id < b.id ? 1 : a.id === b.id ? 0 : -1);
    }
    return '';
  }

}
