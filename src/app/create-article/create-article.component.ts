import {Component, OnInit} from '@angular/core';
import {Article} from '../news/article';
import {ArticleService} from '../news/article.service';
import {LoginService} from '../login/login.service';
import {UploadService} from '../uploads/shared/upload.service';
import {NavigationService} from '../navigation/navigation.service';
import {Link} from '../models/link.model';
import {Paragraph} from '../news/paragraph';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {

  loginService: LoginService;

  today: string = new Date().toISOString().substr(0, 10);
  article: Article = new Article(
    null,
    '',
    '',
    '',
    [new Paragraph('', '')],
    '',
    '',
    '',
    false,
    false,
    '',
    '',
    this.today,
    Date.now().toString(10),
    '',
    null
  );
  articles;
  error = false;
  success = false;
  texts = {
    headline: 'Headline',
    subline: 'Subline',
    paragraph: 'Text',
    paragraphPlaceholder: 'Absatz 1 Text',
    interviewPrefaceLabel: 'Interview-Vorwort',
    interviewPrefacePlaceholder: 'Dieser Text sollte maximal etwa 100 Zeichen lang sein.',
    image: 'Bildpfad (optimal 1000x553)',
    imageCredit: 'Bild-Urheber',
    link: 'Link',
    button: 'Reinkloppen',
    required: 'Pflichtfeld',
    error: 'Bitte alle Pflichtfelder ausfüllen!',
    success: 'Artikel erfolgreich reingekloppt!',
    plus: 'BK+',
    interview: 'Interview',
    intervieweeLabel: 'Interviewte',
    author: 'Autor'
  };
  image = {
    path: 'https://firebasestorage.googleapis.com/v0/b/balken-kloppen.appspot.com/o/placeholder.png?alt=media&token=d1a9b3db-f0da-4925-8b36-990d60bb6ffc',
    uploaded: false
  };
  naviLinks;

  constructor(private articleService: ArticleService,
              loginService: LoginService,
              private navigationService: NavigationService,
              private  router: Router,
              private uploadService: UploadService) {
    this.loginService = loginService;
    this.articleService.getAllArticles()
      .subscribe(actions => this.articles = this.articleService.getArticlesFromActions(actions));
    this.naviLinks = this.navigationService.getNaviLinks();
  }

  ngOnInit() {
  }

  save() {
    if (this.article.headline && this.article.subline
      && this.article.paragraphs[0].text && this.image.uploaded
      && this.article.imageCredit && this.article.author) {

      if (this.article.interview && !(this.article.interviewPreface && this.article.interviewee)) {
        this.success = false;
        this.error = true;
        console.log('FEHLER BEIM ERSTELLEN EINES ARTIKELS', this.article);
      } else {
        this.article.id = this.articles[0].id + 1;
        this.article.image = this.image.path;
        this.article.headline = this.replaceNames(this.article.headline);
        this.article.subline = this.replaceNames(this.article.subline);
        this.article.paragraph = this.replaceNames(this.article.paragraph);
        this.article.interviewPreface = this.replaceNames(this.article.interviewPreface);
        this.article.intervieweeInitials = this.getInitialsFrom(this.article.interviewee);
        this.articleService.createArticle(this.article);

        console.log('ARTIKEL REINGEKLOPPT', this.article);
        this.router.navigate(['/news']);
      }
    } else {
      this.success = false;
      this.error = true;
      console.log('FEHLER BEIM ERSTELLEN EINES ARTIKELS', this.article);
    }
  }

  getInitialsFrom(name: string): string {
    let initials = '';
    if (name) {
      initials = name.match(/\b(\w)/g).join('').toUpperCase();
      console.log(name, initials);
    }
    return initials;
  }

  replaceNames(name) {
    const mapObj = {
      IBKA: 'IBKA®',
      ibka: 'IBKA®',
      Liga: 'IBKA® ThyssenKrupp Magnettechnik-Liga',
      liga: 'IBKA® ThyssenKrupp Magnettechnik-Liga',
    };

    const re = new RegExp(Object.keys(mapObj).join('|'), 'gi');
    name = name.replace(re, (matched) => {
      return mapObj[matched];
    });
    return name;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.uploadService.upload(files[i]).then((snapshot) => {
        snapshot.ref.getDownloadURL().then((downloadURL) => {
          console.log('File available at', downloadURL);
          this.image.path = downloadURL;
          this.image.uploaded = true;
        });
      });
    }
  }

  getLink(naviLink: Link) {
    if (naviLink.intern) {
      return `/${naviLink.href}`;
    }
    return naviLink.href;
  }

  addParagraph(): void {
    this.article.paragraphs.push(new Paragraph('', ''));
  }

  removeParagraph(): void {
    this.article.paragraphs.pop();
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  updateHeadline($event, paragraph: Paragraph) {
    const value = $event.target.value;
    paragraph.headline = value;
  }

  updateParagraph($event, paragraph: Paragraph) {
    const value = $event.target.value;
    paragraph.text = value;
  }
}
