import {ExtraOptions, RouterModule, Routes} from '@angular/router';

import {KloppenComponent} from './kloppen/kloppen.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {BalkloppistanStatisticsComponent} from './statistics/balkloppistan/balkloppistan.component';
import {CupsStatisticsComponent} from './statistics/cups/cups.component';
import {DuelsStatisticsComponent} from './statistics/duels/duels.component';
import {GamesStatisticsComponent} from './statistics/games/games.component';
import {LigaComponent} from './liga/liga.component';
import {PlayersStatisticsComponent} from './statistics/players/players.component';
import {WorldRankingStatisticsComponent} from './statistics/worldranking/worldranking.component';
import {EwigeLigaTabelleComponent} from './statistics/ewigeligatabelle/ewigeligatabelle.component';
import {WurfquotenStatisticsComponent} from './statistics/wurfquoten/wurfquoten.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PageNotFoundComponent} from './error/page-not-found.component';
import {CreateGameComponent} from './games/create-game/create-game.component';
import {UserComponent} from './user/user.component';
import {CreateLigaComponent} from './create-liga/create-liga.component';
import {LoginComponent} from './login/login.component';
import {LigenStatisticsComponent} from './statistics/ligen/ligen.component';
import {NewsComponent} from './news/news.component';
import {FurtherLinksComponent} from './further-links/further-links.component';
import {BalkloppistanComponent} from './balkloppistan/balkloppistan.component';
import {CreateBalkloppistanComponent} from './create-balkloppistan/create-balkloppistan.component';
import {BalkloppistanGameComponent} from './balkloppistan-game/balkloppistan-game.component';
import {CupComponent} from './cup/cup.component';
import {CreateCupComponent} from './create-pokal/create-cup.component';
import {RulesComponent} from './rules/rules.component';
import {ArticleComponent} from './article/article.component';
import {CreateArticleComponent} from './create-article/create-article.component';

const appRoutes: Routes = [
  {
    path: 'start',
    component: DashboardComponent,
    data: {
      displayText: 'Home'
    }
  },
  {
    path: 'news',
    component: NewsComponent,
    data: {
      displayText: 'News'
    }
  },
  {
    path: 'create-article',
    component: CreateArticleComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'klopperInnen',
    component: UserComponent,
    data: {
      displayText: 'Klopper*Innen'
    }
  },
  {
    path: 'kloppen',
    component: KloppenComponent,
    data: {
      displayText: 'Kloppen',
      loginRequired: true
    }
  },
  {
    path: 'liga',
    component: LigaComponent,
    data: {
      displayText: 'Liga'
    }
  },
  {
    path: 'createLiga',
    component: CreateLigaComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'pokal',
    component: CupComponent,
    data: {
      displayText: 'Pokal'
    }
  },
  {
    path: 'balkloppistan',
    component: BalkloppistanComponent,
    data: {
      displayText: 'BalKloppistan'
    }
  },
  {
    path: 'createBalkloppistan',
    component: CreateBalkloppistanComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'balkloppistan-game',
    component: BalkloppistanGameComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'createPokal',
    component: CreateCupComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'statistiken',
    component: StatisticsComponent,
    children: [
      {
        path: 'spieler',
        component: PlayersStatisticsComponent,
        data: {
          displayText: 'Spieler-Statistiken'
        }
      },
      {
        path: 'duelle',
        component: DuelsStatisticsComponent,
        data: {
          displayText: 'Duelle'
        }
      },
      {
        path: 'spiele',
        component: GamesStatisticsComponent,
        data: {
          displayText: 'Spiele (15er)'
        }
      },
      {
        path: 'ligen',
        component: LigenStatisticsComponent,
        data: {
          displayText: 'Ligen'
        }
      },
      {
        path: 'ewigeligatabelle',
        component: EwigeLigaTabelleComponent,
        data: {
          displayText: 'Ewige Ligatabelle'
        }
      },
      {
        path: 'pokale',
        component: CupsStatisticsComponent,
        data: {
          displayText: 'Pokale'
        }
      },
      {
        path: 'weltrangliste',
        component: WorldRankingStatisticsComponent,
        data: {
          displayText: 'Weltrangliste'
        }
      },
      {
        path: 'balkloppistan',
        component: BalkloppistanStatisticsComponent,
        data: {
          displayText: 'BalKloppistan'
        }
      },
      {
        path: 'wurfquoten',
        component: WurfquotenStatisticsComponent,
        data: {
          displayText: 'Wurfquoten'
        }
      }
    ],
    data: {
      displayText: 'Statistiken'
    }
  },
  {
    path: 'regelwerk',
    component: RulesComponent,
    data: {
      displayText: 'Regelwek',
      hide: true
    }
  },
  {
    path: 'start',
    component: DashboardComponent,
    pathMatch: 'full',
    data: {
      hide: true
    }
  },
  {
    path: 'addGame',
    component: CreateGameComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'links',
    component: FurtherLinksComponent,
    data: {
      displayText: 'Links',
      loginRequired: true
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      displayText: 'Login',
    }
  },
  {
    path: 'article/:id',
    component: ArticleComponent,
    data: {
      hide: true
    }
  },
  {
    path: '',
    redirectTo: '/start',
    pathMatch: 'full',
    data: {}
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    data: {
      hide: true
    }
  }
];

const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  onSameUrlNavigation: 'reload'
};

export const routing = RouterModule.forRoot(appRoutes, routerOptions);
