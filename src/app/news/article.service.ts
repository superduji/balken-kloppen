import {EventEmitter, Injectable} from '@angular/core';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {Article} from './article';
import {DbService} from '../utils/db.service';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private db: AngularFirestore;
  private dbPath = '/articles';
  public cookieChanged$: EventEmitter<string>;

  LAST_VIEWED_COOKIE_NAME = 'viewedArticles';

  constructor(private angularFirestore: AngularFirestore,
              private dbService: DbService,
              private cookieService: CookieService) {
    this.db = angularFirestore;
    this.cookieChanged$ = new EventEmitter();
  }

  createArticle(article: Article): void {
    this.dbService.setDocumentWithCustomKey(this.db, this.dbPath, article.id.toString(), article);
  }

  getAllArticles(): Observable<DocumentChangeAction<Article>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('id', 'desc'))
      .snapshotChanges() as Observable<DocumentChangeAction<Article>[]>;
  }

  getArticlesFromActions(actions: DocumentChangeAction<Article>[]): Article[] {
    return actions.map(this.getArticleFromAction);
  }

  private getArticleFromAction(action: DocumentChangeAction<Article>): Article {
    const article = action.payload.doc.data() as Article;
    return article;
  }

  updateArticle(article: Article): void {
    this.dbService.updateDocument(this.db, this.dbPath, article.id.toString(), article);
  }

  updateArticlesCookie(id: number): void {
    const idString = id.toString();
    if (this.cookieService.check(this.LAST_VIEWED_COOKIE_NAME)) {
      const lastViewedCookieValueArray = this.cookieService.get(this.LAST_VIEWED_COOKIE_NAME).split(',');
      if (!lastViewedCookieValueArray.includes(idString)) {
        lastViewedCookieValueArray.push(idString);
        this.cookieService.set(
          this.LAST_VIEWED_COOKIE_NAME,
          lastViewedCookieValueArray.join(','),
          null,
          '/'
        );
      }
    } else {
      this.cookieService.set(
        this.LAST_VIEWED_COOKIE_NAME,
        idString,
        null,
        '/'
      );
    }
    this.cookieChanged$.emit('bla');
  }

  removeArticleIdFromCookie(id: number): void {
    const idString = id.toString();
    if (this.cookieService.check(this.LAST_VIEWED_COOKIE_NAME)) {
      const lastViewedCookieValueArray = this.cookieService.get(this.LAST_VIEWED_COOKIE_NAME).split(',');
      const index = lastViewedCookieValueArray.indexOf(idString);
      if (index > -1) {
        lastViewedCookieValueArray.splice(index, 1);
      }

      if (lastViewedCookieValueArray.length === 0) {
        this.cookieService.delete(this.LAST_VIEWED_COOKIE_NAME, '/');
      } else {
        this.cookieService.set(
          this.LAST_VIEWED_COOKIE_NAME,
          lastViewedCookieValueArray.join(','),
          null,
          '/'
        );
      }

      this.cookieChanged$.emit('bla');
    }
  }
}
