import {Paragraph} from './paragraph';
import {Comment} from '../article/comment';

export class Article {
  id: number;
  headline: string;
  subline: string;
  paragraph: string;
  paragraphs: Paragraph[];
  image: string;
  imageCredit: string;
  link: string;
  plus: boolean;
  interview: boolean;
  interviewPreface: string;
  interviewee: string;
  intervieweeInitials = '';
  date: string;
  createTimeStamp: string;
  author: string;
  comments: Comment[];

  constructor(id: number,
              headline: string,
              subline: string,
              paragraph: string,
              paragraphs: Paragraph[],
              image: string,
              imageCredit: string,
              link: string,
              plus: boolean,
              interview: boolean,
              interviewPreface: string,
              interviewee: string,
              date: string,
              createTimeStamp: string,
              author: string,
              comments: Comment[]) {
    this.id = id;
    this.headline = headline;
    this.subline = subline;
    this.paragraph = paragraph;
    this.paragraphs = paragraphs;
    this.image = image;
    this.imageCredit = imageCredit;
    this.link = link;
    this.plus = plus;
    this.interview = interview;
    this.interviewPreface = interviewPreface;
    this.interviewee = interviewee;
    if (this.interviewee) {
      this.intervieweeInitials = interviewee.match(/\b(\w)/g).join('').toUpperCase();
      console.log(this.intervieweeInitials);
    }
    this.date = date;
    this.createTimeStamp = createTimeStamp;
    this.author = author;
    this.comments = comments;
  }
}
