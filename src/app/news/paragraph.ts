export class Paragraph {

  headline: string;
  text: string;

  constructor(headline: string, text: string) {
    this.headline = headline;
    this.text = text;
  }
}
