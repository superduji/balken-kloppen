import {Component, Input, OnInit} from '@angular/core';
import {ArticleService} from './article.service';
import {LoginService} from '../login/login.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Article} from './article';
import {CookieService} from 'ngx-cookie-service';
import {Comment} from '../article/comment';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(400)),
    ]),
  ]
})
export class NewsComponent implements OnInit {

  loginService: LoginService;

  articles: Article[];
  displayArticles: Article[];

  staticTexts = {
    photoCopyright: 'Foto ©',
    readMore: 'lesen',
    plus: 'BK+',
    author: 'Author',
    searchToggle: {
      true: 'Suche ausblenden',
      false: 'Suche anzeigen'
    }
  };

  showArticleInput = false;

  defaultImage = 'https://firebasestorage.googleapis.com/v0/b/balken-kloppen.appspot.com/o/placeholder-image.png?'
    + 'alt=media&token=b0d8ab73-9d39-4dcb-bd50-c7b468573afe';

  LAST_VIEWED_TIMESTAMP = 'lastViewedNewsTimestamp';
  previousLastViewedTimeStamp = Date.now().toString(10);
  anyNonInterviewUnseen = false;
  anyInterviewUnseen = false;

  @Input() limit;
  @Input() hideAdder;
  @Input() disableSearch;

  finishedAnimations = 0;
  showSearch = false;

  constructor(private articleService: ArticleService,
              loginService: LoginService,
              private cookieService: CookieService) {
    this.loginService = loginService;
    // if (localStorage.getItem('articles')) {
    //   this.articles = JSON.parse(localStorage.getItem('articles'));
    //   this.displayArticles = this.articles.filter(article => !article.interview);
    // } else {
    this.articleService.getAllArticles()
      .subscribe(actions => {
        this.limit = this.limit || 12;
        this.articles = this.articleService.getArticlesFromActions(actions);
        this.displayArticles = this.articles.filter(article => !article.interview);
        // localStorage.setItem('articles', JSON.stringify(this.articles));
      });
    // }
    this.previousLastViewedTimeStamp = this.cookieService.get(this.LAST_VIEWED_TIMESTAMP) || Date.now().toString(10);
    this.cookieService.set(this.LAST_VIEWED_TIMESTAMP, Date.now().toString(10));
  }

  ngOnInit() {
  }

  animationDone() {
    const activateArticleLoading = !this.hideAdder;
    if (activateArticleLoading) {
      this.finishedAnimations++;
      if (this.finishedAnimations === this.limit) {
        const articles = document.querySelectorAll('.article');

        const artObserver = new IntersectionObserver((entry) => {
          if (entry[0].isIntersecting) {
            this.limit += 12;
          }
        });

        artObserver.observe(articles[this.limit - 3]);
      }
    }
  }

  getParts(article: Article): string[] {
    let parts = new Array<string>();

    if (article.paragraph) {
      parts = article.paragraph.replace('<br><br>', '<br> <br>').split('<br>');
    }

    parts.forEach((part, index) => {
      if (part.indexOf('<in>') > -1) {
        parts[index] = '<b>' + part.replace('<in>', article.intervieweeInitials + ': </b>');
      } else {
        parts[index] = '<b>' + article.author.toUpperCase() + ': </b>' + part;
      }
    });
    return parts;
  }

  toggleContent(value) {
    if (value === 'articles') {
      this.displayArticles = this.articles.filter(article => !article.interview);
    } else {
      this.displayArticles = this.articles.filter(article => article.interview);
    }
  }

  isUnseen(article: Article): boolean {
    if (article.createTimeStamp) {
      const unseen = article.createTimeStamp > this.previousLastViewedTimeStamp;

      if (unseen) {
        if (article.interview) {
          this.anyInterviewUnseen = true;
        } else {
          this.anyNonInterviewUnseen = true;
        }
      }
      return unseen;
    } else {
      return false;
    }
  }

  getAverageRating(article: Article): Array<number> {
    let sum = 0;
    if (article.comments) {
      article.comments.forEach(comment => sum += comment.rating ? comment.rating : 0);
    }
    return this.getRoundedAverageRating(sum / article.comments.length);
  }

  getRoundedAverageRating(averageRating: number): Array<number> {
    return new Array<number>(Math.ceil(averageRating));
  }

  anyCommentsUnseen(comments: Comment[]): boolean {
    return comments && comments.filter(comment => comment.createTimeStamp > this.previousLastViewedTimeStamp).length > 0;
  }

  filterArticles(keyword: string): void {
    console.log('SUCHBEGRIFF', keyword);
    if (keyword) {
      const regEx = new RegExp(keyword, 'i');
      this.displayArticles = this.articles.filter(article => {
        const paragraphs = article.paragraphs;
        let paragraphMatch = false;
        if (paragraphs) {
          article.paragraphs.forEach(paragraph => {
            if (paragraph.headline.match(regEx) || paragraph.text.match(regEx)) {
              return paragraphMatch = true;
            }
          });
        }
        return paragraphMatch
          || article.headline.match(regEx)
          || article.subline.match(regEx)
          || article.paragraph.match(regEx);
      });
    } else {
      this.displayArticles = this.articles.filter(article => !article.interview);
    }
  }

}
