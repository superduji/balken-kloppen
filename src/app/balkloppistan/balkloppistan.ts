import {BalkloppistanPlayer} from './balkloppistanPlayer';

export class Balkloppistan {
  key: string;
  name: string;
  id: number;
  players: BalkloppistanPlayer[];
  finished = false;
  currentRoundIndex = 1;
  numberOfRounds = 1;

  setKey(key: string) {
    this.key = key;
  }

  setName(name: string) {
    this.name = name;
  }

  setId(id: number) {
    this.id = id;
  }

  setPlayers(players: BalkloppistanPlayer[]) {
    this.players = players;
  }
  addPlayer(player: BalkloppistanPlayer) {
    this.players.push(player);
  }
  getPlayerByName(name: string): BalkloppistanPlayer {
    let foundPlayer;
    this.players.forEach(player => {
      if (player.name === name) {
        foundPlayer =  player;
      }
    });
    return foundPlayer;
  }

  closeBalkloppistan() {
    this.finished = true;
    this.currentRoundIndex = null;
  }

  isFinished(): boolean {
    return this.finished;
  }

  incrementRoundIndex(): void {
    this.currentRoundIndex++;
    this.numberOfRounds++;
  }

  getMinHandicapedValueOfRound(roundIndex: number): number {
    let minValue = 16;
    this.players.forEach(player => {
      if (player.rounds[roundIndex - 1]) {
        minValue = Math.min(minValue, player.rounds[roundIndex - 1].handicapedValue);
      }
    });
    return minValue;
  }

}
