import {Injectable} from '@angular/core';
import {AngularFirestore, DocumentChangeAction, DocumentSnapshot} from '@angular/fire/firestore';
import {DbService} from '../utils/db.service';
import {Observable} from 'rxjs';
import {Balkloppistan} from './balkloppistan';
import {BalkloppistanPlayer} from './balkloppistanPlayer';
import {UserService} from '../user/user.service';
import {User} from '../user/user';

@Injectable({
  providedIn: 'root'
})
export class BalkloppistanService {

  private db: AngularFirestore;
  private dbPath = '/balkloppistan';

  constructor(private angularFirestore: AngularFirestore,
              private dbService: DbService,
              private userService: UserService) {
    this.db = angularFirestore;
  }

  // ######################### SET ##################################

  createBalkloppistan(name: string, key: string, userKeys: string[]): void {
    const balkloppistan: Balkloppistan = this.createNewBalkloppistan(name, key);
    balkloppistan.setPlayers(this.createBalkloppistanPlayers(userKeys, key));

    this.dbService.setDocumentWithCustomKey(this.db, this.dbPath, balkloppistan.key, balkloppistan);

  }

  private createNewBalkloppistan(name: string, key: string): Balkloppistan {
    const balkloppistan = new Balkloppistan();
    balkloppistan.setKey(key);
    balkloppistan.setName(name);
    balkloppistan.setId(-1);
    return balkloppistan;
  }

  private createBalkloppistanPlayers(userKeys: string[], balkloppistanKey: string): BalkloppistanPlayer[] {
    const balkloppistanPlayers = new Array<BalkloppistanPlayer>();
    let balkloppistanPlayer;

    userKeys.forEach((userKey, index) => {
      balkloppistanPlayer = new BalkloppistanPlayer(userKey, index);
      balkloppistanPlayers.push(balkloppistanPlayer);

      this.userService.getUserByKeyAsync(userKey)
        .then(snapshot => snapshot.data() as User)
        .then(user => {
          this.addBalkloppistanKeyToParticipatingUser(user, balkloppistanKey);
        });
    });

    return balkloppistanPlayers;
  }

  private addBalkloppistanKeyToParticipatingUser(user: User, balkloppistanKey: string): void {
    if (user.balkloppistanKeys === undefined || user.balkloppistanKeys.length === 0) {
      user.balkloppistanKeys = new Array<string>();
    }
    user.balkloppistanKeys.push(balkloppistanKey);
    // this.userService.updateUser(user);
  }

  updateBalkloppistan(balkloppistan: Balkloppistan) {
    this.dbService.updateDocument(this.db, this.dbPath, balkloppistan.key, balkloppistan);
  }

  // ######################### GET ##################################

  getAllAscendingByName(): Observable<DocumentChangeAction<Balkloppistan>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('name', 'asc'))
      .snapshotChanges() as Observable<DocumentChangeAction<Balkloppistan>[]>;
  }

  getBalkloppistansFromActions(actions: DocumentChangeAction<Balkloppistan>[]): Balkloppistan[] {
    return actions.map(this.getBalkloppistanFromAction);
  }

  private getBalkloppistanFromAction(action: DocumentChangeAction<Balkloppistan>): Balkloppistan {
    const key = action.payload.doc.id;
    const balkloppistan = action.payload.doc.data() as Balkloppistan;
    balkloppistan.key = key;
    return balkloppistan;
  }

  async getBalkloppistanByKeyAsync(key): Promise<DocumentSnapshot<Balkloppistan>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .ref.get() as Promise<DocumentSnapshot<Balkloppistan>>;
  }
}
