import {Component} from '@angular/core';

@Component({
  selector: 'app-balkloppistan',
  templateUrl: './balkloppistan.component.html',
  styleUrls: ['./balkloppistan.component.css']
})
export class BalkloppistanComponent {

  orderByBalkloppistanId(balkloppistans) {
    if (balkloppistans) {
      return balkloppistans.sort((a, b) => a.id < b.id ? 1 : a.id === b.id ? 0 : -1);
    }
    return '';
  }
}
