import {BalkloppistanRound} from './balkloppistanRound';

export class BalkloppistanPlayer {
  name: string;
  userKey: string;
  rounds = new Array<BalkloppistanRound>();
  startPosition: number;
  position: number;
  lastRound: number;

  constructor(name: string, startPosition: number) {
    this.name = name;
    this.userKey = name;
    this.startPosition = startPosition;
    this.rounds.push(new BalkloppistanRound(0));
  }

  setPosition(position: number) {
    this.position = position;
  }

}
