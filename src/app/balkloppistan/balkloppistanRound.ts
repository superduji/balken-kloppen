export class BalkloppistanRound {

  throwValue: number;
  handicapedValue: number;
  finished = false;
  index = 0;
  suddenDeathThrows = new Array<number>();

  constructor(index: number) {
    this.index = index;
  }

  setThrowValue(value: number) {
    this.throwValue = value;
  }
  getThrowValue(): number {
    return this.throwValue;
  }

  setHandicapedValue(value: number) {
    this.handicapedValue = value;
  }
  getHandicapedValue(): number {
    return this.handicapedValue;
  }

  setFinished(finished: boolean) {
    this.finished = finished;
  }
  getFinished(): boolean {
    return this.finished;
  }

  addSuddenDeathThrow(suddenDeathThrow: number) {
    this.suddenDeathThrows.push(suddenDeathThrow);
  }

}
