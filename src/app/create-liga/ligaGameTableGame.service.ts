import {Injectable} from '@angular/core';
import {LigaGameTableGame} from './ligaGameTableGame';

@Injectable({
  providedIn: 'root'
})
export class LigaGameTableGameService {

  createLigaGameTableGames(index: number, userKeys: string[]): LigaGameTableGame[] {
    const ligaGameTableGameKeys = Array<LigaGameTableGame>();

    userKeys.forEach((userKeyOpponent, j) => {
      let round = '';
      if (index < j) {
        round = 'first'; // Hinrunde
      } else if (index > j) {
        round = 'second'; // Rückrunde
      }
      ligaGameTableGameKeys.push(this.createLigaGameTableGame(userKeys[index], userKeyOpponent, round));
    });

    return ligaGameTableGameKeys;
  }


  private createLigaGameTableGame(userKey: string, userKeyOpponent: string, round: string): LigaGameTableGame {

    const ligaGameTableGame = new LigaGameTableGame();
    ligaGameTableGame.setRound(round);

    ligaGameTableGame.setRowPlayerName(userKey);
    ligaGameTableGame.setRowPlayerRef(userKey);

    ligaGameTableGame.setColumnPlayerName(userKeyOpponent);
    ligaGameTableGame.setColumnPlayerRef(userKeyOpponent);

    if (userKey === userKeyOpponent) {
      ligaGameTableGame.setPlayable(false);
    }

    return ligaGameTableGame;
  }

}
