import {LigaTablePlayer} from './ligaTablePlayer';
import {LigaGameTableRow} from './ligaGameTableRow';

export class LigaPlayer {

  name: string;
  team: string;
  userRef: string;
  ligaTablePlayer: LigaTablePlayer;
  ligaGameTableRow: LigaGameTableRow;

  setName(name: string) {
    this.name = name;
  }
  getName(): string {
    return this.name;
  }

  setTeam(team: string) {
    this.team = team;
  }

  setUserRef(userRef: string) {
    this.userRef = userRef;
  }

  setLigaTablePlayer(ligaTablePlayer: LigaTablePlayer) {
    this.ligaTablePlayer = ligaTablePlayer;
  }
  getLigaTablePlayer(): LigaTablePlayer {
    return this.ligaTablePlayer;
  }

  setLigaGameTableRow(ligaGameTableRow: LigaGameTableRow) {
    this.ligaGameTableRow = ligaGameTableRow;
  }
  getLigaGameTableRow(): LigaGameTableRow {
    return this.ligaGameTableRow;
  }

}
