import {LigaPlayer} from './ligaPlayer';

export class Liga {

  key: string;
  name: string;
  id: number;
  ligaPlayers: LigaPlayer[];
  legacy = false;
  finished = false;
  official = true;

  setKey(key: string) {
    this.key = key;
  }

  setName(name: string) {
    this.name = name;
  }

  setId(id: number) {
    this.id = id;
  }

  setLegacy(legacy: boolean) {
    this.legacy = legacy;
  }

  setLigaPlayers(ligaPlayers: LigaPlayer[]) {
    this.ligaPlayers = ligaPlayers;
  }
  addLigaPlayer(ligaPlayer: LigaPlayer) {
    if (!this.ligaPlayers) {
      this.ligaPlayers = new Array<LigaPlayer>();
    }
    this.ligaPlayers.push(ligaPlayer);
  }

  setFinished(finished: boolean) {
    this.finished = finished;
  }

  setOfficial(official: boolean) {
    this.official = official;
  }

}
