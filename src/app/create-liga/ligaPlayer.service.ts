import {Injectable} from '@angular/core';
import {LigaPlayer} from './ligaPlayer';
import {UserService} from '../user/user.service';
import {User} from '../user/user';
import {LigaGameTableRowService} from './ligaGameTableRow.service';
import {LigaTablePlayer} from './ligaTablePlayer';

@Injectable({
  providedIn: 'root'
})
export class LigaPlayerService {

  constructor(private userService: UserService,
              private ligaGameTableRowService: LigaGameTableRowService) {
  }

  createLigaPlayer(index: number,
                   userKeys: string[],
                   ligaKey: string): Promise<LigaPlayer> {

    const userKey = userKeys[index];
    const ligaPlayer = new LigaPlayer();
    ligaPlayer.setName(userKey);
    ligaPlayer.setUserRef(userKey);
    ligaPlayer.setLigaTablePlayer(new LigaTablePlayer(ligaPlayer.name));
    ligaPlayer.setLigaGameTableRow(this.ligaGameTableRowService.createLigaGameTableRow(index, userKeys));

    return this.userService.getUserByKeyAsync(userKey)
      .then(snapshot => snapshot.data() as User)
      .then(user => {
        this.addLigaKeyToParticipatingUser(user, ligaKey);
        ligaPlayer.setTeam(user.team);
        console.log('ligaplayer', ligaPlayer);
        return ligaPlayer;
      });
  }

  private addLigaKeyToParticipatingUser(user: User, ligaKey: string): void {
    if (user.ligaRefs === undefined || user.ligaRefs.length === 0) {
      user.ligaRefs = new Array<string>();
    }
    user.ligaRefs.push(ligaKey);
    this.userService.updateUser(user);
  }

}
