import {LigaGameTableGame} from './ligaGameTableGame';

export class LigaGameTableRow {

  ligaGameTableGames: LigaGameTableGame[];

  setLigaGameTableGames(ligaGameTableGames: LigaGameTableGame[]) {
    this.ligaGameTableGames = ligaGameTableGames;
  }
  getLigaGameTableGames(): LigaGameTableGame[] {
    return this.ligaGameTableGames;
  }

}
