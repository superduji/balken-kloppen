import {Injectable} from '@angular/core';
import {AngularFirestore, DocumentChangeAction, DocumentSnapshot} from '@angular/fire/firestore';
import {Liga} from './liga';
import {LigaPlayerService} from './ligaPlayer.service';
import {DbService} from '../utils/db.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LigaService {

  private dbPath = '/ligen';
  private db: AngularFirestore;
  private ligen: Liga[];

  constructor(private angularFirestore: AngularFirestore,
              private ligaPlayerService: LigaPlayerService,
              private dbService: DbService) {
    this.db = angularFirestore;
    this.getAllAscendingById()
      .subscribe(actions => this.ligen = this.getLigenFromActions(actions));
  }

  // ######################### SET ##################################

  createLiga(name: string, userKeys: string[], official: boolean): void {
    const liga = new Liga();
    liga.setKey(name.replace('/', '-'));
    liga.setName(name);
    liga.setId(this.getNextIdFromExistingLigen(official));
    liga.setOfficial(official);

    userKeys.forEach((userKey, index) => {
      this.ligaPlayerService.createLigaPlayer(index, userKeys, liga.key)
        .then(ligaPlayer => {
          liga.addLigaPlayer(ligaPlayer);
          this.dbService.setDocumentWithCustomKey(this.db, this.dbPath, liga.key, liga);
        });
    });
  }

  getNextIdFromExistingLigen(official: boolean): number {
    if (this.ligen) {
      const maxValueFromExistingLigen = Math.max.apply(
        Math, this.ligen.map((liga) => {
          return liga.id;
        })
      );
      if (official) {
        return Math.floor(maxValueFromExistingLigen) + 1;
      } else {
        if (maxValueFromExistingLigen % 1 === 0) {
          return Math.floor(maxValueFromExistingLigen) + 0.01;
        } else {
          return maxValueFromExistingLigen + 0.01;
        }
      }
    } else {
      return -1;
    }
  }

  updateLiga(liga: Liga): void {
    this.dbService.updateDocument(this.db, this.dbPath, liga.key, liga);
  }

  // ######################### GET ##################################

  getAllDescendingByName(): Observable<DocumentChangeAction<Liga>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('name', 'desc'))
      .snapshotChanges() as Observable<DocumentChangeAction<Liga>[]>;
  }

  getAllAscendingById(): Observable<DocumentChangeAction<Liga>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('id', 'asc'))
      .snapshotChanges() as Observable<DocumentChangeAction<Liga>[]>;
  }

  getAllFinished(): Observable<DocumentChangeAction<Liga>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.where('finished', '==', true))
      .snapshotChanges() as Observable<DocumentChangeAction<Liga>[]>;
  }

  getLigaFromLigaObservable(observable): Liga {
    return observable.payload.data() as Liga;
  }

  async getLigaSnapshotByKeyAsync(key): Promise<DocumentSnapshot<Liga>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .ref.get() as Promise<DocumentSnapshot<Liga>>;
  }

  getLigenFromActions(actions: DocumentChangeAction<Liga>[]): Liga[] {
    return actions.map(this.getLigaFromAction);
  }

  private getLigaFromAction(action: DocumentChangeAction<Liga>): Liga {
    const key = action.payload.doc.id;
    const liga = action.payload.doc.data() as Liga;
    liga.key = key;
    return liga;
  }

}
