import {Component} from '@angular/core';

import {User} from '../user/user';
import {UserService} from '../user/user.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Liga} from './liga';
import {LigaService} from './liga.service';
import {GameService} from '../games/game.service';
import {Router} from '@angular/router';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-create-liga',
  templateUrl: './create-liga.component.html',
  styleUrls: ['./create-liga.component.css']
})
export class CreateLigaComponent {

  type: Type = Type.LEAGUE;

  loginService: LoginService;

  liga: Liga = new Liga();
  users: User[];

  form: FormGroup;
  name: FormControl;
  player: FormArray;

  submitted = false;


  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private gameService: GameService,
              private ligaService: LigaService,
              private router: Router,
              loginService: LoginService) {
    this.loginService = loginService;

    // retrieve users from userservice
    this.userService.getAllAscendingByName()
      .subscribe(actions => {
        this.users = this.userService.getUsersFromActions(actions).filter(user => !user.inactive);
      });

    // build formControl for name-field
    this.name = new FormControl(this.liga.name, [
      Validators.required,
      Validators.minLength(2)
    ]),

      // build complete formGroup (temp. with empty players-array)
      this.form = this.formBuilder.group({
        name: this.name,
        players: new FormArray([])
      });

    // after waiting for async-answer from userService, fill players-formArray with checkboxes
    setTimeout(() => {
      this.addCheckboxes();
    }, 500);

  }

  private addCheckboxes() {
    this.users.forEach(() => {
      const control = new FormControl();
      (this.form.controls.players as FormArray).push(control);
    });
  }


  onSubmit() {
    this.submitted = true;
    const userKeys = this.form.value.players
      .map((v, i) => v ? this.users[i].key : null)
      .filter(v => v !== null);

    this.save(this.form.value.name, this.shuffle(userKeys));
    setTimeout(() => {
      this.router.navigate(['/liga']);
    }, 500);
  }

  private save(name, userKeys) {
    this.ligaService.createLiga(name, userKeys, true);
  }

  private shuffle(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

}
