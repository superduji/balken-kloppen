import {Injectable} from '@angular/core';
import {LigaGameTableRow} from './ligaGameTableRow';
import {LigaGameTableGameService} from './ligaGameTableGame.service';

@Injectable({
  providedIn: 'root'
})
export class LigaGameTableRowService {

  constructor(private ligaGameTableGameService: LigaGameTableGameService) {}

  createLigaGameTableRow(index: number, userKeys: string[]): LigaGameTableRow {
    const ligaGameTableRow = new LigaGameTableRow();
    ligaGameTableRow.setLigaGameTableGames(this.ligaGameTableGameService.createLigaGameTableGames(index, userKeys));
    return ligaGameTableRow;
  }

}
