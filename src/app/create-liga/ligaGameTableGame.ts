export class LigaGameTableGame {

  round = '';
  rowPlayerName: string;
  rowPlayerRef: string;
  columnPlayerName: string;
  columnPlayerRef: string;
  gameRef = 'tbp.';
  playable = true;

  setRound(round: string) {
    this.round = round;
  }

  setRowPlayerName(rowPlayerName: string) {
    this.rowPlayerName = rowPlayerName;
  }

  setRowPlayerRef(rowPlayerRef: string) {
    this.rowPlayerRef = rowPlayerRef;
  }

  setColumnPlayerName(columnPlayerName: string) {
    this.columnPlayerName = columnPlayerName;
  }

  setColumnPlayerRef(columnPlayerRef: string) {
    this.columnPlayerRef = columnPlayerRef;
  }

  setGameRef(gameRef: string) {
    this.gameRef = gameRef;
  }

  setPlayable(playable: boolean) {
    this.playable = playable;
  }

}
