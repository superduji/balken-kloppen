export class LigaTablePlayer {

  name = '';

  position = 0;
  positionFirstRound = 0;
  positionSecondRound = 0;

  playedGames = 0;
  playedGamesFirstRound = 0;
  playedGamesSecondRound = 0;

  points = 0;
  pointsFirstRound = 0;
  pointsSecondRound = 0;

  throwsSum = 0.0;
  throwsSumFirstRound = 0.0;
  throwsSumSecondRound = 0.0;

  numberOfThrows = 0.0;
  numberOfThrowsFirstRound = 0.0;
  numberOfThrowsSecondRound = 0.0;

  quote = 0;
  quoteFirstRound = 0;
  quoteSecondRound = 0;

  wins = 0;
  winsFirstRound = 0;
  winsSecondRound = 0;

  draws = 0;
  drawsFirstRound = 0;
  drawsSecondRound = 0;

  defeats = 0;
  defeatsFirstRound = 0;
  defeatsSecondRound = 0;

  gameRefs = new Array<string>();

  constructor(name: string) {
    this.name = name;
  }
}
