import {Component} from '@angular/core';
import {Link} from '../models/link.model';

@Component({
  selector: 'app-further-links',
  templateUrl: './further-links.component.html',
  styleUrls: ['./further-links.component.css']
})
export class FurtherLinksComponent {

  links = new Array<Link>();

  constructor() {
    this.links.push(new Link('https://smile.amazon.de/Magnetband-farbig-Sortiert-Meterstücke-breit/dp/B000W6TJ7M/ref=pd_sbs_196_2/260-8433980-3725127', 'Neue Balken?', false));
    this.links.push(new Link('https://console.firebase.google.com/project/balken-kloppen/database/firestore/data', 'DB', false));
  }

}
