import {Component, Input, OnInit} from '@angular/core';
import {Liga} from '../create-liga/liga';
import {User} from '../user/user';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Game} from '../models/boundary/game.model';
import {UserService} from '../user/user.service';
import {GameService} from '../games/game.service';
import {LigaService} from '../create-liga/liga.service';
import {Router} from '@angular/router';
import {LoginService} from '../login/login.service';
import {CupService} from '../create-pokal/cup.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  loginService: LoginService;

  @Input() type: Type;

  liga: Liga = new Liga();
  users: User[];

  form: FormGroup;
  name: FormControl;
  player: FormArray;

  submitted = false;
  officialEvent = true;

  games: Game[];

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private gameService: GameService,
              private ligaService: LigaService,
              private cupService: CupService,
              private router: Router,
              loginService: LoginService) {
    this.loginService = loginService;

    this.gameService.getAllDescendingById()
      .subscribe(actions => this.games = this.gameService.getGamesFromActions(actions));

    this.userService.getAllAscendingByName()
      .subscribe(actions => {
        this.users = this.userService.getUsersFromActions(actions).filter(user => !user.inactive);
      });

    // build formControl for name-field
    this.name = new FormControl(this.liga.name, [
      Validators.required,
      Validators.minLength(2)
    ]);

    // build complete formGroup (temp. with empty players-array)
    this.form = this.formBuilder.group({
      name: this.name,
      players: new FormArray([])
    });

    // after waiting for async-answer from userService, fill players-formArray with checkboxes
    setTimeout(() => {
      this.addCheckboxes();
    }, 500);

  }

  ngOnInit(): void {
  }

  private addCheckboxes() {
    this.users.forEach(() => {
      const control = new FormControl();
      (this.form.controls.players as FormArray).push(control);
    });
  }


  onSubmit() {
    this.submitted = true;
    const userKeys = this.form.value.players
      .map((v, i) => v ? this.users[i].key : null)
      .filter(v => v !== null);

    this.save(this.form.value.name, userKeys);
    setTimeout(() => {
      this.router.navigate([`/${this.type.toLowerCase()}`]);
    }, 500);
  }

  private save(name, userKeys) {
    if (this.type === Type.LEAGUE) {
      this.ligaService.createLiga(name, this.shuffle(userKeys), this.officialEvent);
      // liga.addLigaRefs() wird im LigaService gemacht
    } else if (this.type === Type.CUP) {
      // cup service
      const filledUpUserKeys = this.cupService.addFreilose(userKeys);
      this.cupService.createCup(name, this.shuffle(filledUpUserKeys), this.officialEvent);

      this.users.forEach(user => {
        if (userKeys.includes(user.key)) {
          user.cupRefs.push(name);
          this.userService.updateUser(user);
        }
      });
    }
  }

  private shuffle(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

}
