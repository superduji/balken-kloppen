export class Player {

  name: string;
  team: string;
  rest: number;
  numberOfThrows: number;
  isWinner: boolean;
  average: number;
  throws: Array<string>;
  suddenDeathThrows: Array<string>;

  constructor(name: string,
              team: string,
              rest: number,
              numberOfThrows: number,
              average: number,
              throws: Array<string>,
              suddenDeathThrows: Array<string>,
              isWinner: boolean) {
    this.name = name;
    this.team = team;
    this.rest = rest;
    this.numberOfThrows = numberOfThrows;
    this.average = average;
    this.throws = throws;
    this.suddenDeathThrows = suddenDeathThrows;
    this.isWinner = isWinner;
  }

}
