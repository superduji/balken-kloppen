export class Link {

  href: string;
  text: string;
  intern: boolean;

  constructor(href: string, text: string, intern: boolean) {
    this.href = href;
    this.text = text;
    this.intern = intern;
  }
}
