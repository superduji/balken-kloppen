import {Injectable} from '@angular/core';
import {Action, AngularFirestore, DocumentChangeAction, DocumentSnapshot} from '@angular/fire/firestore';
import {LigaPlayerService} from '../create-liga/ligaPlayer.service';
import {DbService} from '../utils/db.service';
import {Pokal} from './pokal';
import {PokalRound} from './pokal-round';
import {PokalGame} from './pokal-game';
import {Observable} from 'rxjs';
import {Game} from '../games/game';

@Injectable({
  providedIn: 'root'
})
export class CupService {

  FREILOS_KEY = 'Freilos';

  private dbPath = '/pokal';
  private db: AngularFirestore;
  private cups: Pokal[];

  constructor(private angularFirestore: AngularFirestore,
              private ligaPlayerService: LigaPlayerService,
              private dbService: DbService) {
    this.db = angularFirestore;
    this.getAllDescendingById()
      .subscribe(actions => this.cups = this.getCupsFromActions(actions));
  }

  getAllDescendingById(): Observable<DocumentChangeAction<Pokal>[]> {
    return this.db
      .collection(this.dbPath, ref => ref.orderBy('id', 'desc'))
      .snapshotChanges() as Observable<DocumentChangeAction<Pokal>[]>;
  }

  getCupsFromActions(actions: DocumentChangeAction<Pokal>[]): Pokal[] {
    return actions.map(this.getCupsFromAction);
  }

  private getCupsFromAction(action: DocumentChangeAction<Pokal>): Pokal {
    const key = action.payload.doc.id;
    const cup = action.payload.doc.data() as Pokal;
    cup.key = key;
    return cup;
  }

  private getCupByKey(key): Observable<Action<DocumentSnapshot<Pokal>>> {
    return this.db
      .collection(this.dbPath)
      .doc(key)
      .snapshotChanges() as Observable<Action<DocumentSnapshot<Pokal>>>;
  }

  private updateCup(pokal: Pokal): void {
    this.dbService.updateDocument(this.db, this.dbPath, pokal.name, pokal);
  }

  createCup(name: string, userKeys: string[], officialEvent: boolean): void {
    const pokal = this.createNewPokal(name, userKeys, officialEvent);
    this.dbService.setDocumentWithCustomKey(this.db, this.dbPath, pokal.key, pokal);
  }

  private createNewPokal(name: string, userKeys: string[], officialEvent: boolean): Pokal {
    const pokal = new Pokal();
    const rounds = this.createPokalRounds(userKeys);
    const evaluatedRounds = this.evaluateFreilosGames(rounds);
    pokal.setKey(name.replace(/\//g, '-'));
    pokal.setName(name);
    pokal.setId(this.cups.length + 1);
    pokal.setRounds(evaluatedRounds);
    pokal.setOfficialEvent(officialEvent);
    return pokal;
  }

  private createPokalRounds(userKeys: string[]): PokalRound[] {
    const rounds: PokalRound[] = [];
    const numberOfRounds = Math.log2(userKeys.length);
    let idCount = 0;
    for (let i = 0; i < numberOfRounds; i++) {
      let numberOfGamesInRound = userKeys.length / Math.pow(2, i + 1);
      // Spiel um Platz 3
      if (i === numberOfRounds - 1) {
        numberOfGamesInRound++;
      }
      const gamesInRound: PokalGame[] = [];
      for (let j = 0; j < numberOfGamesInRound; j++) {
        const players: string[] = [];
        if (i === 0) {
          players.push(userKeys[j * 2]);
          players.push(userKeys[j * 2 + 1]);
        } else {
          players.push('offen');
          players.push('offen');
        }
        gamesInRound.push(new PokalGame(idCount, players, '', ''));
        idCount++;
      }
      rounds.push(new PokalRound(`round${i + 1}`, `Runde${i + 1}`, gamesInRound));
    }
    return rounds;
  }

  private evaluateFreilosGames(rounds: PokalRound[]) {
    // ZUM TESTEN:
    // rounds[0].pokalGames[0].playerRefs[0] = 'Freilos';
    // rounds[0].pokalGames[0].playerRefs[1] = 'Freilos';
    // rounds[0].pokalGames[1].playerRefs[0] = 'Freilos';
    // rounds[0].pokalGames[1].playerRefs[1] = 'Freilos';

    // HIER FOLGT DER BESTE CODE DER WELT
    for (let i = 0; i < rounds.length; i++) {
      for (let j = 0; j < rounds[i].pokalGames.length; j++) {
        for (let k = 0; k < rounds[i].pokalGames[j].playerRefs.length; k++) {
          // Alles nur für Nicht-Finale
          if (i + 1 < rounds.length) {
            if (k + 1 < 2 && rounds[i].pokalGames[j].playerRefs[k] === this.FREILOS_KEY
              && rounds[i].pokalGames[j].playerRefs[k + 1] === this.FREILOS_KEY) {
              // Wenn beide Spieler Freilos sind, zieht Freilos weiter
              rounds[i + 1].pokalGames[Math.floor(j / 2)].playerRefs[j % 2] = this.FREILOS_KEY;
            } else if (k + 1 < 2 && rounds[i].pokalGames[j].playerRefs[k] === this.FREILOS_KEY
              && rounds[i].pokalGames[j].playerRefs[k + 1] !== this.FREILOS_KEY) {
              // Wenn der erste Spieler Freilos ist, zieht der zweite weiter
              rounds[i + 1].pokalGames[Math.floor(j / 2)].playerRefs[j % 2] = rounds[i].pokalGames[j].playerRefs[k + 1];
            } else if (k + 1 < 2 && rounds[i].pokalGames[j].playerRefs[k] !== this.FREILOS_KEY
              && rounds[i].pokalGames[j].playerRefs[k + 1] === this.FREILOS_KEY) {
              // Wenn der zweite Spieler Freilos ist, zieht der erste weiter
              rounds[i + 1].pokalGames[Math.floor(j / 2)].playerRefs[j % 2] = rounds[i].pokalGames[j].playerRefs[k];
            }
          }
        }
      }
    }
    return rounds;
  }

  addFreilose(userKeys: string[]): string[] {
    const filledUpUserKeys = userKeys;
    const filledUpUserKeysLength = Math.pow(2, Math.ceil(Math.log10(userKeys.length) / Math.log10(2)));
    const freilose = filledUpUserKeysLength - userKeys.length;
    for (let i = 0; i < freilose; i++) {
      filledUpUserKeys.push('Freilos');
    }
    return filledUpUserKeys;
  }

  addGameToCup(game: Game, pokalInfos) {
    let cups;
    this.getAllDescendingById()
      .subscribe(actions => {
        cups = this.getCupsFromActions(actions);
        const currentCup: Pokal = cups.find(cup => cup.key === game.event);

        currentCup.rounds[pokalInfos.round].pokalGames.forEach(pokalGame => {
          if (pokalGame.id === pokalInfos.gameId) {
            pokalGame.gameRef = game.key;
            pokalGame.winner = game.winner;
          }
        });
        const updatedCup = this.moveWinnerToNextRound(currentCup, game, pokalInfos);
        this.updateCup(updatedCup);
      });
  }

  moveWinnerToNextRound(currentCup: Pokal, game: Game, pokalInfos) {
    const gameIndex = Math.floor(pokalInfos.gameIndex / 2);
    const playerInGamePosition = pokalInfos.gameIndex % 2;
    currentCup.rounds[pokalInfos.round + 1].pokalGames[gameIndex].playerRefs[playerInGamePosition] = game.winner;

    if (currentCup.rounds[pokalInfos.round + 1].pokalGames[gameIndex].playerRefs[0] === this.FREILOS_KEY
      || currentCup.rounds[pokalInfos.round + 1].pokalGames[gameIndex].playerRefs[1] === this.FREILOS_KEY) {
      pokalInfos.gameIndex = gameIndex;
      pokalInfos.round++;
      this.moveWinnerToNextRound(currentCup, game, pokalInfos);
      // const nextRoundGameIndex = Math.floor(gameIndex / 2);
      // const nextRoundPlayerInGamePosition = gameIndex % 2;
      // currentCup.rounds[pokalInfos.round + 2].pokalGames[nextRoundGameIndex].playerRefs[nextRoundPlayerInGamePosition] = game.winner;
    }

    return currentCup;
  }
}
