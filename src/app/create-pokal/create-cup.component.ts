import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-cup',
  templateUrl: './create-cup.component.html',
  styleUrls: ['./create-cup.component.css']
})
export class CreateCupComponent implements OnInit {

  type: Type = Type.CUP;

  constructor() { }

  ngOnInit() {
  }

}
