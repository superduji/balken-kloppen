export class PokalGame {
  id: number;
  playerRefs: string[];
  gameRef: string;
  winner: string;

  constructor(id: number, playerRefs: string[], gameRef: string, winner: string) {
    this.id = id;
    this.playerRefs = playerRefs;
    this.gameRef = gameRef;
    this.winner = winner;
  }
}
