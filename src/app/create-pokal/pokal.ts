import {PokalRound} from './pokal-round';

export class Pokal {

  key: string;
  name: string;
  id: number;
  legacy = false;
  ligaPlayerKeys: string[];
  finished = false;
  rounds: PokalRound[];
  officialEvent: boolean;

  setKey(key: string) {
    this.key = key;
  }

  setName(name: string) {
    this.name = name;
  }

  setId(id: number) {
    this.id = id;
  }

  setLegacy(legacy: boolean) {
    this.legacy = legacy;
  }

  setLigaPlayerKeys(ligaPlayerKeys: string[]) {
    this.ligaPlayerKeys = ligaPlayerKeys;
  }

  setFinished(finished: boolean) {
    this.finished = finished;
  }

  setRounds(rounds: PokalRound[]) {
    this.rounds = rounds;
  }

  setOfficialEvent(officialEvent: boolean) {
    this.officialEvent = officialEvent;
  }

}
