import {PokalGame} from './pokal-game';

export class PokalRound {
  key: string;
  name: string;
  pokalGames: PokalGame[];
  endDate: string;

  constructor(key: string, name: string, pokalGames: PokalGame[]) {
    this.key = key;
    this.name = name;
    this.pokalGames = pokalGames;
  }
}
