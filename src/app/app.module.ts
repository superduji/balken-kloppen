import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireModule} from '@angular/fire';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestoreModule, FirestoreSettingsToken} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {routing} from './app.routes';

import {ReplaceAllPipe} from './pipes/replace-all.pipe';

import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {KloppenComponent} from './kloppen/kloppen.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {PageNotFoundComponent} from './error/page-not-found.component';
import {LigaComponent} from './liga/liga.component';
import {LoginComponent} from './login/login.component';
import {CreateUserComponent} from './user/create-user/create-user.component';

import {UserComponent} from './user/user.component';
import {UserListComponent} from './user/user-list/user-list.component';

import {CookieService} from 'ngx-cookie-service';
import {LoginErrorComponent} from './login-error/login-error.component';

import {CreateGameComponent} from './games/create-game/create-game.component';

import {SoundBoardComponent} from './soundboard/sound-board/sound-board.component';

import {CreateLigaComponent} from './create-liga/create-liga.component';

import {BalkloppistanStatisticsComponent} from './statistics/balkloppistan/balkloppistan.component';
import {CupsStatisticsComponent} from './statistics/cups/cups.component';
import {DuelsStatisticsComponent} from './statistics/duels/duels.component';
import {GamesStatisticsComponent} from './statistics/games/games.component';
import {PlayersStatisticsComponent} from './statistics/players/players.component';
import {WorldRankingStatisticsComponent} from './statistics/worldranking/worldranking.component';
import {EwigeLigaTabelleComponent} from './statistics/ewigeligatabelle/ewigeligatabelle.component';
import {WurfquotenStatisticsComponent} from './statistics/wurfquoten/wurfquoten.component';
import {LigenStatisticsComponent} from './statistics/ligen/ligen.component';
import {NewsTickerComponent} from './news-ticker/news-ticker.component';
import {CreateArticleComponent} from './create-article/create-article.component';
import {NewsComponent} from './news/news.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FurtherLinksComponent} from './further-links/further-links.component';

import {HighchartsChartModule} from 'highcharts-angular';
import {HttpClientModule} from '@angular/common/http';
import {BalkloppistanComponent} from './balkloppistan/balkloppistan.component';
import {CreateBalkloppistanComponent} from './create-balkloppistan/create-balkloppistan.component';
import {BalkloppistanGameComponent} from './balkloppistan-game/balkloppistan-game.component';
import {CupComponent} from './cup/cup.component';
import {CreateCupComponent} from './create-pokal/create-cup.component';
import {CreateEventComponent} from './create-event/create-event.component';
import {NavigationComponent} from './navigation/navigation.component';
import {RulesComponent} from './rules/rules.component';
import {LazyLoadImageModule, scrollPreset} from 'ng-lazyload-image';
import {ArticleComponent} from './article/article.component';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {AdddComponent} from './addd/addd.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HighchartsChartModule,
    HttpClientModule,
    routing,
    LazyLoadImageModule.forRoot({
      preset: scrollPreset
    })
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    KloppenComponent,
    StatisticsComponent,
    PageNotFoundComponent,
    BalkloppistanStatisticsComponent,
    CupsStatisticsComponent,
    DuelsStatisticsComponent,
    GamesStatisticsComponent,
    LigenStatisticsComponent,
    LigaComponent,
    BalkloppistanComponent,
    PlayersStatisticsComponent,
    WorldRankingStatisticsComponent,
    EwigeLigaTabelleComponent,
    PageNotFoundComponent,
    UserComponent,
    UserListComponent,
    CreateUserComponent,
    WurfquotenStatisticsComponent,
    SoundBoardComponent,
    CreateGameComponent,
    CreateLigaComponent,
    CreateBalkloppistanComponent,
    BalkloppistanGameComponent,
    ReplaceAllPipe,
    LoginComponent,
    LoginErrorComponent,
    NewsTickerComponent,
    CreateArticleComponent,
    NewsComponent,
    FurtherLinksComponent,
    CupComponent,
    CreateCupComponent,
    CreateEventComponent,
    NavigationComponent,
    RulesComponent,
    ArticleComponent,
    ActionBarComponent,
    AdddComponent
  ],
  providers: [
    {
      provide: FirestoreSettingsToken,
      useValue: {}
    },
    CookieService,
    AngularFireStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
