import {Component, OnInit} from '@angular/core';
import {UserService} from '../user/user.service';
import {CupService} from '../create-pokal/cup.service';
import {User} from '../user/user';
import {Pokal} from '../create-pokal/pokal';
import {Game} from '../games/game';
import {KloppenService} from '../kloppen/kloppen.service';
import {LoginService} from '../login/login.service';
import {PokalGame} from '../create-pokal/pokal-game';
import {Router} from '@angular/router';
import {GameService} from '../games/game.service';
import {of} from "rxjs";

@Component({
  selector: 'app-cup',
  templateUrl: './cup.component.html',
  styleUrls: ['./cup.component.scss']
})
export class CupComponent implements OnInit {

  loginService: LoginService;

  FREILOS_KEY = 'Freilos';
  OFFEN_KEY = 'offen';

  ROUND_LABELS = {
    THREE: {
      0: 'Viertelfinale',
      1: 'Halbfinale',
      2: 'Finale'
    },
    FOUR: {
      0: 'Achtelfinale',
      1: 'Viertelfinale',
      2: 'Halbfinale',
      3: 'Finale'
    }
  };
  EVENT_NAMES = {
    official: 'IBKA® Karwenzel-Cup',
    unofficial: 'Cups'
  };

  chosenCup: Pokal;
  officialEvents = true;

  users: User[];
  cups: Pokal[];
  cupsToShow: Pokal[];
  games: Game[];

  constructor(private userService: UserService,
              private cupService: CupService,
              private kloppenService: KloppenService,
              loginService: LoginService,
              private router: Router,
              private gameService: GameService) {
    this.loginService = loginService;
    this.getUsers();
    this.getCups();
    this.getGames();
  }

  ngOnInit() {
  }

  getEventName(official: boolean) {
    if (official) {
      return this.EVENT_NAMES.official;
    }
    return this.EVENT_NAMES.unofficial;
  }

  private getUsers(): void {
    // const localStorageUsers = localStorage.getItem('users');
    // if (localStorageUsers) {
    //   this.users = JSON.parse(localStorageUsers) as User[];
    //   console.log('HIER WURDEN LOKALE USER REINGEBALLERT', this.users);
    // } else {
      this.userService.getAllAscendingByName()
        .subscribe(actions => {
          this.users = this.userService.getUsersFromActions(actions);
          // localStorage.setItem('users', JSON.stringify(this.users));
        });
    // }
  }

  private getCups(): void {
    // const localStorageCups = localStorage.getItem('cups');
    // if (localStorageCups) {
    //   this.cups = JSON.parse(localStorageCups) as Pokal[];
    //   this.chosenCup = this.cups[0];
    //   console.log('HIER WURDEN LOKALE CUPS REINGEBALLERT', this.cups);
    // } else {
      this.cupService.getAllDescendingById()
        .subscribe(actions => {
          this.cups = this.cupService.getCupsFromActions(actions);
          this.cupsToShow = this.cups.filter(cup => cup.officialEvent === this.officialEvents);
          // localStorage.setItem('cups', JSON.stringify(this.cups));
          this.showFirstCup();
        });
    // }
  }

  private showFirstCup(): void {
    this.chosenCup = this.cupsToShow[0];
  }

  private getGames(): void {
    // const localStorageGames = localStorage.getItem('games');
    // if (localStorageGames) {
    //   this.games = JSON.parse(localStorageGames) as Game[];
    //   console.log('HIER WURDEN LOKALE GAMES REINGEBALLERT', this.games);
    // } else {
      this.gameService.getAllDescendingById()
        .subscribe(actions => {
          this.games = this.gameService.getGamesFromActions(actions);
          // localStorage.setItem('games', JSON.stringify(this.games));
        });
    // }
  }

  getUserByPlayerRef(playerRef: string): User {
    if (this.FREILOS_KEY === playerRef) {
      return {name: this.FREILOS_KEY, team: this.FREILOS_KEY} as User;
    } else if (this.OFFEN_KEY === playerRef) {
      return {name: this.OFFEN_KEY, team: this.OFFEN_KEY} as User;
    }
    return this.users.find(user => playerRef === user.key);
  }

  getPlayerStatsFromGame(playerRef: string, pokalGame: PokalGame) {
    const refGame = this.games.find(game => game.key === pokalGame.gameRef);
    if (refGame) {
      return refGame.players.find(player => player['name'] === playerRef);
    }
    return null;
  }

  changeCup(cupKey) {
    this.chosenCup = this.cups.find(cup => cupKey === cup.key);
  }

  changeEvents(officialEvents) {
    this.officialEvents = JSON.parse(officialEvents);
    this.cupsToShow = this.cups.filter(cup => cup.officialEvent === this.officialEvents);
    this.showFirstCup();
  }

  isGameKloppable(game: PokalGame) {
    // Game is not kloppable if at least one of the players is Freilos or offen
    return game.playerRefs[0] !== this.FREILOS_KEY
      && game.playerRefs[1] !== this.FREILOS_KEY
      && game.playerRefs[0] !== this.OFFEN_KEY
      && game.playerRefs[1] !== this.OFFEN_KEY;
  }

  pokalSpielKloppen(roundIndex: number, gameIndex: number, game: PokalGame, cup: Pokal): void {
    const kloppable = this.isGameKloppable(game);
    if (this.loginService.isUserLoggedIn() && kloppable && !game.gameRef) {
      const players = [];

      // Find users by name
      game.playerRefs.forEach(player => {
        players.push(this.users.find(user => user.name === player));
      });

      // Add relevant data to KloppenService
      this.kloppenService.setType(Type.CUP);
      this.kloppenService.setEvent(cup.key);
      this.kloppenService.setDate(new Date().toISOString().substr(0, 10));
      this.kloppenService.setPlayers(players);
      this.kloppenService.setPokalInfos({round: roundIndex, gameIndex, gameId: game.id});

      this.router.navigate(['/addGame']);
    } else if (!kloppable) {
      return;
    } else {
      // window.open(`https://console.firebase.google.com/project/balken-kloppen/database/firestore/data~2Fgame~2F${game.gameRef}`);
      this.router.navigate(['/statistiken/spiele']);
    }
  }

  isWinner(player: string, game: PokalGame) {
    return player === game.winner;
  }

  isThirdPlaceMatch(roundIndex: number, gameIndex: number) {
    return roundIndex === this.chosenCup.rounds.length - 1 && gameIndex === 1;
  }

  getRoundLabel(roundIndex) {
    if (this.chosenCup.rounds.length === 3) {
      return this.ROUND_LABELS.THREE[roundIndex];
    }
    return this.ROUND_LABELS.FOUR[roundIndex];
  }

}
