import {Injectable} from '@angular/core';
import Vibrant from 'node-vibrant/lib/vibrant';
import BrowserImage from 'node-vibrant/lib/image/browser';

@Injectable({
  providedIn: 'root'
})
export class ColorHelper {

  deriveColorFrom(originalColor: string, derivationIndex: number): string {
    // hex to hsl
    const rgb = this.hexToRgb(originalColor);
    const hsl = this.rgbToHsl(rgb.r, rgb.g, rgb.b);

    // derive saturation value (make it more grey)
    const newHslSValue = hsl.s - (hsl.s / 3) * derivationIndex;
    const derivedHsl = hsl;
    derivedHsl.s = newHslSValue;

    // hsl to hex
    const derivedRgb = this.hslToRgb(derivedHsl.h, derivedHsl.s, derivedHsl.l);
    const newColor = this.rgbToHex(derivedRgb.r, derivedRgb.g, derivedRgb.b);

    // console.log('colors', originalColor, rgb, hsl, derivedHsl, derivedRgb, newColor);
    return newColor;
  }

  hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  rgbToHsl(r, g, b) {
    r /= 255, g /= 255, b /= 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);
    let hValue;
    let sValue;
    const lValue = (max + min) / 2;

    if (max === min) {
      hValue = sValue = 0; // achromatic
    } else {
      const d = max - min;
      sValue = lValue > 0.5 ? d / (2 - max - min) : d / (max + min);

      switch (max) {
        case r:
          hValue = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          hValue = (b - r) / d + 2;
          break;
        case b:
          hValue = (r - g) / d + 4;
          break;
      }

      hValue /= 6;
    }

    return {
      h: hValue,
      s: sValue,
      l: lValue
    };
  }

  hslToRgb(h, s, l) {
    let r;
    let g;
    let b;

    if (s === 0) {
      r = g = b = l; // achromatic
    } else {

      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;

      r = this.hue2rgb(p, q, h + 1 / 3);
      g = this.hue2rgb(p, q, h);
      b = this.hue2rgb(p, q, h - 1 / 3);
    }

    return {
      r: Math.round(r * 255),
      g: Math.round(g * 255),
      b: Math.round(b * 255)
    };
  }

  hue2rgb(p, q, t) {
    if (t < 0) {
      t += 1;
    }
    if (t > 1) {
      t -= 1;
    }
    if (t < 1 / 6) {
      return p + (q - p) * 6 * t;
    }
    if (t < 1 / 2) {
      return q;
    }
    if (t < 2 / 3) {
      return p + (q - p) * (2 / 3 - t) * 6;
    }
    return p;
  }

  componentToHex(c) {
    const hex = c.toString(16);
    return hex.length === 1 ? '0' + hex : hex;
  }

  rgbToHex(r, g, b) {
    return '#' + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
  }

  getDominantColorFromTeamLogo(name: string, userService, colorsMap, logosMap) {
    return userService.getUserByKeyAsync(name).then(userSnapshot => {

      let url = 'assets/images/user/verein/logo_' + userSnapshot
        .data()
        .team
        .toLowerCase()
        .replace(new RegExp(' ', 'g'), '_') + '.png';
      let vibrant = new Vibrant(url, {ImageClass: BrowserImage});

      return vibrant.getPalette()
        .then((palette) => {
          colorsMap.set(name, palette.Vibrant.getHex());
          logosMap.set(name, url.replace('.png', '_xxs.png'));
          return palette.Vibrant.getHex();
        })
        // if logo based on teamName couldn't be found, use default-image:
        .catch(() => {
          console.log('catched for: ' + name);
          url = 'assets/images/user/verein/logo_default.png';

          vibrant = new Vibrant(url, {ImageClass: BrowserImage});
          return vibrant.getPalette()
            .then((innerPalette) => {
              colorsMap.set(name, innerPalette.Vibrant.getHex());
              logosMap.set(name, url.replace('.png', '_xxs.png'));
              return innerPalette.Vibrant.getHex();
            });
        });
    });
  }
}
