import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  setDocumentWithCustomKey(db, dbPath, customDocumentKey, data) {
    db.collection(dbPath).doc(customDocumentKey).set(this.stringifyData(data));
  }

  setDocumentAndReturnKey(db: AngularFirestore, dbPath: string, data: any) {
    const documentKey = db.createId();
    this.setDocumentWithCustomKey(db, dbPath, documentKey, data);
    return documentKey;
  }

  setDocumentAndReturnCustomizedKey(db: AngularFirestore, dbPath: string, documentKeyPrefix: string, data: any) {
    const documentKey = documentKeyPrefix + '_' + db.createId();
    this.setDocumentWithCustomKey(db, dbPath, documentKey, data);
    return documentKey;
  }

  updateDocument(db: AngularFirestore, dbPath: string, documentKey: string, data: any) {
    db.collection(dbPath).doc(documentKey).update(this.stringifyData(data));
  }

  private stringifyData(data) {
    return (Object.assign({}, JSON.parse(JSON.stringify(data))));
  }
}
