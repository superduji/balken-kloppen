import { Component, OnInit } from '@angular/core';
import RulesJson from '../../assets/rules.json';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {
  headline = 'IBKA® Balken Kloppen™ Regelwerk';
  rules: object;

  constructor() {
    this.rules = RulesJson;
  }

  ngOnInit() {
  }

  jumpToSection(section: number): void {
    const sectionToJumpTo = document.querySelector(`[data-id="${section}"]`) as HTMLElement;
    window.scrollTo(0, sectionToJumpTo.offsetTop);
  }

  jumpToSubSection(section: number, subSection: number): void {
    const sectionToJumpTo = document.querySelector(`[data-id="${section}-${subSection}"]`) as HTMLElement;
    window.scrollTo(0, sectionToJumpTo.offsetTop);
  }

}
