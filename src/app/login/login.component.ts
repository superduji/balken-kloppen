import {Component, OnInit} from '@angular/core';
import {LoginUser} from './login-user';
import {LoginService} from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginService: LoginService;

  texts = {
    name: 'Name',
    password: 'Passwort',
    error: 'Hahahahahahahaha, hast wohl nicht die richtigen Daten!',
    alreadyLoggedIn: 'Deine Anmeldung war erfolgreich. Viel Spaß beim Kloppen auf der Plattform der IBKA®.',
    login: 'Einloggen',
    logout: 'Ausloggen'
  };

  loginUser: LoginUser = new LoginUser();
  error = false;

  constructor(loginService: LoginService) {
    this.loginService = loginService;
  }

  ngOnInit() {
  }

  onSubmit() {
    this.validateUser();
  }

  validateUser() {
    this.loginService.getAdminUsers()
      .subscribe(snapshot => {
        const dbAdminUser = snapshot.data();
        if (this.loginUser.name === dbAdminUser.name && this.loginUser.password === dbAdminUser.password) {
          this.loginService.setLoginCookie();
        } else {
          this.error = true;
        }
      });
  }

}
