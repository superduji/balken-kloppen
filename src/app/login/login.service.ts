import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {CookieService} from 'ngx-cookie-service';
import {Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  COOKIE_NAME = '89FiSVMtOIj8hOhSYARX';
  COOKIE_VALUE = 'M2SjNSAoceEygDbtz1S8';

  private db: AngularFirestore;
  private dbPath = '/admin';

  private loggedInSource = new Subject<boolean>();
  public loggedIn = this.loggedInSource.asObservable();

  constructor(private angularFirestore: AngularFirestore,
              private cookieService: CookieService) {
    this.db = angularFirestore;
  }

  getAdminUsers() {
    return this.db
      .collection(this.dbPath).doc('admin').get();
  }

  setLoginCookie() {
    this.cookieService.set(this.COOKIE_NAME, this.COOKIE_VALUE);
    this.loggedInSource.next(true);
  }

  isUserLoggedIn() {
    return this.cookieService.check(this.COOKIE_NAME);
  }

  logout(): void {
    this.cookieService.delete(this.COOKIE_NAME);
    this.loggedInSource.next(false);
  }
}
