// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC2wx3-hKSD6khemNYOyMO4pqOQ5neM8Q0',
    authDomain: 'balken-kloppen.firebaseapp.com',
    databaseURL: 'https://balken-kloppen.firebaseio.com',
    projectId: 'balken-kloppen',
    storageBucket: 'balken-kloppen.appspot.com',
    messagingSenderId: '218776222883',
    appId: '1:218776222883:web:ccb5c32824ffb849'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
